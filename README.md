# README #

## Important Note: ##
The code is tailored to work on Linux. Make relevant changes before you test it on another OS.
The following commands are needed to be run:
* One of the following:
 - sudo apt install sox libsox-fmt-mp3
 - sudo apt install ffmpeg


### What is this repository for? ###

* Quick summary: 
Source code for tools used in the research for Quran Recitation Verification.

- MyRecitationVerifier: This is a tool preparing data to use the Sphinx trainer to build an acoustic model for the Holy Quran.

- WordCount: Count the word's repetition in a file. Used to get stats from the Quran.

* Version:
The stable version 1.2 is in the main "master" branch

### Who do I talk to? ###

* Repo owner or admin:

Mohamed Y. El Amrani <elamrani.my@gmail.com>

* Other community or team contact:

Asst. Prof. M.M. Hafizur Rahman <hafizur@iium.edu.my>

Prof. Mohamed Ridza Wahiddin <mridza@iium.edu.my>

Prof. Asadullah Shah <asadullah@iium.edu.my>

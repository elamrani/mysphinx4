/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mysounds;

import myacousticmodelbuilder.WaveformPanelContainer;
import java.awt.BorderLayout;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JFrame;

/**
 *
 * @author mohamed
 */
public class WaveformDisplay {

    public WaveformDisplay() {
    }

    public static void main(String[] args) {

        AudioInputStream audioInputStream = null;
        try {
            String filename = "/home/mohamed/Development/models/Arabic/Holy_Quran/Development/t5/wav/Abdul_Basit_Mujawwad_128kbps_versebyverse/001007_id0074.wav";
            File file = new File(filename);
            audioInputStream = AudioSystem.getAudioInputStream(file);

            JFrame frame = new JFrame("Waveform Display Simulator");
            frame.setBounds(200, 200, 500, 350);

            WaveformPanelContainer container = new WaveformPanelContainer();
            container.setAudioToDisplay(audioInputStream);

            frame.getContentPane().setLayout(new BorderLayout());
            frame.getContentPane().add(container, BorderLayout.CENTER);

            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

            frame.show();
            frame.validate();
            frame.repaint();

        }
        catch (UnsupportedAudioFileException | IOException ex) {
            Logger.getLogger(WaveformDisplay.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally {
            try {
                audioInputStream.close();
            }
            catch (IOException ex) {
                Logger.getLogger(WaveformDisplay.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

}

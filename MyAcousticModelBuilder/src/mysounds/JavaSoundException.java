package mysounds;

public class JavaSoundException extends Exception {

    public JavaSoundException(String message) {
        super(message);
    }
}

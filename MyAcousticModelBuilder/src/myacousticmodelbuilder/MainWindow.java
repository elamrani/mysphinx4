/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myacousticmodelbuilder;

import java.awt.Desktop;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import jxl.CellView;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.format.UnderlineStyle;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.NumberFormats;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import org.apache.commons.io.FileUtils;

/**
 * @author Mohamed Y. El Amrani
 */
public class MainWindow extends javax.swing.JFrame implements ILogFile {

    private static final long serialVersionUID = 8334307565763389939L;

    private final String DIR_ETC = "/etc";
    private final String DIR_WAV = "/wav";
    private final String EXT_WAV = ".wav";
    private final String EXT_MP3 = ".mp3";
//    private final String EXT_CONTEXTCUES = ".ccs";
    private final String EXT_FILEIDS = ".fileids";
    private final String EXT_TRANSCRIPTION = ".transcription";
    private final String EXT_FILLER = ".filler";
    private final String EXT_DICTIONARY = ".dic";
    private final String EXT_PHONEMES = ".phone";
    private final String POSTFIX_TRAINING = "_train";
    private final String POSTFIX_TEST = "_test";
    private final String STR_ENCODING = "UTF-8";
    private final String STR_CONNECTION = "jdbc:mysql://localhost/quran?useSSL=false&useUnicode=yes&characterEncoding=UTF-8&user=user_my_quran&password=123456789";
    private double _dPercentageOfTestFiles = 20; // = 20%
    private boolean _bUseTestForTraining = false; // true: use the same file for both training and testing
    private String _sLogData;

    private boolean _bVerboseMode = false;
    private DefaultListModel _dlmAllChapters, _dlmSelectedChapters;
    private String _sLanguageModelName = null;
    private Connection _connection = null;
    private Statement _statement = null;
    private ResultSet _resultSet = null;
    private String _sRootPassword = null;

    /**
     * Creates new form MainWindow
     */
    public MainWindow() {
        initComponents();
        init();
    }

    private void init() {
        //Center form: http://stackoverflow.com/questions/9543320/how-to-position-the-form-in-the-center-screen
        pack();
        setLocationRelativeTo(null);  // *** this will center your app ***

        _dlmAllChapters = new DefaultListModel();
        _dlmSelectedChapters = new DefaultListModel();

        //Related to this project
        tfQuranRootDir.setText("/home/mohamed/Development/models/Arabic/Holy_Quran/audio");
        tfLanguageRootDir.setText("/home/mohamed/Development/models/Arabic/Holy_Quran/Development/t7");
        _sLanguageModelName = tfLanguageRootDir.getText().substring(tfLanguageRootDir.getText().lastIndexOf("/") + 1);

        lstAllChapters.setModel(_dlmAllChapters);
        // chapter 0 is the إستعاذة
        for (int i = 0; i <= 114; i++) {
            _dlmAllChapters.addElement(String.format("%03d", i));
        }//for i

        lstSelectedChapters.setModel(_dlmSelectedChapters);
        _dlmSelectedChapters.removeAllElements();

        try {
            //Prepare the connection to the DB
            _connection = DriverManager.getConnection(STR_CONNECTION);
            // statements allow to issue SQL queries to the database
            _statement = _connection.createStatement();
        }//try
        catch (SQLException ex) {
            Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
            writeLogFile(ex);
        }//catch

        _bVerboseMode = ckVerboseMode.isSelected();
        lblDimensionalityLDA_MLLT.setEnabled(ckCalculateLDA_MLLT.isSelected());
        tfDimensionalityLDA_MLLT.setEnabled(ckCalculateLDA_MLLT.isSelected());

        //Set the tab to display first
        //jTabbedPane1.setSelectedIndex(1);
    }

    private boolean EmptyDatabaseConfirmation() {
        boolean bReturnValue = false;
        String message = "Do you really want to empty all the tables in the database?";
        int answer = JOptionPane.showConfirmDialog(this, message,
                "Empty Database?",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE);
        if (answer == JOptionPane.YES_OPTION) {
            message = "Are you sure? All corrected transcriptions will be lost!? Do you really want to do this?";
            answer = JOptionPane.showConfirmDialog(this, message,
                    "Really!?",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE);
            if (answer == JOptionPane.YES_OPTION) {
                bReturnValue = true;
            }//if
        }//if

        return bReturnValue;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bgSenones = new javax.swing.ButtonGroup();
        bgDimensions = new javax.swing.ButtonGroup();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        btnGenerate = new javax.swing.JButton();
        btnRestartTraining = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        tfSenonesNumber = new javax.swing.JTextField();
        ckVTLN = new javax.swing.JCheckBox();
        ckMultipleGaussian = new javax.swing.JCheckBox();
        ckForceAlignTranscript = new javax.swing.JCheckBox();
        jLabel7 = new javax.swing.JLabel();
        ckOpenTrainingLogInBrowser = new javax.swing.JCheckBox();
        ckCalculateLDA_MLLT = new javax.swing.JCheckBox();
        lblDimensionalityLDA_MLLT = new javax.swing.JLabel();
        tfDimensionalityLDA_MLLT = new javax.swing.JTextField();
        ckTestLanguageModel = new javax.swing.JCheckBox();
        btnTestLanguageModel = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        pTestingScenarios = new javax.swing.JPanel();
        ckLDA_MLLT = new javax.swing.JCheckBox();
        tfLDA_MLLT_From = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        tfLDA_MLLT_To = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        tfNumberOfRepetition = new javax.swing.JTextField();
        ckSenones = new javax.swing.JCheckBox();
        jLabel14 = new javax.swing.JLabel();
        tfSenones_From = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        tfSenones_To = new javax.swing.JTextField();
        btnGenerateSpreadsheet = new javax.swing.JButton();
        jLabel16 = new javax.swing.JLabel();
        tfSenones_Increment = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        tfLDA_MLLT_Increment = new javax.swing.JTextField();
        pbProgress = new javax.swing.JProgressBar();
        jLabel18 = new javax.swing.JLabel();
        lblElapsed = new javax.swing.JLabel();
        rbSenonesRange = new javax.swing.JRadioButton();
        rbSenonesList = new javax.swing.JRadioButton();
        tfSenones_List = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        rbLDA_MLLT_List = new javax.swing.JRadioButton();
        tfLDA_MLLT_List = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        rbLDAMLLT_Range = new javax.swing.JRadioButton();
        jLabel9 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        tfPercentageOfTestFiles = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        ckUseTestForTraining = new javax.swing.JCheckBox();
        jLabel1 = new javax.swing.JLabel();
        tfQuranRootDir = new javax.swing.JTextField();
        btnBrowseQuranDir = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        tfLanguageRootDir = new javax.swing.JTextField();
        btnBrowseAcousticDir = new javax.swing.JButton();
        btnResetAMFolder = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        lstSelectedChapters = new javax.swing.JList();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        lstAllChapters = new javax.swing.JList();
        btnSelectAll = new javax.swing.JButton();
        btnDeselectAll = new javax.swing.JButton();
        btnSelect = new javax.swing.JButton();
        btnDeselect = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        btnShowTranscriptionAligner = new javax.swing.JButton();
        ckOpenTranscriptionAligner = new javax.swing.JCheckBox();
        ckPriorityHigh = new javax.swing.JCheckBox();
        ckVerboseMode = new javax.swing.JCheckBox();
        ckAskBeforeTrain = new javax.swing.JCheckBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Holy Quran Acoustic Model Builder");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        btnGenerate.setText("Generate Acoustic Model");
        btnGenerate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGenerateActionPerformed(evt);
            }
        });

        btnRestartTraining.setText("Restart the Training");
        btnRestartTraining.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRestartTrainingActionPerformed(evt);
            }
        });

        jLabel6.setText("Number of tied states (senones) to create in decision-tree clustering");

        tfSenonesNumber.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        tfSenonesNumber.setText("1000");

        ckVTLN.setText("Perform vocal tract length normalization in training (VTLN)?");
        ckVTLN.setEnabled(false);

        ckMultipleGaussian.setText("Train multiple-gaussian context-independent models (useful for alignment)?");
        ckMultipleGaussian.setEnabled(false);

        ckForceAlignTranscript.setText("Use force-aligned transcripts (if available) as input to training?");
        ckForceAlignTranscript.setEnabled(false);

        jLabel7.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        jLabel7.setText("SphinxTrain configuration");

        ckOpenTrainingLogInBrowser.setSelected(true);
        ckOpenTrainingLogInBrowser.setText("Open training log in browser");

        ckCalculateLDA_MLLT.setSelected(true);
        ckCalculateLDA_MLLT.setText("Calculate an LDA/MLLT transform?");
        ckCalculateLDA_MLLT.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ckCalculateLDA_MLLTMouseClicked(evt);
            }
        });

        lblDimensionalityLDA_MLLT.setText("Dimensionality of LDA/MLLT output");

        tfDimensionalityLDA_MLLT.setText("32");

        ckTestLanguageModel.setText("Test the Acoustic Model after the training");

        btnTestLanguageModel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/myacousticmodelbuilder/resources/audio-input-microphone-48.png"))); // NOI18N
        btnTestLanguageModel.setText("Test the Language Model");
        btnTestLanguageModel.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnTestLanguageModel.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnTestLanguageModel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTestLanguageModelActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(btnRestartTraining, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(btnGenerate, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(96, 96, 96))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(ckTestLanguageModel, javax.swing.GroupLayout.PREFERRED_SIZE, 351, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addComponent(btnTestLanguageModel, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(2, 2, 2)
                                .addComponent(lblDimensionalityLDA_MLLT)
                                .addGap(12, 12, 12)
                                .addComponent(tfDimensionalityLDA_MLLT, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel6))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tfSenonesNumber, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(ckVTLN)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7)
                            .addComponent(ckCalculateLDA_MLLT)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(ckForceAlignTranscript)
                        .addGap(106, 106, 106)
                        .addComponent(ckOpenTrainingLogInBrowser))
                    .addComponent(ckMultipleGaussian, javax.swing.GroupLayout.PREFERRED_SIZE, 578, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(23, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnTestLanguageModel, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addGap(6, 6, 6)
                        .addComponent(ckCalculateLDA_MLLT)
                        .addGap(4, 4, 4)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(5, 5, 5)
                                .addComponent(lblDimensionalityLDA_MLLT))
                            .addComponent(tfDimensionalityLDA_MLLT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(4, 4, 4)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(tfSenonesNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(3, 3, 3)
                        .addComponent(ckVTLN)
                        .addGap(6, 6, 6)
                        .addComponent(ckMultipleGaussian)
                        .addGap(6, 6, 6)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(ckForceAlignTranscript)
                            .addComponent(ckOpenTrainingLogInBrowser))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ckTestLanguageModel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnRestartTraining, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnGenerate, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(28, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Single Training", jPanel1);

        ckLDA_MLLT.setSelected(true);
        ckLDA_MLLT.setText("Calculate an LDA/MLLT transform");
        ckLDA_MLLT.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ckLDA_MLLTMouseClicked(evt);
            }
        });

        tfLDA_MLLT_From.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        tfLDA_MLLT_From.setText("4");
        tfLDA_MLLT_From.setEnabled(false);

        jLabel10.setText("From");

        tfLDA_MLLT_To.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        tfLDA_MLLT_To.setText("64");
        tfLDA_MLLT_To.setEnabled(false);

        jLabel11.setText("to");

        jLabel12.setText("Repeat each training ");

        jLabel13.setText("time(s)");

        tfNumberOfRepetition.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        tfNumberOfRepetition.setText("1");
        tfNumberOfRepetition.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                tfNumberOfRepetitionFocusLost(evt);
            }
        });

        ckSenones.setSelected(true);
        ckSenones.setText("Number of tied states (senones)");
        ckSenones.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ckSenonesMouseClicked(evt);
            }
        });

        jLabel14.setText("From");

        tfSenones_From.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        tfSenones_From.setText("200");
        tfSenones_From.setEnabled(false);

        jLabel15.setText("to");

        tfSenones_To.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        tfSenones_To.setText("1000");
        tfSenones_To.setEnabled(false);

        btnGenerateSpreadsheet.setText("Generate Spreadsheet");
        btnGenerateSpreadsheet.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGenerateSpreadsheetActionPerformed(evt);
            }
        });

        jLabel16.setText("by");

        tfSenones_Increment.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        tfSenones_Increment.setText("200");
        tfSenones_Increment.setEnabled(false);

        jLabel17.setText("by");

        tfLDA_MLLT_Increment.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        tfLDA_MLLT_Increment.setText("2");
        tfLDA_MLLT_Increment.setEnabled(false);

        jLabel18.setText("Elapsed: ");

        lblElapsed.setText("00h 00m 00s");

        bgSenones.add(rbSenonesRange);
        rbSenonesRange.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbSenonesRangeActionPerformed(evt);
            }
        });

        bgSenones.add(rbSenonesList);
        rbSenonesList.setSelected(true);
        rbSenonesList.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbSenonesListActionPerformed(evt);
            }
        });

        tfSenones_List.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        tfSenones_List.setText("500,750");

        jLabel19.setText("List (comma separated)");

        bgDimensions.add(rbLDA_MLLT_List);
        rbLDA_MLLT_List.setSelected(true);
        rbLDA_MLLT_List.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbLDA_MLLT_ListActionPerformed(evt);
            }
        });

        tfLDA_MLLT_List.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        tfLDA_MLLT_List.setText("32");

        jLabel20.setText("List (comma separated)");

        bgDimensions.add(rbLDAMLLT_Range);
        rbLDAMLLT_Range.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbLDAMLLT_RangeActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pTestingScenariosLayout = new javax.swing.GroupLayout(pTestingScenarios);
        pTestingScenarios.setLayout(pTestingScenariosLayout);
        pTestingScenariosLayout.setHorizontalGroup(
            pTestingScenariosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pTestingScenariosLayout.createSequentialGroup()
                .addGroup(pTestingScenariosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pTestingScenariosLayout.createSequentialGroup()
                        .addGroup(pTestingScenariosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pTestingScenariosLayout.createSequentialGroup()
                                .addComponent(jLabel12)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(tfNumberOfRepetition, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel13))
                            .addGroup(pTestingScenariosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(pTestingScenariosLayout.createSequentialGroup()
                                    .addComponent(ckSenones)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jLabel14)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(tfSenones_From, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jLabel15)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(tfSenones_To, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jLabel16)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(tfSenones_Increment, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(rbSenonesRange))
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pTestingScenariosLayout.createSequentialGroup()
                                    .addContainerGap()
                                    .addComponent(jLabel19)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(tfSenones_List)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(rbSenonesList)))
                            .addGroup(pTestingScenariosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(pTestingScenariosLayout.createSequentialGroup()
                                    .addComponent(jLabel20)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(tfLDA_MLLT_List, javax.swing.GroupLayout.PREFERRED_SIZE, 207, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(rbLDA_MLLT_List))
                                .addGroup(pTestingScenariosLayout.createSequentialGroup()
                                    .addGroup(pTestingScenariosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addGroup(pTestingScenariosLayout.createSequentialGroup()
                                            .addComponent(ckLDA_MLLT)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(jLabel10)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(tfLDA_MLLT_From, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(btnGenerateSpreadsheet, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jLabel11)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(tfLDA_MLLT_To, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(jLabel17)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(tfLDA_MLLT_Increment, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(rbLDAMLLT_Range))))
                        .addGap(0, 213, Short.MAX_VALUE))
                    .addGroup(pTestingScenariosLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel18)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblElapsed, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(pbProgress, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        pTestingScenariosLayout.setVerticalGroup(
            pTestingScenariosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pTestingScenariosLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pTestingScenariosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(jLabel13)
                    .addComponent(tfNumberOfRepetition, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pTestingScenariosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(pTestingScenariosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(pTestingScenariosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel17)
                            .addComponent(tfLDA_MLLT_Increment, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(pTestingScenariosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(ckLDA_MLLT)
                            .addComponent(jLabel10)
                            .addComponent(tfLDA_MLLT_From, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel11)
                            .addComponent(tfLDA_MLLT_To, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(rbLDAMLLT_Range))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pTestingScenariosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(rbLDA_MLLT_List)
                    .addGroup(pTestingScenariosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(tfLDA_MLLT_List, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel20)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(pTestingScenariosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pTestingScenariosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel14)
                        .addComponent(tfSenones_From, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel15)
                        .addComponent(tfSenones_To, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel16)
                        .addComponent(tfSenones_Increment, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(ckSenones)
                    .addComponent(rbSenonesRange))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pTestingScenariosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(rbSenonesList)
                    .addGroup(pTestingScenariosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(tfSenones_List, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel19)))
                .addGap(22, 22, 22)
                .addComponent(btnGenerateSpreadsheet, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(62, 62, 62)
                .addGroup(pTestingScenariosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pbProgress, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(pTestingScenariosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel18)
                        .addComponent(lblElapsed))))
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pTestingScenarios, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pTestingScenarios, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("Multiple Trainings", jPanel2);

        jLabel9.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        jLabel9.setText("Acoustic Model training configuration");

        jLabel5.setText("% of test files");

        tfPercentageOfTestFiles.setHorizontalAlignment(javax.swing.JTextField.TRAILING);
        tfPercentageOfTestFiles.setText("0.1");
        tfPercentageOfTestFiles.setToolTipText("20 means 20%");

        jLabel8.setText("%");

        ckUseTestForTraining.setText("Use the same file for both testing and training");

        jLabel1.setLabelFor(tfQuranRootDir);
        jLabel1.setText("Holy Quran root folder:");

        tfQuranRootDir.setName("tfQuranRootDir"); // NOI18N

        btnBrowseQuranDir.setText("...");
        btnBrowseQuranDir.setName("btnBrowseQuranDir"); // NOI18N
        btnBrowseQuranDir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBrowseQuranDirActionPerformed(evt);
            }
        });

        jLabel2.setLabelFor(tfLanguageRootDir);
        jLabel2.setText("Acoustic Model root folder:");

        tfLanguageRootDir.setName("tfQuranRootDir"); // NOI18N

        btnBrowseAcousticDir.setText("...");
        btnBrowseAcousticDir.setName("btnBrowseQuranDir"); // NOI18N
        btnBrowseAcousticDir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBrowseAcousticDirActionPerformed(evt);
            }
        });

        btnResetAMFolder.setText("Reset");
        btnResetAMFolder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnResetAMFolderActionPerformed(evt);
            }
        });

        lstSelectedChapters.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lstSelectedChaptersMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(lstSelectedChapters);

        jLabel3.setLabelFor(lstAllChapters);
        jLabel3.setText("All Holy Quran Chapters");

        lstAllChapters.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lstAllChaptersMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(lstAllChapters);

        btnSelectAll.setText(">>");
        btnSelectAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSelectAllActionPerformed(evt);
            }
        });

        btnDeselectAll.setText("<<");
        btnDeselectAll.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeselectAllActionPerformed(evt);
            }
        });

        btnSelect.setText(">");
        btnSelect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSelectActionPerformed(evt);
            }
        });

        btnDeselect.setText("<");
        btnDeselect.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeselectActionPerformed(evt);
            }
        });

        jLabel4.setLabelFor(lstSelectedChapters);
        jLabel4.setText("Holy Quran Chapters to use in the training");

        btnShowTranscriptionAligner.setText("Transcription Editor");
        btnShowTranscriptionAligner.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnShowTranscriptionAlignerActionPerformed(evt);
            }
        });

        ckOpenTranscriptionAligner.setText("Open the Transcription Editor before training");

        ckPriorityHigh.setFont(new java.awt.Font("Ubuntu", 3, 15)); // NOI18N
        ckPriorityHigh.setText("Run commands with High Priority");
        ckPriorityHigh.setEnabled(false);
        ckPriorityHigh.setHorizontalTextPosition(javax.swing.SwingConstants.LEADING);
        ckPriorityHigh.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ckPriorityHighMouseClicked(evt);
            }
        });

        ckVerboseMode.setText("Verbose mode");
        ckVerboseMode.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ckVerboseModeMouseClicked(evt);
            }
        });

        ckAskBeforeTrain.setText("Ask for confirmation before starting the training?");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTabbedPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(tfLanguageRootDir)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnBrowseAcousticDir, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnResetAMFolder))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(tfQuranRootDir)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnBrowseQuranDir, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel9)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(ckPriorityHigh))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 247, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jScrollPane2)
                                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, 382, Short.MAX_VALUE)))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 338, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(32, 32, 32)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(btnSelectAll, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(btnDeselectAll, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(btnSelect, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(btnDeselect, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 227, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel5)
                                        .addGap(1, 1, 1)
                                        .addComponent(tfPercentageOfTestFiles, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(3, 3, 3)
                                        .addComponent(jLabel8)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(ckUseTestForTraining)))
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(ckVerboseMode)
                                .addGap(35, 35, 35)
                                .addComponent(ckAskBeforeTrain, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(ckOpenTranscriptionAligner))
                        .addGap(138, 138, 138)
                        .addComponent(btnShowTranscriptionAligner, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(15, 15, 15))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(ckPriorityHigh))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(tfPercentageOfTestFiles, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(ckUseTestForTraining))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addGap(2, 2, 2)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tfQuranRootDir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnBrowseQuranDir))
                .addGap(7, 7, 7)
                .addComponent(jLabel2)
                .addGap(3, 3, 3)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tfLanguageRootDir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnBrowseAcousticDir)
                        .addComponent(btnResetAMFolder)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4))
                .addGap(3, 3, 3)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(btnSelectAll)
                        .addGap(1, 1, 1)
                        .addComponent(btnDeselectAll)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSelect)
                        .addGap(1, 1, 1)
                        .addComponent(btnDeselect)))
                .addGap(8, 8, 8)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnShowTranscriptionAligner, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(ckOpenTranscriptionAligner)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(ckVerboseMode)
                            .addComponent(ckAskBeforeTrain))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTabbedPane1))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        try {
            if (!_statement.isClosed()) {
                _statement.close();
            }//if
            if (!_connection.isClosed()) {
                _connection.close();
            }//if
        }//try
        catch (SQLException ex) {
            Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
        }//catch
    }//GEN-LAST:event_formWindowClosing

    private void btnResetAMFolderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnResetAMFolderActionPerformed
        resetLanguageModelFolder();
    }//GEN-LAST:event_btnResetAMFolderActionPerformed

    private void lstSelectedChaptersMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstSelectedChaptersMouseClicked
        if (evt.getClickCount() == 2) {
            btnDeselectActionPerformed(new ActionEvent(evt, -1, null));
        }//if
    }//GEN-LAST:event_lstSelectedChaptersMouseClicked

    private void btnDeselectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeselectActionPerformed
        for (int i = 0; i < lstSelectedChapters.getSelectedIndices().length; i++) {
            _dlmAllChapters.addElement(_dlmSelectedChapters.getElementAt(lstSelectedChapters.getSelectedIndices()[i]));
            _dlmSelectedChapters.removeElementAt(lstSelectedChapters.getSelectedIndices()[i]);
        }//for i
        sort(_dlmAllChapters);
        sort(_dlmSelectedChapters);
    }//GEN-LAST:event_btnDeselectActionPerformed

    private void btnSelectActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSelectActionPerformed
        while (lstAllChapters.getSelectedIndices().length > 0) {
            _dlmSelectedChapters.addElement(_dlmAllChapters.getElementAt(lstAllChapters.getSelectedIndices()[0]));
            _dlmAllChapters.removeElementAt(lstAllChapters.getSelectedIndices()[0]);
        }
        sort(_dlmAllChapters);
        sort(_dlmSelectedChapters);
    }//GEN-LAST:event_btnSelectActionPerformed

    private void btnDeselectAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeselectAllActionPerformed
        while (_dlmSelectedChapters.getSize() > 0) {
            _dlmAllChapters.addElement(_dlmSelectedChapters.getElementAt(0));
            _dlmSelectedChapters.remove(0);
        }//for i
        sort(_dlmAllChapters);
    }//GEN-LAST:event_btnDeselectAllActionPerformed

    private void btnSelectAllActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSelectAllActionPerformed
        for (int i = 0; i < _dlmAllChapters.getSize(); i++) {
            _dlmSelectedChapters.addElement(_dlmAllChapters.getElementAt(i));
        }//for i
        _dlmAllChapters.removeAllElements();
        sort(_dlmSelectedChapters);
    }//GEN-LAST:event_btnSelectAllActionPerformed

    private void lstAllChaptersMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lstAllChaptersMouseClicked
        if (evt.getClickCount() == 2) {
            btnSelectActionPerformed(new ActionEvent(evt, -1, null));
        }//if
    }//GEN-LAST:event_lstAllChaptersMouseClicked

    private void btnBrowseAcousticDirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBrowseAcousticDirActionPerformed
        showFolderChooser("Select the Language Model root directory", tfLanguageRootDir);
        _sLanguageModelName = tfLanguageRootDir.getText().substring(tfLanguageRootDir.getText().lastIndexOf("/") + 1);
    }//GEN-LAST:event_btnBrowseAcousticDirActionPerformed

    private void btnBrowseQuranDirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBrowseQuranDirActionPerformed
        showFolderChooser("Select the Holy Quran root directory", tfQuranRootDir);
    }//GEN-LAST:event_btnBrowseQuranDirActionPerformed

    private void btnShowTranscriptionAlignerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnShowTranscriptionAlignerActionPerformed
        try {
            new TranscriptionEditorWindow(this, true,
                    tfLanguageRootDir.getText(), tfQuranRootDir.getText(),
                    ckUseTestForTraining.isSelected(), _bVerboseMode, _connection
            ).setVisible(true);

            if (JOptionPane.showConfirmDialog(this, "Do you want to re-generate the main files again?",
                    "Re-generate main files?",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
                prepareFiles(false, true);
            }//if
        }//try
        catch (SQLException ex) {
            Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
        }//catch
    }//GEN-LAST:event_btnShowTranscriptionAlignerActionPerformed

    private void ckLDA_MLLTMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ckLDA_MLLTMouseClicked
        boolean value = ckLDA_MLLT.isSelected();
        tfLDA_MLLT_From.setEnabled(value && rbLDAMLLT_Range.isSelected());
        tfLDA_MLLT_To.setEnabled(value && rbLDAMLLT_Range.isSelected());
        tfLDA_MLLT_Increment.setEnabled(value && rbLDAMLLT_Range.isSelected());
        tfLDA_MLLT_List.setEnabled(value && rbLDA_MLLT_List.isSelected());
        rbLDAMLLT_Range.setEnabled(value);
        rbLDA_MLLT_List.setEnabled(value);
    }//GEN-LAST:event_ckLDA_MLLTMouseClicked

    private void ckSenonesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ckSenonesMouseClicked
        boolean value = ckSenones.isSelected();
        tfSenones_From.setEnabled(value && rbSenonesRange.isSelected());
        tfSenones_To.setEnabled(value && rbSenonesRange.isSelected());
        tfSenones_Increment.setEnabled(value && rbSenonesRange.isSelected());
        tfSenones_List.setEnabled(value && rbSenonesList.isSelected());
        rbSenonesRange.setEnabled(value);
        rbSenonesList.setEnabled(value);
    }//GEN-LAST:event_ckSenonesMouseClicked

    private void btnGenerateSpreadsheetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGenerateSpreadsheetActionPerformed
        generateSpreadsheet();
    }//GEN-LAST:event_btnGenerateSpreadsheetActionPerformed

    private void printElapsedTime(long startAll) {
        long time = System.nanoTime() - startAll;
        double seconds = (double) time / 1e9;
        int hours = (int) seconds / 3600;
        seconds -= 3600 * hours;
        int minutes = (int) seconds / 60;
        seconds -= 60 * minutes;
        _sLogData = "[" + new java.sql.Timestamp(new java.util.Date().getTime()) + "] "
                + String.format("Time elapsed (%02dh %02dm %02d.%1ds)", hours, minutes,
                        (int) seconds, (int) (10 * (seconds - (int) seconds)));
        System.out.println(_sLogData);
        writeLogFile(_sLogData);
    }

    private void tfNumberOfRepetitionFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_tfNumberOfRepetitionFocusLost
        if (Integer.parseInt(tfNumberOfRepetition.getText()) < 1) {
            tfNumberOfRepetition.setText("1");
        }//if
    }//GEN-LAST:event_tfNumberOfRepetitionFocusLost

    private void ckPriorityHighMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ckPriorityHighMouseClicked
        if (ckPriorityHigh.isSelected()) {
            JPasswordField pf = new JPasswordField();
            int okCxl = JOptionPane.showConfirmDialog(null, pf, "root passwd (at your own risk!)", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);

            if (okCxl == JOptionPane.OK_OPTION) {
                _sRootPassword = new String(pf.getPassword());
            }//if
            else {
                _sRootPassword = null;
            }
        }//if
        else {
            _sRootPassword = null;
        }//else

        if (_sRootPassword == null) {
            ckPriorityHigh.setSelected(false);
        }//if
    }//GEN-LAST:event_ckPriorityHighMouseClicked

    private void ckVerboseModeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ckVerboseModeMouseClicked
        _bVerboseMode = ckVerboseMode.isSelected();
    }//GEN-LAST:event_ckVerboseModeMouseClicked

    private void rbSenonesRangeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbSenonesRangeActionPerformed
        updateSenonesGUI();
    }//GEN-LAST:event_rbSenonesRangeActionPerformed

    private void rbSenonesListActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbSenonesListActionPerformed
        updateSenonesGUI();
    }//GEN-LAST:event_rbSenonesListActionPerformed

    private void rbLDAMLLT_RangeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbLDAMLLT_RangeActionPerformed
        update_LDA_MLLT_GUI();
    }//GEN-LAST:event_rbLDAMLLT_RangeActionPerformed

    private void rbLDA_MLLT_ListActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbLDA_MLLT_ListActionPerformed
        update_LDA_MLLT_GUI();
    }//GEN-LAST:event_rbLDA_MLLT_ListActionPerformed

    private void btnTestLanguageModelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTestLanguageModelActionPerformed
        testLanguageModel();
    }//GEN-LAST:event_btnTestLanguageModelActionPerformed

    private void ckCalculateLDA_MLLTMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ckCalculateLDA_MLLTMouseClicked
        lblDimensionalityLDA_MLLT.setEnabled(ckCalculateLDA_MLLT.isSelected());
        tfDimensionalityLDA_MLLT.setEnabled(ckCalculateLDA_MLLT.isSelected());
    }//GEN-LAST:event_ckCalculateLDA_MLLTMouseClicked

    private void btnRestartTrainingActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRestartTrainingActionPerformed
        if (JOptionPane.showConfirmDialog(this, "Do you want to restart the training?",
                "Restart the training?",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
            long startTime = System.nanoTime();
            configureAndStartTraining();
            printElapsedTime(startTime);
        }//if
    }//GEN-LAST:event_btnRestartTrainingActionPerformed

    private void btnGenerateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGenerateActionPerformed
        if (_dlmSelectedChapters.getSize() == 0) {
            JOptionPane.showMessageDialog(this, "Please select a Holy Quran chapter first!",
                    "Nothing selected!",
                    JOptionPane.OK_OPTION);
        }//if
        else {
            long startTime = System.nanoTime();
            //Ask if we need to reset the AM folder
            boolean bResetDB = resetLanguageModelFolder();

            _dPercentageOfTestFiles = Double.parseDouble(tfPercentageOfTestFiles.getText());
            _bUseTestForTraining = ckUseTestForTraining.isSelected();

            //Ask if we need to reset the database also?
            if (bResetDB) {
                bResetDB = EmptyDatabaseConfirmation();
            }//if
            prepareFiles(bResetDB, false);

            if (ckOpenTranscriptionAligner.isSelected()) {
                TranscriptionEditorWindow teWin;
                try {
                    teWin = new TranscriptionEditorWindow(this, true,
                            tfLanguageRootDir.getText(), tfQuranRootDir.getText(),
                            ckUseTestForTraining.isSelected(), _bVerboseMode, _connection);
                    teWin.setVisible(true);

                    if (JOptionPane.showConfirmDialog(this, "Do you want to re-generate the main files again?",
                            "Re-generate main files?",
                            JOptionPane.YES_NO_OPTION,
                            JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
                        prepareFiles(false, true);
                    }//if
                }//try
                catch (SQLException ex) {
                    Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
                    writeLogFile(ex);
                }//catch
            }//if
            printElapsedTime(startTime);
            boolean bStartTraining = true;
            if (ckAskBeforeTrain.isSelected()) {
                bStartTraining = JOptionPane.showConfirmDialog(this, "Do you want to start the training?",
                        "Start the training?",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION;
            }//if
            if (bStartTraining) {
                configureAndStartTraining();
                btnRestartTraining.setEnabled(true);
                printElapsedTime(startTime);
            }//if
        }//else
    }//GEN-LAST:event_btnGenerateActionPerformed

    private void generateSpreadsheet() {
        try {
            if (_dlmSelectedChapters.getSize() == 0) {
                JOptionPane.showMessageDialog(this, "Please select a Holy Quran chapter first!",
                        "Nothing selected!",
                        JOptionPane.OK_OPTION);
                return;
            }//if
            else {
                //Ask if we need to reset the AM folder
                boolean bResetDB = resetLanguageModelFolder();

                _dPercentageOfTestFiles = Double.parseDouble(tfPercentageOfTestFiles.getText());
                _bUseTestForTraining = ckUseTestForTraining.isSelected();

                if (bResetDB) {
                    bResetDB = EmptyDatabaseConfirmation();
                    prepareFiles(bResetDB, false);
                }//if

                if (ckOpenTranscriptionAligner.isSelected()) {
                    TranscriptionEditorWindow teWin;
                    try {
                        teWin = new TranscriptionEditorWindow(this, true,
                                tfLanguageRootDir.getText(), tfQuranRootDir.getText(),
                                ckUseTestForTraining.isSelected(), _bVerboseMode, _connection);
                        teWin.setVisible(true);
                        if (JOptionPane.showConfirmDialog(this, "Do you want to re-generate the main files again?",
                                "Re-generate main files?",
                                JOptionPane.YES_NO_OPTION,
                                JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
                            prepareFiles(false, true);
                        }//if
                    }//try
                    catch (SQLException ex) {
                        Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
                        writeLogFile(ex);
                    }//catch
                }//if

                buildLanguageModelFile();

            }//else

            int iSenones_From = 0;
            int iSenones_To = 0;
            int iSenones_Increment;
            int iLDA_MLLT_From = 0;
            int iLDA_MLLT_To = 0;
            int iLDA_MLLT_Increment;
            ArrayList<Integer> listSenones = new ArrayList<>();
            ArrayList<Integer> listDimensions = new ArrayList<>();

            if (rbSenonesRange.isSelected()) {
                iSenones_From = !ckSenones.isSelected() ? 200 : Integer.parseInt(tfSenones_From.getText());
                iSenones_To = !ckSenones.isSelected() ? 200 : Integer.parseInt(tfSenones_To.getText());
                iSenones_Increment = !ckSenones.isSelected() ? 1 : Integer.parseInt(tfSenones_Increment.getText());
                for (int i = iSenones_From; i <= iSenones_To; i += iSenones_Increment) {
                    listSenones.add(i);
                }//for i
            }//if
            else {
                String[] array = tfSenones_List.getText().split(",");
                for (String s : array) {
                    listSenones.add(Integer.parseInt(s.trim()));
                }//foreach
            }//else

            if (rbLDAMLLT_Range.isSelected()) {
                iLDA_MLLT_From = !ckLDA_MLLT.isSelected() ? 0 : Integer.parseInt(tfLDA_MLLT_From.getText());
                iLDA_MLLT_To = !ckLDA_MLLT.isSelected() ? 0 : Integer.parseInt(tfLDA_MLLT_To.getText());
                iLDA_MLLT_Increment = !ckLDA_MLLT.isSelected() ? 1 : Integer.parseInt(tfLDA_MLLT_Increment.getText());
                for (int i = iLDA_MLLT_From; i <= iLDA_MLLT_To; i += iLDA_MLLT_Increment) {
                    listDimensions.add(i);
                }//for i
            }//if
            else {
                String[] array = tfLDA_MLLT_List.getText().split(",");
                for (String s : array) {
                    listDimensions.add(Integer.parseInt(s.trim()));
                }//foreach
            }//else

            int iNumberOfRepetition = Integer.parseInt(tfNumberOfRepetition.getText());
            int iNumberOfScenarios = iNumberOfRepetition
                    * (ckSenones.isSelected() ? listSenones.size() : 1)
                    * (ckLDA_MLLT.isSelected() ? listDimensions.size() : 1);
            int iCurrentScenario = 0;

            //Prepare to write to a spreadsheet
            WritableCellFormat cfTimesBoldUnderline, cfTimes, cfTimesPercent;
            WritableFont times11pt = new WritableFont(WritableFont.TIMES, 11);
            WritableFont times11ptBoldUnderline = new WritableFont(WritableFont.TIMES, 11, WritableFont.BOLD, false,
                    UnderlineStyle.SINGLE);
            String outputFile = tfLanguageRootDir.getText().substring(0, tfLanguageRootDir.getText().lastIndexOf("/"))
                    + "/" + _sLanguageModelName
                    + "_" + new SimpleDateFormat("yyyyMMdd-HHmmss").format(new Date())
                    + "_Senones_" + (rbSenonesList.isSelected() ? tfSenones_List.getText() : (iSenones_From + "-" + iSenones_To))
                    + "_Dimension_" + (rbLDA_MLLT_List.isSelected() ? tfLDA_MLLT_List.getText() : (iLDA_MLLT_From + "-" + iLDA_MLLT_To))
                    + "_Repeat_" + iNumberOfRepetition
                    + "_PercentTestFiles_" + tfPercentageOfTestFiles.getText()
                    + ".xls";
            File file = new File(outputFile);

            cfTimes = new WritableCellFormat(times11pt);
            cfTimesBoldUnderline = new WritableCellFormat(times11ptBoldUnderline);
            cfTimesPercent = new WritableCellFormat(times11pt, NumberFormats.PERCENT_FLOAT);

            WorkbookSettings wbSettings = new WorkbookSettings();
            wbSettings.setLocale(new Locale("en", "EN"));
            WritableWorkbook workbook = Workbook.createWorkbook(file, wbSettings);
            workbook.createSheet("Average", 0);
            workbook.createSheet("Data", 1);
            WritableSheet averageSheet = workbook.getSheet(0);
            WritableSheet dataSheet = workbook.getSheet(1);

            //Add content in (Column, Row)
            String sMessage;
            if (ckUseTestForTraining.isSelected()) {
                sMessage = String.format("Using 100%% files for training and %s%% for testing", tfPercentageOfTestFiles.getText());
            }//if
            else {
                sMessage = String.format("Using %d%% of files for training, and %s%% for testing.", 100 - Integer.parseInt(tfPercentageOfTestFiles.getText()), tfPercentageOfTestFiles.getText());
            }//else

            _sLogData = "[" + new java.sql.Timestamp(new java.util.Date().getTime())
                    + "] \n***\nNew Training:\n***\n";
            System.out.println(_sLogData + "\n" + sMessage);
            writeLogFile(_sLogData + "\n" + sMessage);

            dataSheet.addCell(new Label(0, 0, sMessage, cfTimesBoldUnderline));
            String sTitle;
            if (rbSenonesRange.isSelected()) {
                sTitle = String.format("Senones from %s to %s", iSenones_From, iSenones_To);
            }//if
            else {
                sTitle = "Senones: " + tfSenones_List.getText();
            }//else
            if (rbSenonesRange.isSelected()) {
                sTitle += String.format(", LDA MLLT from %s to %s", iLDA_MLLT_From, iLDA_MLLT_To);
            }//if
            else {
                sTitle += ", LDA MLLT: " + tfLDA_MLLT_List.getText();
            }//else
            dataSheet.addCell(new Label(0, 1, sTitle, cfTimesBoldUnderline));
            //Other sheet
            averageSheet.addCell(new Label(0, 0, sMessage, cfTimesBoldUnderline));
            averageSheet.addCell(new Label(0, 1, sTitle, cfTimesBoldUnderline));

            dataSheet.addCell(new Label(1, 3, "Senones", cfTimesBoldUnderline));
            dataSheet.addCell(new Label(2, 3, "LDA MLLT", cfTimesBoldUnderline));
            dataSheet.addCell(new Label(3, 3, "Number", cfTimesBoldUnderline));
            dataSheet.addCell(new Label(4, 3, "SER", cfTimesBoldUnderline));
            dataSheet.addCell(new Label(5, 3, "WER", cfTimesBoldUnderline));
            dataSheet.addCell(new Label(6, 3, "Training Time (s)", cfTimesBoldUnderline));
            //Other table for the averages
            averageSheet.addCell(new Label(1, 3, "Senones", cfTimesBoldUnderline));
            averageSheet.addCell(new Label(2, 3, "LDA MLLT", cfTimesBoldUnderline));
            averageSheet.addCell(new Label(3, 3, "Number", cfTimesBoldUnderline));
            averageSheet.addCell(new Label(4, 3, "SER", cfTimesBoldUnderline));
            averageSheet.addCell(new Label(5, 3, "WER", cfTimesBoldUnderline));
            averageSheet.addCell(new Label(6, 3, "Training Time (s)", cfTimesBoldUnderline));
            //Graph data
            averageSheet.addCell(new Label(8, 3, "Senones", cfTimesBoldUnderline));
            int columnAverage = 9;
            for (int i : listDimensions) {
                averageSheet.addCell(new Label(columnAverage++, 3, "D = " + i, cfTimesBoldUnderline));
            }//for i

            //Write to the spreadsheet
            workbook.write();
            workbook.close();

            double[] averages = new double[3];
            averages[0] = averages[1] = averages[2] = 0;
            int rowData = 4, rowAverage = 4, rowAverageChart = 3, iPreviousSenones = -1;
            //execution time
            long startAll = System.nanoTime();
            for (int iSenones : listSenones) {
                columnAverage = 9;
                for (int iLDA_MLLT : listDimensions) {
                    for (int i = 0; i < iNumberOfRepetition; i++) {
                        _sLogData = "[" + new java.sql.Timestamp(new java.util.Date().getTime()) + "] "
                                + "\nIteration# " + ++iCurrentScenario + "/" + iNumberOfScenarios;
                        System.out.println(_sLogData);
                        writeLogFile(_sLogData);
                        long start = System.nanoTime();
                        double stats[] = configureAndRepeatTraining(iLDA_MLLT, iSenones);
                        long time = System.nanoTime() - start;
                        double seconds = (double) time / 1e9;
                        averages[0] += stats[0];
                        averages[1] += stats[1];
                        averages[2] += seconds;

                        //Open spreadsheet to append information
                        workbook = Workbook.createWorkbook(file, Workbook.getWorkbook(file, wbSettings));
                        dataSheet = workbook.getSheet(1);
                        dataSheet.addCell(new Number(1, rowData, iSenones, cfTimes));
                        dataSheet.addCell(new Number(2, rowData, iLDA_MLLT, cfTimes));
                        dataSheet.addCell(new Number(3, rowData, (i + 1), cfTimes));
                        dataSheet.addCell(new Number(4, rowData, stats[0], cfTimesPercent));
                        dataSheet.addCell(new Number(5, rowData, stats[1], cfTimesPercent));
                        dataSheet.addCell(new Number(6, rowData, seconds, cfTimes));
                        rowData++;
                        //Write to the spreadsheet
                        workbook.write();
                        workbook.close();

                        int hours, minutes;
                        hours = (int) seconds / 3600;
                        seconds -= 3600 * hours;
                        minutes = (int) seconds / 60;
                        seconds -= 60 * minutes;
                        //Log
                        _sLogData = "[" + new java.sql.Timestamp(new java.util.Date().getTime()) + "] "
                                + String.format("Senones= %d, LDA MLLT= %d, #%d: SER= %.2f%% WER= %.2f%% (%02dh %02dm %02d.%1ds)",
                                        iSenones, iLDA_MLLT, (i + 1), 100 * stats[0], 100 * stats[1], hours, minutes,
                                        (int) seconds, (int) (10 * (seconds - (int) seconds)));
                        System.out.println(_sLogData);
                        writeLogFile(_sLogData);

                        printElapsedTime(startAll);
                        _sLogData = "[" + new java.sql.Timestamp(new java.util.Date().getTime()) + "] "
                                + String.format("%.2f%% completed\n---", 100.0 * iCurrentScenario / iNumberOfScenarios);
                        System.out.println(_sLogData);
                        writeLogFile(_sLogData);
                    }//for i
                    for (int i = 0; i < averages.length; i++) {
                        averages[i] /= iNumberOfRepetition;
                    }//for i

                    //Open spreadsheet to append information
                    workbook = Workbook.createWorkbook(file, Workbook.getWorkbook(file, wbSettings));
                    averageSheet = workbook.getSheet(0);
                    averageSheet.addCell(new Number(1, rowAverage, iSenones, cfTimes));
                    averageSheet.addCell(new Number(2, rowAverage, iLDA_MLLT, cfTimes));
                    averageSheet.addCell(new Number(3, rowAverage, iNumberOfRepetition, cfTimes));
                    averageSheet.addCell(new Number(4, rowAverage, averages[0], cfTimesPercent));
                    averageSheet.addCell(new Number(5, rowAverage, averages[1], cfTimesPercent));
                    averageSheet.addCell(new Number(6, rowAverage, averages[2], cfTimes));
                    if (iPreviousSenones != iSenones) {
                        averageSheet.addCell(new Number(8, ++rowAverageChart, iSenones, cfTimes));
                        iPreviousSenones = iSenones;
                    }//if
                    averageSheet.addCell(new Number(columnAverage++, rowAverageChart, averages[1], cfTimesPercent));
                    rowAverage++;
                    //Write to the spreadsheet
                    workbook.write();
                    workbook.close();
                    //reset the array
                    for (int i = 0; i < averages.length; i++) {
                        averages[i] = 0;
                    }//for i
                }//for iLDA_MLLT
            }//for iSenones

            //Open spreadsheet to append information
            workbook = Workbook.createWorkbook(file, Workbook.getWorkbook(file, wbSettings));
            averageSheet = workbook.getSheet(0);
            dataSheet = workbook.getSheet(1);

            CellView cv = new CellView();
            cv.setFormat(cfTimes);
            cv.setAutosize(true);
            for (int i = 1; i < 4; i++) {
                dataSheet.setColumnView(i, cv);
                averageSheet.setColumnView(i, cv);
            }//for i

            double seconds = (double) (System.nanoTime() - startAll) / 1e9;
            int hours, minutes;
            hours = (int) seconds / 3600;
            seconds -= 3600 * hours;
            minutes = (int) seconds / 60;
            seconds -= 60 * minutes;

            _sLogData = "[" + new java.sql.Timestamp(new java.util.Date().getTime()) + "] ";
            System.out.println(_sLogData + sMessage);
            sMessage = String.format("Finished all training scenarios in %02dh %02dm %02d.%2ds",
                    hours, minutes, (int) seconds, (int) (100 * (seconds - (int) seconds)));
            writeLogFile(_sLogData);

            averageSheet.addCell(new Label(0, ++rowAverage, sMessage, cfTimes));
            //Write to the spreadsheet
            workbook.write();
            workbook.close();

            _sLogData = "[" + new java.sql.Timestamp(new java.util.Date().getTime()) + "] \n"
                    + "File is: " + outputFile + "\n\n***\n" + sMessage;
            System.out.println(_sLogData);
            writeLogFile(_sLogData);

            JOptionPane.showConfirmDialog(this, sMessage,
                    "Process completed!",
                    JOptionPane.CLOSED_OPTION,
                    JOptionPane.INFORMATION_MESSAGE);
        }//try
        catch (IOException | WriteException | BiffException ex) {
            Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
            writeLogFile(ex);
        }//catch
    }

    private boolean resetLanguageModelFolder() {
        boolean bReturnedValue = false;
        String message = "Do you want to delete all files and \nsub-folders in the Language Model root folder?";
        int answer = JOptionPane.showConfirmDialog(this, message,
                "Language Model root folder",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE);
        if (answer == JOptionPane.YES_OPTION) {
            bReturnedValue = true;
            try {
                //Create the folder in case it is not created yet.
                try {
                    FileUtils.forceMkdir(new File(tfLanguageRootDir.getText()));
                }//try
                catch (IOException e) {
                    Logger.getLogger(MainWindow.class
                            .getName()).log(Level.INFO, "Probably the directory already exists! No need to create it.", e);
                    writeLogFile(e);
                }//catch
                FileUtils.cleanDirectory(new File(tfLanguageRootDir.getText()));

                //Create directories "/etc" and "/wav"
                FileUtils.forceMkdir(new File(tfLanguageRootDir.getText() + DIR_ETC));
                FileUtils.forceMkdir(new File(tfLanguageRootDir.getText() + DIR_WAV));

                btnRestartTraining.setEnabled(false);
            }//try
            catch (IOException ex) {
                Logger.getLogger(MainWindow.class
                        .getName()).log(Level.SEVERE, null, ex);
                writeLogFile(ex);
            }//catch
        }//if

        return bReturnedValue;
    }

    private void configureAndStartTraining() {
        try {
            buildLanguageModelFile();

            //Prepare the training config file "sphinx_train.cfg"
            runCommand("sphinxtrain -t " + _sLanguageModelName + " setup", ckPriorityHigh.isSelected());

            //Apply the selected configuration
            String sDefaultConfig, sNewConfig;
            _sLogData = "[" + new java.sql.Timestamp(new java.util.Date().getTime()) + "] "
                    + "\n---\nConfiguring the sphinx train with:";
            System.out.println(_sLogData);
            writeLogFile(_sLogData);

            File file = new File(tfLanguageRootDir.getText() + DIR_ETC + "/sphinx_train.cfg");
            List<String> fileLines = null;
            int lineIndex = -1;
            try {
                fileLines = FileUtils.readLines(file, STR_ENCODING);
            }//try
            catch (FileNotFoundException e) {
                _sLogData = "File sphinx_train.cfg not found! Giving it some time to be generated by sphinxtrain!\n"
                        + "Trying again after 5 seconds!";
                System.out.println(_sLogData);
                writeLogFile(_sLogData);
                Thread.sleep(5000); //5 seconds
                fileLines = FileUtils.readLines(file, STR_ENCODING);
            }//catch
            if (ckCalculateLDA_MLLT.isSelected()) {
                //Calculate an LDA/MLLT transform?
                sDefaultConfig = "$CFG_LDA_MLLT = 'no';";
                lineIndex = fileLines.indexOf(sDefaultConfig);
                if (lineIndex != -1) {
                    sNewConfig = "$CFG_LDA_MLLT = 'yes';";
                    fileLines.set(lineIndex, sNewConfig + " #" + sDefaultConfig);
                    _sLogData = "| " + sNewConfig;
                } else {
                    _sLogData = "| Error: '" + sDefaultConfig + "' not found!";
                }
                System.out.println(_sLogData);
                writeLogFile(_sLogData);

                //Dimensionality of LDA/MLLT output
                sDefaultConfig = "$CFG_LDA_DIMENSION = 29;";
                lineIndex = fileLines.indexOf(sDefaultConfig);
                if (lineIndex != -1) {
                    sNewConfig = "$CFG_LDA_DIMENSION = " + tfDimensionalityLDA_MLLT.getText() + ";";
                    fileLines.set(lineIndex, sNewConfig + " #" + sDefaultConfig);
                    _sLogData = "| " + sNewConfig;
                } else {
                    _sLogData = "| Error: '" + sDefaultConfig + "' not found!";
                }
                System.out.println(_sLogData);
                writeLogFile(_sLogData);
            }//if

            //Number of tied states (senones) to create in decision-tree clustering
            sDefaultConfig = "$CFG_N_TIED_STATES = 200;";
            lineIndex = fileLines.indexOf(sDefaultConfig);
            if (lineIndex != -1) {
                sNewConfig = "$CFG_N_TIED_STATES = " + tfSenonesNumber.getText() + ";";
                fileLines.set(lineIndex, sNewConfig + " #" + sDefaultConfig);
                _sLogData = "| " + sNewConfig;
            } else {
                _sLogData = "| Error: '" + sDefaultConfig + "' not found!";
            }
            System.out.println(_sLogData);
            writeLogFile(_sLogData);

            //multicore machine
            sDefaultConfig = "$CFG_QUEUE_TYPE = \"Queue\";";
            lineIndex = fileLines.indexOf(sDefaultConfig);
            if (lineIndex != -1) {
                sNewConfig = "$CFG_QUEUE_TYPE = \"Queue::POSIX\";";
                fileLines.set(lineIndex, sNewConfig + " #" + sDefaultConfig);
                _sLogData = "| " + sNewConfig;
            } else {
                _sLogData = "| Error: '" + sDefaultConfig + "' not found!";
            }
            System.out.println(_sLogData);
            writeLogFile(_sLogData);

            // (yes/no) Perform vocal tract length normalization in training.  This
            // will result in a "normalized" model which requires VTLN to be done
            // during decoding as well.
            sDefaultConfig = "$CFG_VTLN = 'no';";
            lineIndex = fileLines.indexOf(sDefaultConfig);
            if (lineIndex != -1) {
                sNewConfig = "$CFG_VTLN = '"
                        + (ckVTLN.isSelected() ? "yes" : "no")
                        + "';";
                fileLines.set(lineIndex, sNewConfig + " #" + sDefaultConfig);
                _sLogData = "| " + sNewConfig;
            } else {
                _sLogData = "| Error: '" + sDefaultConfig + "' not found!";
            }
            System.out.println(_sLogData);
            writeLogFile(_sLogData);

            //(yes/no) Train multiple-gaussian context-independent models (useful
            //for alignment, use 'no' otherwise)
            sDefaultConfig = "$CFG_CI_MGAU = 'no';";
            lineIndex = fileLines.indexOf(sDefaultConfig);
            if (lineIndex != -1) {
                sNewConfig = "$CFG_CI_MGAU = '"
                        + (ckMultipleGaussian.isSelected() ? "yes" : "no")
                        + "';";
                fileLines.set(lineIndex, sNewConfig + " #" + sDefaultConfig);
                _sLogData = "| " + sNewConfig;
            } else {
                _sLogData = "| Error: '" + sDefaultConfig + "' not found!";
            }
            System.out.println(_sLogData);
            writeLogFile(_sLogData);

            //Use force-aligned transcripts (if available) as input to training
            sDefaultConfig = "$CFG_FORCEDALIGN = 'no';";
            lineIndex = fileLines.indexOf(sDefaultConfig);
            if (lineIndex != -1) {
                sNewConfig = "$CFG_FORCEDALIGN = '"
                        + (ckForceAlignTranscript.isSelected() ? "yes" : "no")
                        + "';";
                fileLines.set(lineIndex, sNewConfig + " #" + sDefaultConfig);
                _sLogData = "| " + sNewConfig;
            } else {
                _sLogData = "| Error: '" + sDefaultConfig + "' not found!";
            }
            System.out.println(_sLogData);
            writeLogFile(_sLogData);
            //Save back to the file
            FileUtils.writeLines(file, STR_ENCODING, fileLines);

            //Start the training
            runCommand("sphinxtrain run", ckPriorityHigh.isSelected());

            if (ckOpenTrainingLogInBrowser.isSelected()) {
                File htmlFile = new File(tfLanguageRootDir.getText() + "/" + _sLanguageModelName + ".html");
                Desktop.getDesktop().browse(htmlFile.toURI());
            }//if

            if (ckTestLanguageModel.isSelected()) {
                testLanguageModel();

            }//if
        }//try
        catch (IOException | InterruptedException ex) {
            Logger.getLogger(MainWindow.class
                    .getName()).log(Level.SEVERE, null, ex);
            writeLogFile(ex);
        }//catch
    }

    private double[] configureAndRepeatTraining(int p_iCFG_LDA_DIMENSION,
            int p_iCFG_N_TIED_STATES) {
        double[] returnedValue = new double[2];
        try {

            //Prepare the training config file "sphinx_train.cfg"
            runCommand("sphinxtrain -t " + _sLanguageModelName + " setup", ckPriorityHigh.isSelected());
            Thread.sleep(500); //wait in case the sphinxtrain setup took some time to finish

            //Apply the selected configuration
            String sDefaultConfig, sNewConfig;
            _sLogData = "[" + new java.sql.Timestamp(new java.util.Date().getTime()) + "] "
                    + "\n---\nConfiguring the sphinx train with:";
            System.out.println(_sLogData);
            writeLogFile(_sLogData);

            File file = new File(tfLanguageRootDir.getText() + DIR_ETC + "/sphinx_train.cfg");
            List<String> fileLines = FileUtils.readLines(file, STR_ENCODING);
            if (p_iCFG_LDA_DIMENSION > 0) {
                //Calculate an LDA/MLLT transform?
                sDefaultConfig = "$CFG_LDA_MLLT = 'no';";
                sNewConfig = "$CFG_LDA_MLLT = 'yes';";
                fileLines.set(fileLines.indexOf(sDefaultConfig), sNewConfig + " #");
                _sLogData = "| " + sNewConfig;
                System.out.println(_sLogData);
                writeLogFile(_sLogData);

                //Dimensionality of LDA/MLLT output
                sDefaultConfig = "$CFG_LDA_DIMENSION = 29;";
                sNewConfig = "$CFG_LDA_DIMENSION = " + p_iCFG_LDA_DIMENSION + ";";
                fileLines.set(fileLines.indexOf(sDefaultConfig), sNewConfig + " #");
                _sLogData = "| " + sNewConfig;
                System.out.println(_sLogData);
                writeLogFile(_sLogData);
            }//if

            //Number of tied states (senones) to create in decision-tree clustering
            sDefaultConfig = "$CFG_N_TIED_STATES = 200;";
            sNewConfig = "$CFG_N_TIED_STATES = " + p_iCFG_N_TIED_STATES + ";";
            fileLines.set(fileLines.indexOf(sDefaultConfig), sNewConfig + " #");
            _sLogData = "| " + sNewConfig;
            System.out.println(_sLogData);
            writeLogFile(_sLogData);

            //Save back to the file
            FileUtils.writeLines(file, STR_ENCODING, fileLines);

            _sLogData = "[" + new java.sql.Timestamp(new java.util.Date().getTime()) + "] "
                    + "\n>>> Training in progress... ";
            System.out.println(_sLogData);
            writeLogFile(_sLogData);
            String sOutput = runCommandReturnOutput("sphinxtrain run", ckPriorityHigh.isSelected());
            _sLogData = "[" + new java.sql.Timestamp(new java.util.Date().getTime()) + "] "
                    + "\n>>> Training completed.";
            System.out.println(_sLogData);
            writeLogFile(_sLogData);

            int startIndex = sOutput.indexOf("% (");
            int endIndex = sOutput.indexOf("/", startIndex);
            int startSER = Integer.parseInt(sOutput.substring(startIndex + 3, endIndex));

            startIndex = endIndex++;
            endIndex = sOutput.indexOf(")", startIndex);
            int endSER = Integer.parseInt(sOutput.substring(startIndex + 1, endIndex));

            startIndex = endIndex++;
            startIndex = sOutput.indexOf("% (", startIndex);
            endIndex = sOutput.indexOf("/", startIndex);
            int startWER = Integer.parseInt(sOutput.substring(startIndex + 3, endIndex));

            startIndex = endIndex++;
            endIndex = sOutput.indexOf(")", startIndex);
            int endWER = Integer.parseInt(sOutput.substring(startIndex + 1, endIndex));

            returnedValue[0] = (double) startSER / (double) endSER;
            returnedValue[1] = (double) startWER / (double) endWER;

        }//try
        catch (InterruptedException | IOException | NumberFormatException ex) {
            Logger.getLogger(MainWindow.class
                    .getName()).log(Level.SEVERE, null, ex);
            writeLogFile(ex);
        }//catch

        return returnedValue;
    }

    private void testLanguageModel() {
        String cmd = "/usr/local/bin/pocketsphinx_continuous -inmic yes -hmm "
                + tfLanguageRootDir.getText() + "/model_parameters/" + _sLanguageModelName + ".cd_cont_" + tfSenonesNumber.getText()
                + " -lm "
                + tfLanguageRootDir.getText() + "/etc/" + _sLanguageModelName + ".lm"
                + " -dict "
                + tfLanguageRootDir.getText() + "/etc/" + _sLanguageModelName + ".dic";

        //Copy to clipboard
        Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new StringSelection(cmd), new StringSelection(cmd));

        //runCommandInTerminal(cmd);
        runCommand(cmd, ckPriorityHigh.isSelected());
    }

    /**
     *
     * @param cmd
     * @return the output of the command
     */
    private String runCommandReturnOutput(String cmd,
            boolean runWithHighPriority) {
        String sReturnedValue = "";
        try {
            final Process p;
            ProcessBuilder pb;
            if (runWithHighPriority) {
                pb = new ProcessBuilder("/bin/bash", "-c", "echo \"" + _sRootPassword + "\"| sudo -S nice -n -5 " + cmd);
                pb.directory(new File(tfLanguageRootDir.getText()));
                p = pb.start();
            }//if
            else {
                Runtime r = Runtime.getRuntime();
                p = r.exec(cmd, null, new File(tfLanguageRootDir.getText()));
            }//else
            //Add the process to be destroyed when the parents process is killed
            Runtime.getRuntime().addShutdownHook(new Thread() {
                @Override
                public void run() {
                    p.destroy();
                }
            });
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
            BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
            // read the output from the command
            String s;
            if (_bVerboseMode) {
                _sLogData = "\n-------\nRunning: " + cmd;
                System.out.println(_sLogData);
                writeLogFile(_sLogData);
            }//if
            while ((s = stdInput.readLine()) != null) {
                sReturnedValue += s;
                if (_bVerboseMode) {
                    System.out.println(s);
                    writeLogFile(s);
                }//if
            }//while

            // read any errors from the attempted command
            if (_bVerboseMode) {
                _sLogData = "\n> Errors of the command (if any):";
                System.out.println(_sLogData);
                writeLogFile(_sLogData);
                while ((s = stdError.readLine()) != null) {
                    System.out.println(s);
                    writeLogFile(s);
                }//while
            }//if
        }//try
        catch (IOException ex) {
            Logger.getLogger(MainWindow.class
                    .getName()).log(Level.WARNING, null, ex);
            writeLogFile(ex);
        }//catch

        return sReturnedValue;
    }

    private void runCommand(String cmd, boolean runWithHighPriority) {
        try {
            final Process p;
            ProcessBuilder pb;
            if (runWithHighPriority) {
                pb = new ProcessBuilder("/bin/bash", "-c", "echo \"" + _sRootPassword + "\"| sudo -S nice -n -5 " + cmd);
                pb.directory(new File(tfLanguageRootDir.getText()));
                p = pb.start();
            }//if
            else {
                Runtime r = Runtime.getRuntime();
                p = r.exec(cmd, null, new File(tfLanguageRootDir.getText()));
            }//else
            //Add the process to be destroyed when the parents process is killed
            Runtime.getRuntime().addShutdownHook(new Thread() {
                @Override
                public void run() {
                    p.destroy();
                }
            });

            if (_bVerboseMode) {
                BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
                BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
                // read the output from the command
                String s;
                _sLogData = "\n-------\nRunning: " + cmd;
                System.out.println(_sLogData);
                writeLogFile(_sLogData);
                while ((s = stdInput.readLine()) != null) {
                    System.out.println(s);
                    writeLogFile(s);
                }//while

                // read any errors from the attempted command
                _sLogData = "\n> Errors of the command (if any):";
                System.out.println(_sLogData);
                writeLogFile(_sLogData);
                while ((s = stdError.readLine()) != null) {
                    System.out.println(s);
                    writeLogFile(s);
                }//while
            }//if
        }//try
        catch (IOException ex) {
            Logger.getLogger(MainWindow.class
                    .getName()).log(Level.WARNING, null, ex);
            writeLogFile(ex);
        }//catch
    }

    private void sort(DefaultListModel dlm) {
        if (dlm.getSize() < 2) {
            return;
        }//if
        try {
            for (int i = 1; i < dlm.getSize(); i++) {
                if (Integer.parseInt(dlm.getElementAt(i).toString()) < Integer.parseInt(dlm.getElementAt(i - 1).toString())) {
                    Object tmp = dlm.getElementAt(i);
                    dlm.setElementAt(dlm.getElementAt(i - 1), i);
                    dlm.setElementAt(tmp, i - 1);

                }//if
            }//for i
        }//try
        catch (NumberFormatException ex) {
            Logger.getLogger(MainWindow.class
                    .getName()).log(Level.WARNING, null, ex);
            writeLogFile(ex);
        }//catch
    }

    /**
     * Generates the required files for Sphinx4 Language model format
     *
     * @param p_bResetDB do we reset the database
     * @param p_bRegeneratePhonemes de we regenerate the phonemes because we
     * (manually) updated the transcriptions
     */
    private void prepareFiles(boolean p_bResetDB, boolean p_bRegeneratePhonemes) {
        ArrayList<Integer> alSelectedChapters = new ArrayList<>();
        //Add the "الإستعاذة" alSelectedChapters.add(0);
        for (int i = 0; i < _dlmSelectedChapters.getSize(); i++) {
            alSelectedChapters.add(Integer.parseInt(_dlmSelectedChapters.getElementAt(i).toString()));
        }//for i

        PhonemeDictionaryGenerator gen;
        gen = new PhonemeDictionaryGenerator(alSelectedChapters, p_bResetDB, _bVerboseMode);
        try {
            gen.mapPhonemes(true, p_bRegeneratePhonemes);

            //Update the next values only if needed.
            String sSql = "UPDATE `config` SET `index_next_word` = " + gen.getIdNextWord()
                    + ", `index_next_phoneme` = " + gen.getIdNextPhoneme();
            if (_bVerboseMode) {
                System.out.println(sSql);
                writeLogFile(sSql);
            }//if
            gen._statement.executeUpdate(sSql);

            _sLogData = "[" + new java.sql.Timestamp(new java.util.Date().getTime()) + "] ";
            System.out.println(_sLogData);
            writeLogFile(_sLogData);
            String sMsg = "Done populating the database with the phonetic dictionary (word mapping)!";
            System.out.println(sMsg);
            writeLogFile(sMsg);
            sMsg = (gen.getIdNextWord() - 1) + " words, and " + (gen.getIdNextPhoneme() - 1) + " phonemes.";
            System.out.println(sMsg);
            writeLogFile(sMsg);

            //Generate the text files from the generated database
            _sLogData = ">>> Starting generating all main files";
            System.out.println(_sLogData);
            writeLogFile(_sLogData);
            generateMainFiles(p_bRegeneratePhonemes);

        }//try
        catch (SQLException | IOException ex) {
            Logger.getLogger(MainWindow.class
                    .getName()).log(Level.SEVERE, "prepareFiles()", ex);
            writeLogFile(ex);
        }//catch
    }

    /**
     * Copy chosen chapters from all folders and ensure all files have a unique
     * name. The all Ayat files is generated before generating the language
     * model. The main files are: 1/ Ayah files for each of the available
     * reciters 2/ fileids file with the list of all wav files 2/ transcription
     * file including the transcription of each wav file. 4/ phonemes' list 5/
     * dictionary of words and their phonemes.
     *
     * @param p_bRegeneratePhonemes Are we regenerating the phonemes hence not
     * needing to regenerate the WAV files?
     */
    private void generateMainFiles(boolean p_bRegeneratePhonemes) throws IOException, SQLException {
        String sTrainingFileidsFilename;
        String sTestFileidsFilename;
        String sTrainingTranscriptionFilename;
        String sTestTranscriptionFilename;
        String sFillerFilename;
        String sDictionaryFilename;
        String sPhonemesFilename;
        String sAllAyatFilename;
        //sContextCueFilename = tfLanguageRootDir.getText() + DIR_ETC + "/"
        //                      + _sLanguageModelName + EXT_CONTEXTCUES;
        sAllAyatFilename = tfLanguageRootDir.getText() + DIR_ETC + "/"
                + _sLanguageModelName;
        sTestFileidsFilename = tfLanguageRootDir.getText() + DIR_ETC + "/"
                + _sLanguageModelName + POSTFIX_TEST + EXT_FILEIDS;
        sTestTranscriptionFilename = tfLanguageRootDir.getText() + DIR_ETC + "/"
                + _sLanguageModelName + POSTFIX_TEST + EXT_TRANSCRIPTION;
        sTrainingFileidsFilename = tfLanguageRootDir.getText() + DIR_ETC + "/"
                + _sLanguageModelName + POSTFIX_TRAINING + EXT_FILEIDS;
        sTrainingTranscriptionFilename = tfLanguageRootDir.getText() + DIR_ETC + "/"
                + _sLanguageModelName + POSTFIX_TRAINING + EXT_TRANSCRIPTION;
        sFillerFilename = tfLanguageRootDir.getText() + DIR_ETC + "/"
                + _sLanguageModelName + EXT_FILLER;
        sDictionaryFilename = tfLanguageRootDir.getText() + DIR_ETC + "/"
                + _sLanguageModelName + EXT_DICTIONARY;
        sPhonemesFilename = tfLanguageRootDir.getText() + DIR_ETC + "/"
                + _sLanguageModelName + EXT_PHONEMES;

        BufferedWriter bwTrainingFileids, bwTrainingTranscription, bwTestFileids, bwTestTranscription, bufferedWriter, bwDictionary;

        /*
         * //Create the Context Cues file bufferedWriter = new
         * BufferedWriter(new FileWriter(sContextCueFilename));
         * bufferedWriter.write("<s>\n"); bufferedWriter.close(); if
         * (_bVerboseMode) { System.out.print("\nContext Cues file created: " +
         * sContextCueFilename); }//if
         */
        //Create the Filler file
        bufferedWriter = new BufferedWriter(new FileWriter(sFillerFilename));
        String sSql = "SELECT * FROM `fillers`";
        if (_bVerboseMode) {
            System.out.println(sSql);
            writeLogFile(sSql);
        }//if
        _resultSet = _statement.executeQuery(sSql);
        boolean bResultsExist = false;
        //TODO: might want to put the "value" in a List to add later to the code below to generate the list of phonemes
        while (_resultSet.next()) {
            //the fillers' table is not empty populate the file from the database
            bResultsExist = true;
            bufferedWriter.write(_resultSet.getString("tag") + "   " + _resultSet.getString("value") + "\n");
        }//while
        _resultSet.close();
        //Empty table?
        if (!bResultsExist) {
            //Add the default list to the database and the file
            sSql = "INSERT INTO `fillers`(`tag`, `value`) VALUES "
                    + "(\"<s>\", \"SIL\"), (\"</s>\", \"SIL\"), (\"<sil>\", \"SIL\")";
            if (_bVerboseMode) {
                System.out.println(sSql);
                writeLogFile(sSql);
            }//if
            _statement.executeUpdate(sSql);
            //Add to the file
            bufferedWriter.write("<s>     SIL\n");
            bufferedWriter.write("</s>    SIL\n");
            bufferedWriter.write("<sil>   SIL\n");
        }//else
        bufferedWriter.close();
        if (_bVerboseMode) {
            _sLogData = "\nFiller file created: " + sFillerFilename;
            System.out.println(_sLogData);
            writeLogFile(_sLogData);
        }//if

        //Create the phonemes' list file based on the phonemes present in the selected chapters
        bufferedWriter = new BufferedWriter(new FileWriter(sPhonemesFilename));
        bufferedWriter.write("SIL\n");
        sSql = "SELECT DISTINCT phoneme "
                + "FROM `phonemes`, `word_has_phonemes`, `words`, `quran_text_has_words`, `quran_text` "
                + "WHERE `phonemes`.`index` = `word_has_phonemes`.`id_phonemes` AND "
                + "`word_has_phonemes`.`id_words` = `words`.`index` AND "
                + "`words`.`index` = `quran_text_has_words`.`id_words` AND "
                + "`quran_text_has_words`.`id_quran_text` = `quran_text`.`index` AND "
                + "`quran_text`.`sura` IN (";
        for (int c = 0; c < _dlmSelectedChapters.size(); c++) {
            sSql += _dlmSelectedChapters.getElementAt(c).toString();
            if ((c + 1) < _dlmSelectedChapters.size()) {
                sSql += ", ";
            }//if
        }//for c
        sSql += ")";

        if (_bVerboseMode) {
            System.out.println(sSql);
            writeLogFile(sSql);
        }//if
        _resultSet = _statement.executeQuery(sSql);
        while (_resultSet.next()) {
            bufferedWriter.write(_resultSet.getString("phoneme") + "\n");
        }//while
        _resultSet.close();
        bufferedWriter.close();

        if (!p_bRegeneratePhonemes) {
            //For each folder in the Holy Quran root folder
            bwTestFileids = new BufferedWriter(new FileWriter(sTestFileidsFilename));
            bwTestTranscription = new BufferedWriter(new FileWriter(sTestTranscriptionFilename));
            bwTrainingFileids = new BufferedWriter(new FileWriter(sTrainingFileidsFilename));
            bwTrainingTranscription = new BufferedWriter(new FileWriter(sTrainingTranscriptionFilename));
            int iIdCounter = 0;
            File[] filesQuranRootDir = new File(tfQuranRootDir.getText()).listFiles();
            File fileWavDir = new File(tfLanguageRootDir.getText() + DIR_WAV);
            FileUtils.cleanDirectory(fileWavDir);
            for (File quranDir : filesQuranRootDir) {
                if (quranDir.isDirectory()) {
                    //Create a new folder with the same name in the Language model root dir.
                    File fileNewQuranDir = new File(fileWavDir, quranDir.getName());
                    if (fileNewQuranDir.mkdir()) {
                        if (_bVerboseMode) {
                            _sLogData = "\n---\nNew Folder Created: " + quranDir.getName()
                                    + "\n" + sSql;
                            System.out.println(_sLogData);
                            writeLogFile(_sLogData);
                        }//if
                        //This is a new recitor's folder. Add the reciter to the database if it is not in the database already
                        int iIdRecitor = -1;
                        sSql = "SELECT * FROM `recitors` WHERE `name` = \"" + quranDir.getName() + "\"";
                        if (_bVerboseMode) {
                            System.out.println(sSql);
                            writeLogFile(sSql);
                        }
                        _resultSet = _statement.executeQuery(sSql);
                        if (_resultSet.next()) {
                            iIdRecitor = _resultSet.getInt("index");
                        }//if
                        _resultSet.close();
                        if (iIdRecitor == -1) {
                            //Recitor not found in the db, let's add it
                            sSql = "INSERT INTO `recitors`(`name`) VALUES (\"" + quranDir.getName() + "\")";
                            if (_bVerboseMode) {
                                System.out.println(sSql);
                                writeLogFile(sSql);
                            }//if
                            _statement.executeUpdate(sSql, Statement.RETURN_GENERATED_KEYS);
                            //Retrieving the id of the new entry
                            _resultSet = _statement.getGeneratedKeys();
                            _resultSet.next();
                            //Ids used in the database
                            iIdRecitor = _resultSet.getInt(1);
                            _resultSet.close();
                        }//if

                        int iIdQuranText = -1, iIdTranscription = -1, iIdFillerStart = -1, iIdFillerEnd = -1;

                        //Get the IDs of the starting and ending fillers
                        sSql = "SELECT id1, id2 "
                                + "FROM ( SELECT `index` as id1 FROM `fillers` WHERE `tag` = \"<s>\" ) t1, "
                                + "( SELECT `index` as id2 FROM `fillers` WHERE `tag` = \"</s>\") t2";
                        if (_bVerboseMode) {
                            System.out.println(sSql);
                            writeLogFile(sSql);
                        }//if
                        _resultSet = _statement.executeQuery(sSql);
                        if (_resultSet.next()) {
                            iIdFillerStart = _resultSet.getInt("id1");
                            iIdFillerEnd = _resultSet.getInt("id2");
                        }//if
                        _resultSet.close();

                        for (File fileAyahFile : quranDir.listFiles()) {
                            if (fileAyahFile.isFile() && _dlmSelectedChapters.indexOf(fileAyahFile.getName().substring(0, 3)) != -1) {
                                try {
                                    //Convert to wav
                                    String extention = fileAyahFile.getName().substring(fileAyahFile.getName().lastIndexOf("."));
                                    if (extention.equals(".mp3")) {
                                        String newQuranFilename = fileAyahFile.getName().substring(0, fileAyahFile.getName().lastIndexOf("."));
                                        String sIdSurah = newQuranFilename.substring(0, 3);
                                        String sIdAyah = null;
                                        try {
                                            sIdAyah = newQuranFilename.substring(3, 6);
                                        }//try
                                        catch (Exception e) {
                                            _sLogData = e.getMessage()
                                                    + "\nnewQuranFilename = " + newQuranFilename
                                                    + "\nfileAyahFile = " + fileAyahFile;
                                            System.out.println(_sLogData);
                                            writeLogFile(_sLogData);
                                            continue;
                                        }
                                        if (_dlmSelectedChapters.contains(sIdSurah)) {
                                            //Get the id of the Ayah to save it in the transcription table
                                            sSql = "SELECT `index` FROM `quran_text` WHERE `sura` = "
                                                    + Integer.parseInt(sIdSurah) + " AND `aya` = " + Integer.parseInt(sIdAyah);
                                            if (_bVerboseMode) {
                                                System.out.println(sSql);
                                                writeLogFile(sSql);
                                            }//if
                                            _resultSet = _statement.executeQuery(sSql);
                                            if (_resultSet.next()) {
                                                iIdQuranText = _resultSet.getInt("index");
                                            }//if
                                            _resultSet.close();

                                            //Check if the file's transcription is not already in the database
                                            iIdTranscription = -1;
                                            sSql = "SELECT `index` FROM `transcriptions` WHERE `id_recitors` = "
                                                    + iIdRecitor + " AND `id_quran_text` = " + iIdQuranText;
                                            if (_bVerboseMode) {
                                                System.out.println(sSql);
                                                writeLogFile(sSql);
                                            }//if
                                            _resultSet = _statement.executeQuery(sSql);
                                            if (_resultSet.next()) {
                                                iIdTranscription = _resultSet.getInt("index");
                                            }//if
                                            _resultSet.close();

                                            newQuranFilename += "_id" + String.format("%015d", iIdCounter++);
                                            newQuranFilename += EXT_WAV;
                                            if (extention.toLowerCase().equals(EXT_MP3)) {
                                                convertMP3toWAV(fileAyahFile.getCanonicalPath(),
                                                        fileNewQuranDir.getCanonicalPath() + "/" + newQuranFilename);
                                            }//if
                                            if (_bVerboseMode) {
                                                _sLogData = "\n>>> File Created: " + newQuranFilename;
                                                System.out.println(_sLogData);
                                                writeLogFile(_sLogData);
                                            }//if

                                            //Add the relative path to the fileids file
                                            long lValueForModuloDivision = Math.round(100 / _dPercentageOfTestFiles) * 100;
                                            String sTmp = fileNewQuranDir.getCanonicalPath();
                                            sTmp = sTmp.substring(sTmp.lastIndexOf(_sLanguageModelName + DIR_WAV + "/") + (_sLanguageModelName + DIR_WAV + "/").length());
                                            sTmp += "/" + newQuranFilename.substring(0, newQuranFilename.lastIndexOf(EXT_WAV)) + "\n";
                                            if ((100 * iIdCounter) % lValueForModuloDivision == 0) {
                                                bwTestFileids.write(sTmp);
                                                if (_bUseTestForTraining) {
                                                    bwTrainingFileids.write(sTmp);
                                                }//if
                                            }//if
                                            else {
                                                bwTrainingFileids.write(sTmp);
                                            }//else
                                            if (_bVerboseMode) {
                                                _sLogData = "> and added to " + EXT_FILEIDS;
                                                System.out.println(_sLogData);
                                                writeLogFile(_sLogData);
                                            }//if

                                            String sAyah = "";
                                            if (iIdTranscription == -1) {
                                                //Add the corresponding transcription for the Holy Quran Ayah
                                                sSql = "INSERT INTO `transcriptions`(`id_quran_text`, `id_recitors`) VALUES ("
                                                        + iIdQuranText + ", " + iIdRecitor + ")";
                                                if (_bVerboseMode) {
                                                    System.out.println(sSql);
                                                    writeLogFile(sSql);
                                                }//if
                                                _statement.executeUpdate(sSql, Statement.RETURN_GENERATED_KEYS);
                                                //Retrieving the id of the new entry
                                                _resultSet = _statement.getGeneratedKeys();
                                                _resultSet.next();
                                                iIdTranscription = _resultSet.getInt(1);

                                                int iPositionInTranscription = 1;
                                                sSql = "SELECT `words`.`index`, `words`.`word`"
                                                        + " FROM `words`, `quran_text_has_words`, `quran_text`"
                                                        + " WHERE `words`.`index` = `quran_text_has_words`.`id_words`"
                                                        + " AND `quran_text_has_words`.`id_quran_text` = `quran_text`.`index`"
                                                        + " AND `quran_text`.`sura` = " + Integer.parseInt(sIdSurah)
                                                        + " AND `quran_text`.`aya` = " + Integer.parseInt(sIdAyah)
                                                        + " ORDER BY `quran_text_has_words`.`word_position`";
                                                if (_bVerboseMode) {
                                                    System.out.println(sSql);
                                                    writeLogFile(sSql);
                                                }//if
                                                _resultSet = _statement.executeQuery(sSql);
                                                sSql = "INSERT INTO `transcription_has_words_and_fillers`"
                                                        + "(`id_transcriptions`, `position`, `id_words`, `id_fillers`) VALUES ";
                                                sSql += String.format("(%d, %d, null, %d)", iIdTranscription, iPositionInTranscription++, iIdFillerStart);
                                                while (_resultSet.next()) {
                                                    sAyah += _resultSet.getString("word") + " ";
                                                    //prepare the sql for the inserts in table transcription_has_words_and_fillers
                                                    sSql += String.format(", (%d, %d, %d, null)", iIdTranscription, iPositionInTranscription++, _resultSet.getInt("index"));
                                                }//while
                                                _resultSet.close();
                                                sSql += String.format(", (%d, %d, null, %d)", iIdTranscription, iPositionInTranscription++, iIdFillerEnd);
                                                if (_bVerboseMode) {
                                                    System.out.println(sSql);
                                                    writeLogFile(sSql);
                                                }//if
                                                _statement.executeUpdate(sSql);
                                            }//if
                                            else {
                                                //Extract the text of the Ayah that has a transcription
                                                //FIXME: Need to load fewer records at a time since it maes the server drop the connection while the client is still waiting for it to reply.
                                                //FIXME: need to add to the select statement: limit <startIndex>, <NumberOfRecords>;
                                                sSql = "SELECT * FROM `load_transcription`"
                                                        + " WHERE `index` = " + iIdTranscription;
                                                if (_bVerboseMode) {
                                                    System.out.println("\n" + sSql);
                                                    writeLogFile("\n" + sSql);
                                                }//if
                                                _resultSet = _statement.executeQuery(sSql);
                                                String sTranscription = "";
                                                while (_resultSet.next()) {
                                                    if (_resultSet.getString("type").equals("f") && _resultSet.getInt("id") < 3) {
                                                    }//if
                                                    else {
                                                        sTranscription += _resultSet.getString("token") + " ";
                                                    }//else
                                                }//while
                                                _resultSet.close();
                                                sAyah = sTranscription;
                                            }//else
                                            sAyah = sAyah.trim();
                                            if ((100 * iIdCounter) % lValueForModuloDivision == 0) {
                                                bwTestTranscription.write("<s> " + sAyah + " </s> (" + newQuranFilename.substring(0, newQuranFilename.lastIndexOf(EXT_WAV)) + ")\n");
                                                if (_bUseTestForTraining) {
                                                    bwTrainingTranscription.write("<s> " + sAyah + " </s> (" + newQuranFilename.substring(0, newQuranFilename.lastIndexOf(EXT_WAV)) + ")\n");
                                                    if (_bVerboseMode) {
                                                        _sLogData = "> and " + EXT_TRANSCRIPTION + " (for training and testing)";
                                                        System.out.println(_sLogData);
                                                        writeLogFile(_sLogData);
                                                    }//if
                                                }//if
                                                else if (_bVerboseMode) {
                                                    _sLogData = " (for testing only) ";
                                                    System.out.println(_sLogData);
                                                    writeLogFile(_sLogData);
                                                }//elseif
                                            }//if
                                            else {
                                                bwTrainingTranscription.write("<s> " + sAyah + " </s> (" + newQuranFilename.substring(0, newQuranFilename.lastIndexOf(EXT_WAV)) + ")\n");
                                                if (_bVerboseMode) {
                                                    _sLogData = "> and " + EXT_TRANSCRIPTION + "(from training only)";
                                                    System.out.println(_sLogData);
                                                    writeLogFile(_sLogData);
                                                }//if
                                            }//else
                                        }//if
                                    }//if
                                }//try
                                catch (IOException | NumberFormatException | SQLException ex) {
                                    Logger.getLogger(MainWindow.class
                                            .getName()).log(Level.SEVERE, null, ex);
                                    writeLogFile(ex);
                                    JOptionPane.showMessageDialog(this, ex.getMessage(), ex.getClass().getName(), JOptionPane.ERROR_MESSAGE);
                                    throw ex;
                                }//catch
                            }//if
                        }//for each
                    }//if
                }//if
            }//for each

            //closing opened files
            bwTestFileids.close();
            bwTestTranscription.close();
            bwTrainingFileids.close();
            bwTrainingTranscription.close();
            _sLogData = "\n---\n >>> Holy Quran Folders Creation Completed.";
            System.out.println(_sLogData);
            writeLogFile(_sLogData);
        }//if

        //Prepare for the dictionary
        boolean bFirstLine = true;
        String sConditionChapters = "("; // For the query for generating the dictionary file
        bwDictionary = new BufferedWriter(new FileWriter(sDictionaryFilename));
        try (Statement st = _connection.createStatement()) {
            for (int i = 0; i < _dlmSelectedChapters.getSize(); i++) {
                sConditionChapters += _dlmSelectedChapters.getElementAt(i);
                if (i < _dlmSelectedChapters.getSize() - 1) {
                    sConditionChapters += ", ";
                }//if
            }//for
            sConditionChapters += ")";

            sSql = "SELECT `word_has_phonemes`.`index`, word,  phoneme_position, phoneme "
                    + "FROM word_has_phonemes, phonemes, words "
                    + "WHERE word_has_phonemes.id_words = words.`index` AND word_has_phonemes.id_phonemes = phonemes.`index` "
                    + "AND `words`.`index` IN ( SELECT  id_words FROM quran_text_has_words, quran_text WHERE id_quran_text = quran_text.`index` AND `sura` IN "
                    + sConditionChapters + " ) "
                    + "ORDER BY `words`.`index`, `phoneme_position`";
            if (_bVerboseMode) {
                System.out.println("\n" + sSql);
                writeLogFile("\n" + sSql);
            }//if
            try (ResultSet rs = st.executeQuery(sSql)) {
                String sWord, sPreviousWord = null;
                while (rs.next()) {
                    sWord = rs.getString("word");
                    if (sWord.equals(sPreviousWord)) {
                        bwDictionary.write(" " + rs.getString("phoneme"));
                    }//if
                    else {
                        //New word
                        if (bFirstLine) {
                            bwDictionary.write(sWord + "        " + rs.getString("phoneme"));
                            bFirstLine = false;
                        }//if
                        else {
                            bwDictionary.write("\n" + sWord + "        " + rs.getString("phoneme"));
                        }//else
                        sPreviousWord = sWord;
                    }//else
                }//while
            }//try rs
        }//try st
        if (_bVerboseMode) {
            _sLogData = "\nDictionary file created: " + sDictionaryFilename;
            System.out.println(_sLogData);
            writeLogFile(_sLogData);
        }//if
        bwDictionary.close();

        //All Ayat file
        try {
            bufferedWriter = new BufferedWriter(new FileWriter(sAllAyatFilename));
            Set<String> set = new LinkedHashSet<>();
            String sTranscription = "";
            for (int i = 0; i < _dlmSelectedChapters.getSize(); i++) {
                sSql = "SELECT * FROM `load_transcription`"
                        + " WHERE `sura` = " + _dlmSelectedChapters.getElementAt(i);
                if (_bVerboseMode) {
                    _sLogData = "\n" + sSql;
                    System.out.println(_sLogData);
                    writeLogFile(_sLogData);
                }//if
                _resultSet = _statement.executeQuery(sSql);
                while (_resultSet.next()) {
                    if (_resultSet.getString("type").equals("f") && _resultSet.getInt("id") == 2) {
                        set.add(sTranscription + _resultSet.getString("token"));
                        sTranscription = "";
                    }//if
                    else {
                        sTranscription += _resultSet.getString("token") + " ";
                    }//else
                }//while
                _resultSet.close();
            }//for i

            //Write to the file
            for (String s : set) {
                bufferedWriter.write(s + "\n");
            }//foreach
            bufferedWriter.close();
        }//try
        catch (IOException | SQLException e) {
            Logger.getLogger(MainWindow.class
                    .getName()).log(Level.SEVERE, null, e);
            writeLogFile(e);
            JOptionPane.showMessageDialog(this, e.getMessage(), e.getClass().getName(), JOptionPane.ERROR_MESSAGE);
            throw e;
        }//catch
        _sLogData = "done generating main files.";
        System.out.println(_sLogData);
        writeLogFile(_sLogData);

    }

    /**
     * Call sphinx-lm-builder to build the language model
     *
     * @return Exception that was raised or null if all went fine.
     */
    private void buildLanguageModelFile() {
        String sAllAyatFilename = tfLanguageRootDir.getText() + DIR_ETC + "/"
                + _sLanguageModelName;
        _sLogData = "\nLet's call sphinx-lm-builder... ";
        System.out.println(_sLogData);
        writeLogFile(_sLogData);
        runCommand("sphinx-lm-builder " + sAllAyatFilename, ckPriorityHigh.isSelected());
    }

    /**
     * Converts an mp3 file to a wav file with a sampling rate of 16kbps by
     * using 1 channel (mono) by using only the left channel.
     *
     * The old code used ffmpeg by default but caused some file not being
     * converted since Ubuntu was upgraded to 16.04. Now using SoX
     * (http://sox.sourceforge.net/sox.html).
     *
     * Execute the bash script: old code that produced some low quality audio by
     * merging the 2 channels:
     * <code>ffmpeg -i "$f" -acodec pcm_s16le -ac 1 -ar 16000 "${f%.mp3}.wav"</code>
     * The new code just discards one channel to get a mono audio file as in
     * https://trac.ffmpeg.org/wiki/AudioChannelManipulation For right: use
     * -map_channel 0.0.1 as in:
     * <code>ffmpeg -i "$f" -acodec pcm_s16le -map_channel 0.0.1 -ar 16000 "${f%.mp3}.wav"</code>
     * However, for some files, only the left is available:
     * <code>ffmpeg -i "$f" -acodec pcm_s16le -map_channel 0.0.0 -ar 16000 "${f%.mp3}.wav"</code>
     *
     * @param source mp3 file
     * @param target wav file
     * @param useSoX true means that sox is used for conversion, false uses
     * ffmpeg.
     * @param startSilence the silence duration to add to the BEGINNING of the
     * file, if sox is used.
     * @param endSilence the silence duration to add to the END of the file, if
     * sox is used.
     */
    private void convertMP3toWAV(String source, String target, boolean useSoX,
            double startSilence, double endSilence) {
        try {
            String cmd;
            if (useSoX) {
                cmd = "sox " + source + " " + target + " rate 16000 remix 1";
                if (startSilence != 0 || endSilence != 0) {
                    cmd += " pad " + (startSilence > 0 ? startSilence : 0);
                    cmd += " " + (endSilence > 0 ? endSilence : 0);
                }//if
            }//if
            else {
                //Getting the left channel since some files doesn not have a right channel
                cmd = "ffmpeg -i " + source + " -acodec pcm_s16le -map_channel 0.0.0 -ar 16000 " + target;
            }
            if (_bVerboseMode) {
                System.out.println(cmd);
                writeLogFile(cmd);
            }//if
            Runtime.getRuntime().exec(cmd);
        }//try
        catch (IOException ex) {
            Logger.getLogger(MainWindow.class
                    .getName()).log(Level.SEVERE, null, ex);
            writeLogFile(ex);
        }//catch
    }

    /**
     * Convert mp3 to wav without adding any silences to the beginning or end of
     * the file
     *
     * @param source mp3
     * @param target wav
     * @param useSoX true: using sox, false: using ffmpeg
     */
    private void convertMP3toWAV(String source, String target, boolean useSoX) {
        convertMP3toWAV(source, target, useSoX, 0, 0);
    }

    /**
     * Using SoX by default to convert mp3 files to Sphinx's wav.
     *
     * @param source mp3 input
     * @param target wav output
     */
    private void convertMP3toWAV(String source, String target) {
        convertMP3toWAV(source, target, true, 0, 0);
    }

    private void showFolderChooser(String title, JTextField textField) {
        JFileChooser chooser = new JFileChooser();
        chooser.setCurrentDirectory(new java.io.File("."));
        chooser.setDialogTitle(title);
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.setAcceptAllFileFilterUsed(false);

        if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            textField.setText(chooser.getCurrentDirectory().getAbsolutePath() + "/" + chooser.getSelectedFile().getName());
        }//if
    }

    private void showFileChooser(String title, JTextField textField) {
        JFileChooser chooser = new JFileChooser();
        chooser.setCurrentDirectory(new java.io.File("."));
        chooser.setDialogTitle(title);
        chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);

        if (chooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
            textField.setText(chooser.getCurrentDirectory().getAbsolutePath() + "/" + chooser.getSelectedFile().getName());
        }//if
    }

    private void update_LDA_MLLT_GUI() {
        tfLDA_MLLT_From.setEnabled(rbLDAMLLT_Range.isSelected());
        tfLDA_MLLT_To.setEnabled(rbLDAMLLT_Range.isSelected());
        tfLDA_MLLT_Increment.setEnabled(rbLDAMLLT_Range.isSelected());
        tfLDA_MLLT_List.setEnabled(!rbLDAMLLT_Range.isSelected());
    }

    private void updateSenonesGUI() {
        tfSenones_From.setEnabled(rbSenonesRange.isSelected());
        tfSenones_To.setEnabled(rbSenonesRange.isSelected());
        tfSenones_Increment.setEnabled(rbSenonesRange.isSelected());
        tfSenones_List.setEnabled(!rbSenonesRange.isSelected());
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /*
         * Set the Nimbus look and feel
         */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /*
         * If Nimbus (introduced in Java SE 6) is not available, stay with the
         * default look and feel. For details see
         * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("GTK+".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainWindow.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        //</editor-fold>

        /*
         * Create and display the form
         */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new MainWindow().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup bgDimensions;
    private javax.swing.ButtonGroup bgSenones;
    private javax.swing.JButton btnBrowseAcousticDir;
    private javax.swing.JButton btnBrowseQuranDir;
    private javax.swing.JButton btnDeselect;
    private javax.swing.JButton btnDeselectAll;
    private javax.swing.JButton btnGenerate;
    private javax.swing.JButton btnGenerateSpreadsheet;
    private javax.swing.JButton btnResetAMFolder;
    javax.swing.JButton btnRestartTraining;
    private javax.swing.JButton btnSelect;
    private javax.swing.JButton btnSelectAll;
    private javax.swing.JButton btnShowTranscriptionAligner;
    private javax.swing.JButton btnTestLanguageModel;
    private javax.swing.JCheckBox ckAskBeforeTrain;
    private javax.swing.JCheckBox ckCalculateLDA_MLLT;
    private javax.swing.JCheckBox ckForceAlignTranscript;
    private javax.swing.JCheckBox ckLDA_MLLT;
    private javax.swing.JCheckBox ckMultipleGaussian;
    private javax.swing.JCheckBox ckOpenTrainingLogInBrowser;
    private javax.swing.JCheckBox ckOpenTranscriptionAligner;
    private javax.swing.JCheckBox ckPriorityHigh;
    private javax.swing.JCheckBox ckSenones;
    private javax.swing.JCheckBox ckTestLanguageModel;
    private javax.swing.JCheckBox ckUseTestForTraining;
    private javax.swing.JCheckBox ckVTLN;
    private javax.swing.JCheckBox ckVerboseMode;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JLabel lblDimensionalityLDA_MLLT;
    private javax.swing.JLabel lblElapsed;
    private javax.swing.JList lstAllChapters;
    private javax.swing.JList lstSelectedChapters;
    private javax.swing.JPanel pTestingScenarios;
    private javax.swing.JProgressBar pbProgress;
    private javax.swing.JRadioButton rbLDAMLLT_Range;
    private javax.swing.JRadioButton rbLDA_MLLT_List;
    private javax.swing.JRadioButton rbSenonesList;
    private javax.swing.JRadioButton rbSenonesRange;
    private javax.swing.JTextField tfDimensionalityLDA_MLLT;
    private javax.swing.JTextField tfLDA_MLLT_From;
    private javax.swing.JTextField tfLDA_MLLT_Increment;
    private javax.swing.JTextField tfLDA_MLLT_List;
    private javax.swing.JTextField tfLDA_MLLT_To;
    private javax.swing.JTextField tfLanguageRootDir;
    private javax.swing.JTextField tfNumberOfRepetition;
    private javax.swing.JTextField tfPercentageOfTestFiles;
    private javax.swing.JTextField tfQuranRootDir;
    private javax.swing.JTextField tfSenonesNumber;
    private javax.swing.JTextField tfSenones_From;
    private javax.swing.JTextField tfSenones_Increment;
    private javax.swing.JTextField tfSenones_List;
    private javax.swing.JTextField tfSenones_To;
    // End of variables declaration//GEN-END:variables

}

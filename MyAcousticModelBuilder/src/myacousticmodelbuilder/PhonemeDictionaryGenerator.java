/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myacousticmodelbuilder;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Mohamed Y. El Amrani
 */
public class PhonemeDictionaryGenerator implements ILogFile {

    private ArrayList<Integer> _alChapters;
    private ArrayList<Integer> _alFinished; //used to track finished threads
    private int _iIdNextPhoneme;
    private int _iIdNextWord;
    private int _indexSura;
    private String _sLogData;
    final String STR_CONNECTION = "jdbc:mysql://localhost/quran?useSSL=false&useUnicode=yes&characterEncoding=UTF-8&user=user_my_quran&password=123456789";
    final String STR_EMPTY = "";
    final String STR_HARAKA_DAMMAH = "ُ";
    final String STR_HARAKA_FATHAH = "َ";
    final String STR_HARAKA_KASRAH = "ِ";
    final String STR_HARAKA_SUKUN = "ْ";
    final String STR_HARAKA_TANWEEN_DAMMAH = "ٌ";
    final String STR_HARAKA_TANWEEN_FATHAH = "ً";
    final String STR_HARAKA_TANWEEN_KASRAH = "ٍ";
    final String STR_HARAKAT = STR_HARAKA_SUKUN + STR_HARAKA_FATHAH
            + STR_HARAKA_DAMMAH + STR_HARAKA_KASRAH
            + STR_HARAKA_TANWEEN_FATHAH
            + STR_HARAKA_TANWEEN_DAMMAH
            + STR_HARAKA_TANWEEN_KASRAH;
    final String STR_LETTERS = "دجحخهعغإفﻹقثصضذطكمنتاألﻷآبيسشظزوةىﻻﻵرؤءئ";
    final String STR_LETTERS_IDGHAM = "يرملون";
    final String STR_LETTER_HAMZAH = "ء";
    final String STR_LETTER_HAMZAT_WASL = "ا";
    final String STR_LETTER_YA = "ي";
    final String STR_LETTER_WAW = "و";
    final String STR_LETTER_FA = "ف";
    final String STR_LETTER_MADD = "ٰ";
    final String STR_PHONEME_NOUN_SUKUN = "نْ";
    final String STR_HARAKA_SHADDAH = "ّ";
    final String STR_SPECIAL = STR_HARAKA_SHADDAH + STR_LETTER_MADD;
    static final String STR_STOPS = "ۛ" //Special stop that comes in 2 but only one is used as a stop (and is an optional stop).
            + "ۗ" // stop preferably
            + "ۖ" // no stopping preferably
            + "ۚ" // stopping or not stopping is equaly preferable
            + "۩" // stopping for prosternation (sujud)
            + "ۘ"//Mandatory stop.
            + "ۜ";//Short pause
    static final String STR_NON_STOP = "ۙ"; //"recommended" not to stop.
    final String STR_WORD_ANA = "أَنَا";
    boolean _bDebugShowAya = true;
    boolean _bDebugShowPhonemes = true;
    boolean _bDebugShowSqlCrud = true; //INSERT, UPDATE or DELETE
    boolean _bDebugShowSqlSelect = true;
    BufferedWriter _bwResultsPhonemes;

    Connection _connection = null;
    Statement _statement = null;

    /**
     * Constructor
     *
     * @param p_alChapters the list of chapters to
     * @param p_bResetDB do we need to reset all db content (words and phonemes)
     * @param p_bVerboseMode do we need to display debug messages?
     */
    public PhonemeDictionaryGenerator(ArrayList<Integer> p_alChapters,
            boolean p_bResetDB, boolean p_bVerboseMode) {
        try {
            _bwResultsPhonemes = new BufferedWriter(new FileWriter(new File("allPhonemes.txt")));
            _bDebugShowAya = p_bVerboseMode;
            _bDebugShowPhonemes = p_bVerboseMode;
            _bDebugShowSqlCrud = p_bVerboseMode;
            _bDebugShowSqlSelect = p_bVerboseMode;
            init(p_alChapters, p_bResetDB);
        }//try//try//try//try
        catch (Exception ex) {
            Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, "PhonemeDictionaryGenerator()", ex);
            writeLogFile(ex);
        }//catch
    }//PhonemeDictionaryGenerator()

    private synchronized void addWordMappingToDB(Connection p_connection,
            int p_iIndexAyah,
            int p_iWordPosition,
            String p_sWord,
            Map<Integer, String> p_mapWordPhonemes) {
        try {
            try (Statement st = p_connection.createStatement()) {
                Map<Integer, Integer> mapIdPhonemes = new HashMap<>();
                String sSql;
                boolean isWordAFiller = false;
                int iIdWord = -1, iIdPhoneme;
                int iWordAlternativesCount = 0; //The number next to the word and between parentheses
                //Start a transaction
                p_connection.setAutoCommit(false);

                //Check if the word is a filler!
                sSql = "SELECT * FROM `fillers` WHERE `tag` = \"" + p_sWord + "\"";
                if (_bDebugShowSqlSelect) {
                    System.out.println(sSql);
                    writeLogFile(sSql);
                }//if
                try (ResultSet rs = st.executeQuery(sSql)) {
                    if (rs.next()) {
                        isWordAFiller = true;
                        iIdWord = rs.getInt("index");
                    }//if
                }//try-resources

                if (!isWordAFiller) {
                    //Check if the word is already in the DB
                    sSql = "SELECT COUNT(`index`) AS Nb FROM `words` WHERE (`word` = \"" + p_sWord
                            + "\") OR (`word` LIKE \"" + p_sWord + "(%)\")";
                    if (_bDebugShowSqlSelect) {
                        System.out.println(sSql);
                        writeLogFile(sSql);
                    }//if
                    try (ResultSet rs = st.executeQuery(sSql)) {
                        if (rs.next()) {
                            iWordAlternativesCount = rs.getInt("Nb");
                            if (iWordAlternativesCount == 0) {
                                //New word really and no other alternatives
                                iIdWord = getAndIncrementIdNextWord();
                                sSql = "INSERT INTO `words`(`index`, `word`) VALUES( " + iIdWord
                                        + ", \"" + p_sWord + "\")";
                                if (_bDebugShowSqlCrud) {
                                    System.out.println(sSql);
                                    writeLogFile(sSql);
                                }//if
                                st.executeUpdate(sSql);
                                //However, we still need to add the list of phonemes for this new word
                            }//if
                            else if (iWordAlternativesCount == 1) {
                                //get the word's id
                                sSql = "SELECT `index` FROM `words` WHERE `word` = \"" + p_sWord + "\"";
                                if (_bDebugShowSqlSelect) {
                                    System.out.println(sSql);
                                    writeLogFile(sSql);
                                }//if
                                try (ResultSet rs2 = st.executeQuery(sSql)) {
                                    if (rs2.next()) {
                                        iIdWord = rs2.getInt("index");
                                    }//if
                                }//try rs2
                            }//if
                        }//if
                    }//try rs
                    //Preparing to check if the phonemes' sequence for a word already exists in the DB
                    for (Map.Entry pairs : p_mapWordPhonemes.entrySet()) {
                        sSql = "SELECT `index` FROM `phonemes` WHERE `phoneme` = \"" + pairs.getValue() + "\"";
                        if (_bDebugShowSqlSelect) {
                            System.out.println(sSql);
                            writeLogFile(sSql);
                        }//if
                        try (ResultSet rs = st.executeQuery(sSql)) {
                            if (rs.next()) {
                                iIdPhoneme = rs.getInt("index");
                                mapIdPhonemes.put(Integer.parseInt(pairs.getKey().toString()), iIdPhoneme);
                            }//if
                            else {
                                //New phoneme, add it
                                iIdPhoneme = getAndIncrementIdNextPhoneme();
                                sSql = "INSERT INTO `phonemes`(`index`, `phoneme`) VALUES( " + iIdPhoneme
                                        + ", \"" + pairs.getValue() + "\")";
                                if (_bDebugShowSqlCrud) {
                                    System.out.println(sSql);
                                    writeLogFile(sSql);
                                }//if
                                st.executeUpdate(sSql);
                                mapIdPhonemes.put(Integer.parseInt(pairs.getKey().toString()), iIdPhoneme);
                            }//else
                        }//try rs
                    }//for
                    //Check if the phonemes' sequence for a word already exists in the DB
                    boolean bWordMapExists;
                    String sConditionsSql = "";
                    String sTablesSql = "`words`, ";
                    int iPhonemeCounter = 0;
                    for (Map.Entry pairs : mapIdPhonemes.entrySet()) {
                        iPhonemeCounter++;
                        String sTableName = "t" + iPhonemeCounter;
                        //Saving all the conditions in case the word mapping does exist to find the id of that word
                        if (iPhonemeCounter > 1) {
                            sTablesSql += ", ";
                        }//if
                        sTablesSql += "`word_has_phonemes` " + sTableName;

                        if (iPhonemeCounter > 1) {
                            sConditionsSql += " AND (t1.`id_words` = " + sTableName + ".`id_words`) AND ("
                                    + sTableName + ".`phoneme_position` = " + pairs.getKey()
                                    + " AND " + sTableName + ".`id_phonemes` = " + pairs.getValue() + ")";
                        }//if
                        else {
                            sConditionsSql += "(" + sTableName + ".`phoneme_position` = " + pairs.getKey()
                                    + " AND " + sTableName + ".`id_phonemes` = " + pairs.getValue() + ")";
                        }//else
                    }//for each
                    sConditionsSql += " AND (t1.`id_words` = `words`.`index`) AND (`word` = \""
                            + p_sWord + "\" OR `word` LIKE \"" + p_sWord + "(%)\")";
                    sSql = "SELECT t1.`id_words` FROM " + sTablesSql + " WHERE " + sConditionsSql;
                    if (_bDebugShowSqlSelect) {
                        System.out.println(sSql);
                        writeLogFile(sSql);
                    }//if
                    try (ResultSet rs = st.executeQuery(sSql)) {
                        if (bWordMapExists = rs.next()) {
                            iIdWord = rs.getInt("id_words");
                        }//if
                    }//try rs

                    if (!bWordMapExists) {
                        if (iWordAlternativesCount > 0) {
                            sSql = "INSERT INTO `words`(`word`) VALUES(\"" + p_sWord + "(" + (iWordAlternativesCount + 1) + ")\")";
                            if (_bDebugShowSqlCrud) {
                                System.out.println(sSql);
                                writeLogFile(sSql);
                            }//if
                            st.executeUpdate(sSql, Statement.RETURN_GENERATED_KEYS);
                            try (ResultSet rs = st.getGeneratedKeys()) {
                                rs.next();
                                iIdWord = rs.getInt(1);
                                setIdNextWord(iIdWord + 1);
                            }//try rs
                        }//if

                        //Add the new map to the DB
                        for (Map.Entry pairs : mapIdPhonemes.entrySet()) {
                            sSql = "INSERT INTO `word_has_phonemes`(`id_words`, `phoneme_position`,`id_phonemes`) VALUES ("
                                    + iIdWord + ", " + pairs.getKey() + ", " + pairs.getValue() + ")";
                            if (_bDebugShowSqlCrud) {
                                System.out.println(sSql);
                                writeLogFile(sSql);
                            }//if
                            st.executeUpdate(sSql);
                        }//for
                    }//if
                }//if
                //Add the word/filler to the ayah
                //Check if the word sequence for a ayah already exists in the DB
                sSql = "SELECT * FROM `quran_text_has_words` WHERE `id_quran_text` = " + p_iIndexAyah
                        + " AND `word_position` = " + p_iWordPosition
                        + " AND `id_words` = " + iIdWord;
                if (_bDebugShowSqlSelect) {
                    System.out.println(sSql);
                    writeLogFile(sSql);
                }//if
                try (ResultSet rs = st.executeQuery(sSql)) {
                    if (!rs.next()) {
                        //Add the sequence to the table
                        sSql = "INSERT INTO `quran_text_has_words`(`id_quran_text`, `word_position`, `id_words`) VALUES ("
                                + p_iIndexAyah + ", " + p_iWordPosition + ", " + iIdWord + ")";
                        if (_bDebugShowSqlCrud) {
                            System.out.println(sSql);
                            writeLogFile(sSql);
                        }//if
                        st.executeUpdate(sSql);
                    }//if
                }//try rs
                //Commit the transaction
                p_connection.commit();
            }//try statement with resource to release
        }//try
        catch (SQLException | NumberFormatException ex) {
            Logger.getLogger(PhonemeDictionaryGenerator.class.getName()).log(Level.SEVERE, null, ex);
            writeLogFile(ex);
            if (p_connection != null) {
                try {
                    _sLogData = "Transaction is being rolled back";
                    System.out.println(_sLogData);
                    writeLogFile(_sLogData);
                    p_connection.rollback();
                }//try
                catch (SQLException ex1) {
                    Logger.getLogger(PhonemeDictionaryGenerator.class.getName()).log(Level.SEVERE, "Transaction roll-back failed", ex1);
                    writeLogFile(ex1);
                }//catch
            }//if
        }//catch
        finally {
            try {
                if (p_connection != null) {
                    p_connection.setAutoCommit(true);
                }//if
            }//try
            catch (SQLException ex) {
                Logger.getLogger(PhonemeDictionaryGenerator.class.getName()).log(Level.SEVERE, "Finally Failed", ex);
                writeLogFile(ex);
            }//catch
        }//finally
    }

    public synchronized int getAndIncrementIdNextPhoneme() {
        return _iIdNextPhoneme++;
    }

    public synchronized int getAndIncrementIdNextWord() {
        return _iIdNextWord++;
    }

    /**
     * Gets and increments the index of the sura that should be processed
     *
     * @return the index of the sura
     */
    public synchronized int getAndIncrementIndexSura() {
        return _indexSura++;
    }

    public synchronized int getIdNextPhoneme() {
        return _iIdNextPhoneme;
    }

    public synchronized int getIdNextWord() {
        return _iIdNextWord;
    }

    private void init(ArrayList<Integer> p_alChapters, boolean p_bResetDB) throws SQLException, Exception {
        //Prepare the surat to include
        _indexSura = 0;
        _alFinished = new ArrayList<>();
        _alChapters = new ArrayList<>(p_alChapters);

        //Prepare the connection to the DB
        _connection = DriverManager.getConnection(STR_CONNECTION);
        // statements allow to issue SQL queries to the database
        _statement = _connection.createStatement();

        //Resetting the DB content
        String sSql;
        if (p_bResetDB) {
            sSql = "DELETE FROM `transcription_has_words_and_fillers`";
            if (_bDebugShowSqlCrud) {
                System.out.println(sSql);
                writeLogFile(sSql);
            }//if
            _statement.executeUpdate(sSql);

            sSql = "DELETE FROM `word_has_phonemes`";
            if (_bDebugShowSqlCrud) {
                System.out.println(sSql);
                writeLogFile(sSql);
            }//if
            _statement.executeUpdate(sSql);

            sSql = "DELETE FROM `quran_text_has_words`";
            if (_bDebugShowSqlCrud) {
                System.out.println(sSql);
                writeLogFile(sSql);
            }//if
            _statement.executeUpdate(sSql);

            sSql = "DELETE FROM `transcriptions`";
            if (_bDebugShowSqlCrud) {
                System.out.println(sSql);
                writeLogFile(sSql);
            }//if
            _statement.executeUpdate(sSql);
            sSql = "ALTER TABLE `transcriptions` AUTO_INCREMENT = 1";
            if (_bDebugShowSqlCrud) {
                System.out.println(sSql);
                writeLogFile(sSql);
            }//if
            _statement.executeUpdate(sSql);

            sSql = "DELETE FROM `fillers`";
            if (_bDebugShowSqlCrud) {
                System.out.println(sSql);
                writeLogFile(sSql);
            }//if
            _statement.executeUpdate(sSql);
            sSql = "ALTER TABLE `fillers` AUTO_INCREMENT = 1";
            if (_bDebugShowSqlCrud) {
                System.out.println(sSql);
                writeLogFile(sSql);
            }//if
            _statement.executeUpdate(sSql);

            sSql = "DELETE FROM `recitors`";
            if (_bDebugShowSqlCrud) {
                System.out.println(sSql);
                writeLogFile(sSql);
            }//if
            _statement.executeUpdate(sSql);
            sSql = "ALTER TABLE `recitors` AUTO_INCREMENT = 1";
            if (_bDebugShowSqlCrud) {
                System.out.println(sSql);
                writeLogFile(sSql);
            }//if
            _statement.executeUpdate(sSql);

            sSql = "DELETE FROM `words`";
            if (_bDebugShowSqlCrud) {
                System.out.println(sSql);
                writeLogFile(sSql);
            }//if
            _statement.executeUpdate(sSql);
            sSql = "ALTER TABLE `words` AUTO_INCREMENT = 1";
            if (_bDebugShowSqlCrud) {
                System.out.println(sSql);
                writeLogFile(sSql);
            }//if
            _statement.executeUpdate(sSql);

            sSql = "DELETE FROM `phonemes`";
            if (_bDebugShowSqlCrud) {
                System.out.println(sSql);
                writeLogFile(sSql);
            }//if
            _statement.executeUpdate(sSql);
            sSql = "ALTER TABLE `phonemes` AUTO_INCREMENT = 1";
            if (_bDebugShowSqlCrud) {
                System.out.println(sSql);
                writeLogFile(sSql);
            }//if
            _statement.executeUpdate(sSql);

            //Reset phonemes and words numbers
            setIdNextPhoneme(1);
            setIdNextWord(1);
        }//if
        else {
            //Get info from config table
            sSql = "SELECT * FROM `config`";
            if (_bDebugShowSqlSelect) {
                System.out.println(sSql);
                writeLogFile(sSql);
            }//if
            try (ResultSet rs = _statement.executeQuery(sSql)) {
                if (rs.next()) {
                    setIdNextWord(rs.getInt("index_next_word"));
                    setIdNextPhoneme(rs.getInt("index_next_phoneme"));
                }//if
                else {
                    throw new Exception("Error while loading the `config` information!");
                }//else
            }//try
        }//else
    }

    public boolean isDebugShowAya() {
        return _bDebugShowAya;
    }

    public boolean isDebugShowPhonemes() {
        return _bDebugShowPhonemes;
    }

    public boolean isDebugShowSqlCrud() {
        return _bDebugShowSqlCrud;
    }

    public boolean isDebugShowSqlSelect() {
        return _bDebugShowSqlSelect;
    }

    /**
     * *
     * Map the words to the phonemes
     *
     * @param withDiacritics true means that all phonemes will be with their
     * diacritics except elongation
     * @param isTranscriptionsUpdated is the method called because there is an
     * update to the transcription?
     */
    void mapPhonemes(boolean withDiacritics, boolean isTranscriptionsUpdated) {
        try {
            try (Connection connection = DriverManager.getConnection(STR_CONNECTION); Statement statement = connection.createStatement()) {
                //Load fillers from the db
                String sSql = "SELECT * FROM `fillers`";
                Map<String, String> mapFillers = new HashMap<>(); //Loading all even if we will be using only the tag for now
                try (ResultSet rs = statement.executeQuery(sSql)) {
                    while (rs.next()) {
                        mapFillers.put(rs.getString("tag"), rs.getString("value"));
                    }//while
                }//try-resources

                for (int chapter = 0; chapter < _alChapters.size(); chapter++) {
                    //Check if this is not already present in the database to skip it
                    boolean isLastPhonemeInLastWordWithTanween = false;
                    int i;
                    int iSuraNumber = _alChapters.get(chapter);
                    int iAyahNumber = -1;
                    int iIdAyah = -1;
                    //For saving all words and their phonemes in one place
                    ArrayList<Map<Integer, String>> alWordsPhonemes;
                    ArrayList<String> alWords;
                    if (isTranscriptionsUpdated) {
                        statement.execute("SET group_concat_max_len = 32768");
                        sSql = String.format(
                                "SELECT DISTINCT id_recitors, aya, GROUP_CONCAT(token ORDER BY position SEPARATOR ' ') AS `text` "
                                + "FROM load_transcription "
                                + "WHERE token NOT IN (\"<s>\", \"</s>\") AND sura = %d "
                                + "GROUP BY id_recitors, aya "
                                + "HAVING remove_parentheses(GROUP_CONCAT(token ORDER BY position SEPARATOR ' ')) "
                                + "NOT IN (SELECT `text` FROM quran_text WHERE sura = %d)",
                                iSuraNumber, iSuraNumber);
                    }//if
                    else {
                        sSql = "SELECT * FROM `quran_text` WHERE `sura` = " + iSuraNumber;
                    }//else
                    if (_bDebugShowSqlSelect) {
                        System.out.println(sSql);
                        writeLogFile(sSql);
                    }//if
                    try (ResultSet resultSet = statement.executeQuery(sSql)) {
                        while (resultSet.next()) {
                            Map<Integer, String> mapCurrentWordPhonemes;
                            String sAyah = resultSet.getString("text");

                            //TODO: need to process the stoping characters as potential stops with the different possibilities
                            //remove stopping characters from the aya
                            for (i = 0; i < STR_STOPS.length(); i++) {
                                sAyah = sAyah.replace(STR_STOPS.substring(i, i + 1), "");
                            }//for i
                            sAyah = sAyah.replace(STR_NON_STOP, "");
                            sAyah = sAyah.replaceAll(" {2,}", " "); //Keeping only 1 space between words/fillers

                            if (isTranscriptionsUpdated) {
                                try (Statement st = connection.createStatement();
                                        ResultSet rs = st.executeQuery(
                                                "SELECT * FROM `quran_text` WHERE `sura` = " + iSuraNumber)) {
                                    if (rs.next()) {
                                        iIdAyah = rs.getInt("index");
                                        iAyahNumber = rs.getInt("aya");
                                    }//if
                                }//try-resources
                            }//if
                            else {
                                iIdAyah = resultSet.getInt("index");
                                iAyahNumber = resultSet.getInt("aya");
                            }//else
                            String[] stAya = sAyah.split("\\s");
                            alWords = new ArrayList<>();
                            alWordsPhonemes = new ArrayList<>();
                            boolean isWordAFiller, isNextWordAFiller;
                            for (int iWordIndex = 0; iWordIndex < stAya.length; iWordIndex++) {
                                isLastPhonemeInLastWordWithTanween = false;
                                String sWord = stAya[iWordIndex];
                                isWordAFiller = isNextWordAFiller = false;
                                for (Map.Entry<String, String> entry : mapFillers.entrySet()) {
                                    if (iWordIndex < stAya.length - 1
                                            && stAya[iWordIndex].equals(entry.getKey())) {
                                        isNextWordAFiller = true;
                                    }//if
                                    if (entry.getKey().equals(sWord)) {
                                        isWordAFiller = true;
                                        break;
                                    }//if
                                }//foreach
                                if (isWordAFiller) {
                                    processAyahOrPartOfAyah(alWordsPhonemes, alWords,
                                            isLastPhonemeInLastWordWithTanween,
                                            chapter, iIdAyah, resultSet,
                                            withDiacritics, isTranscriptionsUpdated, false);
                                    continue;
                                }//if

                                alWords.add(sWord);
                                mapCurrentWordPhonemes = new HashMap<>();
                                String sPhoneme = STR_EMPTY;
                                String sChar;
                                int iPhonemeCount = 0;
                                //Check if this is a word that has a known set of phonemes that is not well extracted
                                if (sWord.equals(STR_WORD_ANA)) {
                                    mapCurrentWordPhonemes.put(1, "ءَ");
                                    mapCurrentWordPhonemes.put(2, "نَ");
                                    alWordsPhonemes.add(new HashMap<>(mapCurrentWordPhonemes));
                                    continue;
                                }//if
                                //TODO: This seems to be working since the letter had a correct entry in the .dic file
                                else if (iAyahNumber == 1) {
                                    int iPosition;
                                    if (iSuraNumber == 2 || iSuraNumber == 3 || (iSuraNumber > 28 && iSuraNumber < 33)) {//الم
                                        iPosition = 0;
                                        mapCurrentWordPhonemes.put(++iPosition, "ءَ");
                                        mapCurrentWordPhonemes.put(++iPosition, "لِ");
                                        mapCurrentWordPhonemes.put(++iPosition, "فْ");
                                        mapCurrentWordPhonemes.put(++iPosition, "لَ");
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_HAMZAT_WASL);
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_HAMZAT_WASL);
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_HAMZAT_WASL);
                                        mapCurrentWordPhonemes.put(++iPosition, "مِّ");
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_YA);
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_YA);
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_YA);
                                        mapCurrentWordPhonemes.put(++iPosition, "مْ");
                                    } else if (iSuraNumber == 7) {//المص
                                        iPosition = 0;
                                        mapCurrentWordPhonemes.put(++iPosition, "ءَ");
                                        mapCurrentWordPhonemes.put(++iPosition, "لِ");
                                        mapCurrentWordPhonemes.put(++iPosition, "فْ");
                                        mapCurrentWordPhonemes.put(++iPosition, "لَ");
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_HAMZAT_WASL);
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_HAMZAT_WASL);
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_HAMZAT_WASL);
                                        mapCurrentWordPhonemes.put(++iPosition, "مِّ");
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_YA);
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_YA);
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_YA);
                                        mapCurrentWordPhonemes.put(++iPosition, "مْ");
                                        mapCurrentWordPhonemes.put(++iPosition, "صَ");
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_HAMZAT_WASL);
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_HAMZAT_WASL);
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_HAMZAT_WASL);
                                        mapCurrentWordPhonemes.put(++iPosition, "دْ");
                                    } else if (sWord.equals("المر") && iSuraNumber == 13) {
                                        iPosition = 0;
                                        mapCurrentWordPhonemes.put(++iPosition, "ءَ");
                                        mapCurrentWordPhonemes.put(++iPosition, "لِ");
                                        mapCurrentWordPhonemes.put(++iPosition, "فْ");
                                        mapCurrentWordPhonemes.put(++iPosition, "لَ");
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_HAMZAT_WASL);
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_HAMZAT_WASL);
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_HAMZAT_WASL);
                                        mapCurrentWordPhonemes.put(++iPosition, "مِّ");
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_YA);
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_YA);
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_YA);
                                        mapCurrentWordPhonemes.put(++iPosition, "مْ");
                                        mapCurrentWordPhonemes.put(++iPosition, "رَ");
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_HAMZAT_WASL);
                                    } else if (sWord.equals("الر") && (iSuraNumber > 9 && iSuraNumber < 16)) {
                                        iPosition = 0;
                                        mapCurrentWordPhonemes.put(++iPosition, "ءَ");
                                        mapCurrentWordPhonemes.put(++iPosition, "لِ");
                                        mapCurrentWordPhonemes.put(++iPosition, "فْ");
                                        mapCurrentWordPhonemes.put(++iPosition, "لَ");
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_HAMZAT_WASL);
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_HAMZAT_WASL);
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_HAMZAT_WASL);
                                        mapCurrentWordPhonemes.put(++iPosition, "مْ");
                                        mapCurrentWordPhonemes.put(++iPosition, "رَ");
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_HAMZAT_WASL);
                                    } else if (iSuraNumber == 19) {//كهيعص
                                        iPosition = 0;
                                        mapCurrentWordPhonemes.put(++iPosition, "كَ");
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_HAMZAT_WASL);
                                        mapCurrentWordPhonemes.put(++iPosition, "فْ");
                                        mapCurrentWordPhonemes.put(++iPosition, "هْ");
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_HAMZAT_WASL);
                                        mapCurrentWordPhonemes.put(++iPosition, "يْ");
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_HAMZAT_WASL);
                                        mapCurrentWordPhonemes.put(++iPosition, "عَ");
                                        mapCurrentWordPhonemes.put(++iPosition, "يْ");
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_YA);
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_YA);
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_YA);
                                        mapCurrentWordPhonemes.put(++iPosition, "نْ");
                                        mapCurrentWordPhonemes.put(++iPosition, "صَ");
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_HAMZAT_WASL);
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_HAMZAT_WASL);
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_HAMZAT_WASL);
                                        mapCurrentWordPhonemes.put(++iPosition, "دْ");
                                    } else if (iSuraNumber == 20) {//طه
                                        iPosition = 0;
                                        mapCurrentWordPhonemes.put(++iPosition, "طَ");
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_HAMZAT_WASL);
                                        mapCurrentWordPhonemes.put(++iPosition, "هَ");
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_HAMZAT_WASL);
                                    } else if (iSuraNumber == 26 || iSuraNumber == 28) {//طسم
                                        iPosition = 0;
                                        mapCurrentWordPhonemes.put(++iPosition, "طَ");
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_HAMZAT_WASL);
                                        mapCurrentWordPhonemes.put(++iPosition, "سِ");
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_HAMZAT_WASL);
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_HAMZAT_WASL);
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_HAMZAT_WASL);
                                        mapCurrentWordPhonemes.put(++iPosition, "مِّ");
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_HAMZAT_WASL);
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_HAMZAT_WASL);
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_HAMZAT_WASL);
                                        mapCurrentWordPhonemes.put(++iPosition, "مْ");
                                    } else if (sWord.equals("طس") && iSuraNumber == 27) {
                                        iPosition = 0;
                                        mapCurrentWordPhonemes.put(++iPosition, "طَ");
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_HAMZAT_WASL);
                                        mapCurrentWordPhonemes.put(++iPosition, "سِ");
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_HAMZAT_WASL);
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_HAMZAT_WASL);
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_HAMZAT_WASL);
                                        mapCurrentWordPhonemes.put(++iPosition, "نْ");
                                    } else if (iSuraNumber == 36) {//يس
                                        iPosition = 0;
                                        mapCurrentWordPhonemes.put(++iPosition, "يَ");
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_HAMZAT_WASL);
                                        mapCurrentWordPhonemes.put(++iPosition, "سِ");
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_HAMZAT_WASL);
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_HAMZAT_WASL);
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_HAMZAT_WASL);
                                        mapCurrentWordPhonemes.put(++iPosition, "نْ");
                                    } else if (sWord.equals("ص") && iSuraNumber == 38) {
                                        iPosition = 0;
                                        mapCurrentWordPhonemes.put(++iPosition, "صَ");
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_HAMZAT_WASL);
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_HAMZAT_WASL);
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_HAMZAT_WASL);
                                        mapCurrentWordPhonemes.put(++iPosition, "دْ");
                                    } else if (iSuraNumber > 39 && iSuraNumber < 47) { //حم
                                        iPosition = 0;
                                        mapCurrentWordPhonemes.put(++iPosition, "حَ");
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_HAMZAT_WASL);
                                        mapCurrentWordPhonemes.put(++iPosition, "مِ");
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_YA);
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_YA);
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_YA);
                                        mapCurrentWordPhonemes.put(++iPosition, "مْ");
                                    } else if (sWord.equals("ق") && iSuraNumber == 50) {
                                        iPosition = 0;
                                        mapCurrentWordPhonemes.put(++iPosition, "قَ");
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_HAMZAT_WASL);
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_HAMZAT_WASL);
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_HAMZAT_WASL);
                                        mapCurrentWordPhonemes.put(++iPosition, "فْ");
                                    } else if (sWord.equals("ن") && iSuraNumber == 68) {
                                        iPosition = 0;
                                        mapCurrentWordPhonemes.put(++iPosition, "نُ");
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_WAW);
                                        mapCurrentWordPhonemes.put(++iPosition, STR_LETTER_WAW);
                                        mapCurrentWordPhonemes.put(++iPosition, "نْ");
                                    }
                                    //Add the word phonemes to the list
                                    alWordsPhonemes.add(new HashMap<>(mapCurrentWordPhonemes));
                                    continue;
                                }//else if

                                //Performing replacements of characters
                                sWord = sWord.replace("آ", "ءَا");
                                //replacing different forms of hamzah ("أ", "إ", ...) with "ء"
                                sWord = sWord.replace("أ", STR_LETTER_HAMZAH);
                                sWord = sWord.replace("إ", STR_LETTER_HAMZAH);
                                sWord = sWord.replace("ئ", STR_LETTER_HAMZAH);
                                sWord = sWord.replace("ؤ", STR_LETTER_HAMZAH);
                                //Rule إقلاب: Replace "ب" with "م" when preceded by "نْ"
                                sWord = sWord.replace("نْب", "مْب");
                                //Process the Madd
                                sWord = sWord.replace("ى" + STR_LETTER_MADD, STR_LETTER_HAMZAT_WASL);
                                sWord = sWord.replace("ى", STR_LETTER_HAMZAT_WASL);
                                sWord = sWord.replace(STR_LETTER_MADD, STR_LETTER_HAMZAT_WASL);

                                //<editor-fold defaultstate="collapsed" desc="for each character in a word (not a filler)">
                                //Check each character of the word
                                for (i = 0; i < sWord.length(); i++) {
                                    try {
                                        sChar = sWord.substring(i, i + 1);
                                    }//try
                                    catch (Exception e) {
                                        sChar = STR_EMPTY;
                                    }//catch

                                    //Manage "ال"
                                    if ("ال".equals(sPhoneme + sChar)) {
                                        if (iPhonemeCount == 0) {
                                            String sNextChar;
                                            try {
                                                sNextChar = sWord.substring(i + 1, i + 2);
                                            }//try
                                            catch (Exception e) {
                                                sNextChar = STR_EMPTY;
                                            }//catch
                                            if (STR_HARAKA_SUKUN.equals(sNextChar)) {
                                                if (iWordIndex == 0) {
                                                    mapCurrentWordPhonemes.put(++iPhonemeCount, STR_LETTER_HAMZAH + STR_HARAKA_FATHAH);
                                                }//if
                                                mapCurrentWordPhonemes.put(++iPhonemeCount, "ل" + STR_HARAKA_SUKUN);
                                                i++;//we already used the next haraka
                                                sPhoneme = STR_EMPTY;
                                            }//if
                                            else if (STR_HARAKA_SHADDAH.equals(sNextChar)) {
                                                if (iWordIndex == 0) {
                                                    mapCurrentWordPhonemes.put(++iPhonemeCount, STR_LETTER_HAMZAH + STR_HARAKA_FATHAH);
                                                }//if
                                                sPhoneme = "ل" + STR_HARAKA_SHADDAH;
                                                i++;//the shaddah
                                            }//else if
                                            else if (iWordIndex == 0 && STR_LETTERS.contains(sNextChar)) {
                                                mapCurrentWordPhonemes.put(++iPhonemeCount, STR_LETTER_HAMZAH + STR_HARAKA_FATHAH);
                                                sPhoneme = sNextChar;
                                                i++;
                                            }//else
                                            else {
                                                // ال= First phoneme of the first word of the Aya
                                                sPhoneme = STR_EMPTY;
                                            }//else
                                        }//if
                                        else {
                                            mapCurrentWordPhonemes.put(++iPhonemeCount, sPhoneme);
                                            sPhoneme = sChar;
                                        }//else
                                        continue;
                                    }//if

                                    if (STR_LETTERS.contains(sChar)) {
                                        if (sPhoneme.length() == 1 && STR_LETTERS.contains(sPhoneme)) {
                                            mapCurrentWordPhonemes.put(++iPhonemeCount, sPhoneme);
                                            sPhoneme = STR_EMPTY;
                                        }//if
                                        sPhoneme += sChar;
                                    }//if
                                    else if (sChar.equals(STR_HARAKA_SHADDAH)) {
                                        //Shaddah ّ
                                        sPhoneme += sChar;
                                    }//else if
                                    else if (STR_HARAKAT.contains(sChar)) {
                                        sPhoneme += sChar;
                                        //إلتقاء الساكنين
                                        //before adding the phoneme check if this is a sukun
                                        if (sChar.equals(STR_HARAKA_SUKUN)) {
                                            //Check if the previous phoneme is STR_HAMZAT_WASL
                                            if (STR_LETTER_HAMZAT_WASL.equals(mapCurrentWordPhonemes.get(iPhonemeCount)) && iWordIndex > 0) {
                                                //إلتقاء الساكنين
                                                //Remove STR_LETTER_HAMZAT_WASL when another letter has a STR_HARAKA_SUKUN
                                                mapCurrentWordPhonemes.remove(iPhonemeCount--);
                                            }//if
                                        }//if

                                        mapCurrentWordPhonemes.put(++iPhonemeCount, sPhoneme);
                                        sPhoneme = STR_EMPTY;
                                    }//else if

                                    if (iPhonemeCount == 3) { //Checking the leading STR_HAMZAT_WASL in a word
                                        if (STR_LETTER_HAMZAT_WASL.equals(mapCurrentWordPhonemes.get(1))) {
                                            String sTmpPhoneme = mapCurrentWordPhonemes.get(3);
                                            String sTmpPhonemeHaraka = sTmpPhoneme.substring(sTmpPhoneme.length() - 1, sTmpPhoneme.length());
                                            if (sTmpPhonemeHaraka.equals(STR_HARAKA_DAMMAH)) {
                                                mapCurrentWordPhonemes.replace(1, STR_LETTER_HAMZAH + STR_HARAKA_DAMMAH);
                                            }//if
                                            else {
                                                mapCurrentWordPhonemes.replace(1, STR_LETTER_HAMZAH + STR_HARAKA_KASRAH);
                                            }//else
                                        }//if
                                        //Check ("ال" or "ا" with the next phoneme aving a sukun) if preceded by "و" or "ف"
                                        else if ((mapCurrentWordPhonemes.get(1).contains(STR_LETTER_WAW)
                                                || mapCurrentWordPhonemes.get(1).contains(STR_LETTER_FA))
                                                && (STR_LETTER_HAMZAT_WASL.equals(mapCurrentWordPhonemes.get(2))
                                                && (mapCurrentWordPhonemes.get(3).contains("ل" + STR_HARAKA_SHADDAH)
                                                || mapCurrentWordPhonemes.get(3).contains(STR_HARAKA_SUKUN)))) {
                                            //Remove STR_LETTER_HAMZAT_WASL and
                                            //update id of phoneme containing "ل" and phoneme counter
                                            mapCurrentWordPhonemes.replace(2, mapCurrentWordPhonemes.get(3));
                                            mapCurrentWordPhonemes.remove(3);
                                            iPhonemeCount--;
                                        }//else if
                                    }//if
                                    else if (iPhonemeCount > 4) { //Checking for "ال" if the 2nd letter from the beginning of the word
                                        if (STR_LETTER_HAMZAT_WASL.equals(mapCurrentWordPhonemes.get(2)) //single letter without haraka
                                                && "ل".equals(mapCurrentWordPhonemes.get(3))) {
                                            //Delete the phoneme 2 and 3
                                            mapCurrentWordPhonemes.remove(2);
                                            mapCurrentWordPhonemes.remove(3);
                                            Map<Integer, String> mapTmp = new HashMap<>(mapCurrentWordPhonemes);

                                            //Update the phonemes positions
                                            for (Map.Entry<Integer, String> entry : mapCurrentWordPhonemes.entrySet()) {
                                                if (entry.getKey().equals(1)) {
                                                    continue;
                                                }//if
                                                mapTmp.put(entry.getKey() - 2, mapTmp.remove(entry.getKey()));
                                            } //for each entry

                                            mapCurrentWordPhonemes = mapTmp;
                                            iPhonemeCount -= 2;
                                        }//if
                                    }//if
                                }//for i
                                //</editor-fold>//</editor-fold>

                                //In case this is the last character and it is a letter
                                if (sPhoneme.length() > 0) {
                                    mapCurrentWordPhonemes.put(++iPhonemeCount, sPhoneme);
                                }//if

                                //Check if this is the last word in order to replace the last harakah with sokoun
                                if (!mapCurrentWordPhonemes.isEmpty()
                                        && (iWordIndex == stAya.length - 1
                                        || isNextWordAFiller)) {
                                    String sLastPhoneme = mapCurrentWordPhonemes.get(iPhonemeCount);
                                    try {
                                        if ("ة".equals(sLastPhoneme.substring(0, 1))) {
                                            mapCurrentWordPhonemes.replace(iPhonemeCount, "ه" + STR_HARAKA_SUKUN);
                                        }//if
                                        else {
                                            sChar = sLastPhoneme.substring(sLastPhoneme.length() - 1, sLastPhoneme.length());
                                            if (STR_HARAKAT.contains(sChar) && !sChar.equals(STR_HARAKA_SUKUN)) {
                                                sLastPhoneme = sLastPhoneme.substring(0, sLastPhoneme.length() - 1);
                                                sLastPhoneme += STR_HARAKA_SUKUN;
                                                mapCurrentWordPhonemes.replace(iPhonemeCount, sLastPhoneme);
                                            }//if
                                        }//else
                                    }//try
                                    catch (Exception e) {
                                        Logger.getLogger(PhonemeDictionaryGenerator.class.getName()).log(Level.SEVERE, "Check if this is the last word in order to replace the last harakah with sokoun", e);
                                        writeLogFile(e);
                                    }//catch
                                }//if

                                //Looking for Idgham and Iqlab
                                if (alWordsPhonemes.size() > 0) { //there is a previous phoneme
                                    Map<Integer, String> mapPrevPhonemes = alWordsPhonemes.get(alWordsPhonemes.size() - 1);
                                    String sTmp1 = mapCurrentWordPhonemes.get(1);
                                    String sTmp2 = mapPrevPhonemes.get(mapPrevPhonemes.size());
                                    if (sTmp1 != null && sTmp2 != null
                                            && sTmp2.equals(STR_PHONEME_NOUN_SUKUN)
                                            && (sTmp1.contains(STR_LETTERS_IDGHAM.subSequence(0, 1))
                                            || sTmp1.contains(STR_LETTERS_IDGHAM.subSequence(1, 2))
                                            || sTmp1.contains(STR_LETTERS_IDGHAM.subSequence(2, 3))
                                            || sTmp1.contains(STR_LETTERS_IDGHAM.subSequence(3, 4))
                                            || sTmp1.contains(STR_LETTERS_IDGHAM.subSequence(4, 5))
                                            || sTmp1.contains(STR_LETTERS_IDGHAM.subSequence(5, 6)))) {
                                        //Remove STR_PHONEME_NOUN_SUKUN from the previous phoneme if this is not the first word of the first Ayah of chapter 68
                                        if (!(alWordsPhonemes.size() == 1 && resultSet.getInt("aya") == 1 && iSuraNumber == 68)) {
                                            alWordsPhonemes.get(alWordsPhonemes.size() - 1).remove(mapPrevPhonemes.size());
                                            //Add shaddah to the first phoneme of the current phonemes
                                            mapCurrentWordPhonemes.replace(1, sTmp1, new StringBuilder(sTmp1).insert(1, STR_HARAKA_SHADDAH).toString());
                                        }//if
                                    }//if
                                    //When there are at least 2 words:
                                    //Check for Iqlab
                                    Map<Integer, String> phonemesWord1 = alWordsPhonemes.get(alWordsPhonemes.size() - 1);
                                    sTmp1 = phonemesWord1.get(phonemesWord1.size());
                                    sTmp2 = mapCurrentWordPhonemes.get(1);
                                    if (sTmp1 != null && sTmp2 != null
                                            && sTmp1.equals("نْ")
                                            && sTmp2.contains("ب")) {
                                        alWordsPhonemes.get(alWordsPhonemes.size() - 1).replace(phonemesWord1.size(), "مْ");
                                    }//if
                                }//if

                                if (mapCurrentWordPhonemes.size() > 2) {
                                    //Removing the last STR_HAMZAT_WASL when before it there is "و"
                                    if (mapCurrentWordPhonemes.get(mapCurrentWordPhonemes.size()).equals(STR_LETTER_HAMZAT_WASL)
                                            && (mapCurrentWordPhonemes.get(mapCurrentWordPhonemes.size() - 1).equals(STR_LETTER_WAW)
                                            || mapCurrentWordPhonemes.get(mapCurrentWordPhonemes.size() - 1).equals(STR_LETTER_WAW + STR_HARAKA_SUKUN))) {
                                        mapCurrentWordPhonemes.remove(mapCurrentWordPhonemes.size());
                                    }//if

                                    //Rule for 2 non diacritics (إلتقاء الساكنان) in 1 word
                                    if (mapCurrentWordPhonemes.get(2).equals(STR_LETTER_HAMZAT_WASL)
                                            && mapCurrentWordPhonemes.get(3).equals("لْ")) {
                                        for (int j = 2; j < mapCurrentWordPhonemes.size(); j++) {
                                            mapCurrentWordPhonemes.replace(j, mapCurrentWordPhonemes.get(j + 1));
                                        }//for j
                                        mapCurrentWordPhonemes.remove(mapCurrentWordPhonemes.size());
                                    }//if
                                }//if

                                //Removing last STR_HAMZAT_WASL when before it there is tanween
                                if (STR_LETTER_HAMZAT_WASL.equals(mapCurrentWordPhonemes.get(mapCurrentWordPhonemes.size()))) {
                                    String sPreviousPhoneme = mapCurrentWordPhonemes.get(mapCurrentWordPhonemes.size() - 1);
                                    if (sPreviousPhoneme.contains(STR_HARAKA_TANWEEN_DAMMAH)
                                            || sPreviousPhoneme.contains(STR_HARAKA_TANWEEN_KASRAH)
                                            || sPreviousPhoneme.contains(STR_HARAKA_TANWEEN_FATHAH)) {

                                        sPreviousPhoneme = sPreviousPhoneme.replace(STR_HARAKA_TANWEEN_DAMMAH, STR_HARAKA_DAMMAH);
                                        sPreviousPhoneme = sPreviousPhoneme.replace(STR_HARAKA_TANWEEN_KASRAH, STR_HARAKA_KASRAH);
                                        sPreviousPhoneme = sPreviousPhoneme.replace(STR_HARAKA_TANWEEN_FATHAH, STR_HARAKA_FATHAH);

                                        //Update the previous phoneme
                                        mapCurrentWordPhonemes.replace(mapCurrentWordPhonemes.size() - 1, mapCurrentWordPhonemes.get(mapCurrentWordPhonemes.size() - 1), sPreviousPhoneme);
                                        //replace STR_HAMZAT_WASL with STR_PHONEME_NOUN_SUKUN
                                        mapCurrentWordPhonemes.replace(mapCurrentWordPhonemes.size(), STR_LETTER_HAMZAT_WASL, STR_PHONEME_NOUN_SUKUN);
                                        isLastPhonemeInLastWordWithTanween = true;
                                    }//if
                                }//if

                                //Replacing tanween in last phoneme with STR_PHONEME_NOUN_SUKUN
                                String sLastPhoneme = mapCurrentWordPhonemes.get(mapCurrentWordPhonemes.size());
                                if (sLastPhoneme != null && (sLastPhoneme.contains(STR_HARAKA_TANWEEN_DAMMAH)
                                        || sLastPhoneme.contains(STR_HARAKA_TANWEEN_KASRAH)
                                        || sLastPhoneme.contains(STR_HARAKA_TANWEEN_FATHAH))) {

                                    sLastPhoneme = sLastPhoneme.replace(STR_HARAKA_TANWEEN_DAMMAH, STR_HARAKA_DAMMAH);
                                    sLastPhoneme = sLastPhoneme.replace(STR_HARAKA_TANWEEN_KASRAH, STR_HARAKA_KASRAH);
                                    sLastPhoneme = sLastPhoneme.replace(STR_HARAKA_TANWEEN_FATHAH, STR_HARAKA_FATHAH);

                                    //Update the last phoneme
                                    mapCurrentWordPhonemes.replace(mapCurrentWordPhonemes.size(), mapCurrentWordPhonemes.get(mapCurrentWordPhonemes.size()), sLastPhoneme);
                                    //add STR_PHONEME_NOUN_SUKUN
                                    mapCurrentWordPhonemes.put(mapCurrentWordPhonemes.size() + 1, STR_PHONEME_NOUN_SUKUN);
                                    isLastPhonemeInLastWordWithTanween = true;
                                }//if

                                //Replacing "ة" with "ت" when all the different processing is done
                                for (Map.Entry<Integer, String> p : mapCurrentWordPhonemes.entrySet()) {
                                    p.setValue(p.getValue().replaceAll("ة", "ت"));
                                }//foreach

                                //Add the word phonemes to the list
                                alWordsPhonemes.add(new HashMap<>(mapCurrentWordPhonemes));

                                //Check the previous word for a previous letter to be STR_HAMZAT_WASL, STR_LETTER_YA, or STR_LETTER_WAW with no haraka
                                if (alWords.size() > 1) {
                                    //Get the last phoneme of the previous word
                                    Map<Integer, String> mapPrevPhonemes = alWordsPhonemes.get(alWordsPhonemes.size() - 2);
                                    String sTmp1 = mapPrevPhonemes.get(mapPrevPhonemes.size());
                                    if (sTmp1 != null) {
                                        switch (sTmp1) {
                                            case STR_LETTER_HAMZAT_WASL:
                                                if (mapCurrentWordPhonemes.get(1).contains(STR_HARAKA_SHADDAH) // ّ
                                                        || mapCurrentWordPhonemes.get(1).contains(STR_HARAKA_SUKUN)) { // ْ
                                                    //Remove the last phoneme from the previous word
                                                    alWordsPhonemes.get(alWordsPhonemes.size() - 2).remove(mapPrevPhonemes.size());
                                                }//if
                                                break;
                                            case STR_LETTER_YA:
                                                if (mapCurrentWordPhonemes.get(1).contains(STR_HARAKA_SUKUN)) { // ْ
                                                    //Remove the last phoneme from the previous word
                                                    alWordsPhonemes.get(alWordsPhonemes.size() - 2).remove(mapPrevPhonemes.size());
                                                }//if
                                                break;
                                            case STR_LETTER_WAW:
                                                if (mapCurrentWordPhonemes.get(1).contains(STR_HARAKA_SUKUN)) { // ْ
                                                    //Remove the last phoneme from the previous word
                                                    alWordsPhonemes.get(alWordsPhonemes.size() - 2).remove(mapPrevPhonemes.size());
                                                }//if
                                                break;
                                        }//switch
                                    }//if
                                    //Tajweed rule of Mad if a Hamzah is the first phoneme of a word
                                    //and a Hae is the last phoneme of the previous word.
                                    sTmp1 = mapPrevPhonemes.get(mapPrevPhonemes.size());
                                    String sTmp2 = mapCurrentWordPhonemes.get(1);
                                    if (sTmp1 != null && sTmp2 != null && sTmp1.contains("ه")
                                            && sTmp2.contains(STR_LETTER_HAMZAH)) {
                                        //Determining the kind of mad depends on the Haraka of the Hae
                                        if (mapPrevPhonemes.get(mapPrevPhonemes.size()).contains(STR_HARAKA_FATHAH)) {
                                            mapPrevPhonemes.put(mapPrevPhonemes.size() + 1, STR_LETTER_HAMZAT_WASL);
                                        }//if
                                        else if (mapPrevPhonemes.get(mapPrevPhonemes.size()).contains(STR_HARAKA_DAMMAH)) {
                                            mapPrevPhonemes.put(mapPrevPhonemes.size() + 1, STR_LETTER_WAW);
                                        }//else if
                                        else if (mapPrevPhonemes.get(mapPrevPhonemes.size()).contains(STR_HARAKA_KASRAH)) {
                                            mapPrevPhonemes.put(mapPrevPhonemes.size() + 1, STR_LETTER_YA);
                                        }//else if
                                    }//if

                                    //Arabic Rule: Two sukuns between 2 words
                                    //إلتقاء الساكنين بين الكلمات
                                    //Is the first phoneme in the 2nd word contains a sukun and the last phoneme of
                                    // the 1st word is a "و" or "ا" or "ي" which are the only phonemes without harakat
                                    String sWord2FirstPhoneme = alWordsPhonemes.get(alWordsPhonemes.size() - 1).get(1);
                                    String sWord1LastPhoneme = alWordsPhonemes.get(alWordsPhonemes.size() - 2).get(alWordsPhonemes.get(alWordsPhonemes.size() - 2).size());
                                    if (sWord1LastPhoneme != null && sWord2FirstPhoneme != null && sWord2FirstPhoneme.contains(STR_HARAKA_SUKUN)
                                            && (sWord1LastPhoneme.equals(STR_LETTER_HAMZAT_WASL)
                                            || sWord1LastPhoneme.equals(STR_LETTER_WAW)
                                            || sWord1LastPhoneme.equals(STR_LETTER_YA))) {
                                        //remove the last phoneme of the first word
                                        alWordsPhonemes.get(alWordsPhonemes.size() - 2).remove(alWordsPhonemes.get(alWordsPhonemes.size() - 2).size());
                                    }//if

                                }//if
                            }//for iWordIndex

                            processAyahOrPartOfAyah(alWordsPhonemes, alWords,
                                    isLastPhonemeInLastWordWithTanween,
                                    chapter, iIdAyah, resultSet,
                                    withDiacritics, isTranscriptionsUpdated, true);
                        }//while resultSet.next()
                    }//try resultSet
                    _alFinished.add(chapter);
                }//for i
            }//try connection & statement
            _bwResultsPhonemes.close();
        }//try
        catch (Exception ex) {
            Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, "mapPhonemes()", ex);
            writeLogFile(ex);
            JOptionPane.showMessageDialog(null, ex.getMessage(), ex.getClass().getName(), JOptionPane.WARNING_MESSAGE);
        }//catch
    }

    /**
     * Processing done when a complete sentence or a filler is met.
     *
     * @param alWordsPhonemes
     * @param alWords
     * @param isLastPhonemeInLastWordWithTanween
     * @param chapter
     * @param iIdAyah
     * @param resultSet
     * @param withDiacritics Do we add diacritics to the Arabic phonemes?
     * @param isTranscriptionsUpdated
     * @throws SQLException
     */
    private void processAyahOrPartOfAyah(
            ArrayList<Map<Integer, String>> alWordsPhonemes,
            ArrayList<String> alWords,
            boolean isLastPhonemeInLastWordWithTanween,
            int chapter, int iIdAyah, ResultSet resultSet,
            boolean withDiacritics,
            boolean isTranscriptionsUpdated,
            boolean isEndOfTranscription
    ) throws SQLException {
        if (alWordsPhonemes.size() > 1) {
            for (int j = 0; j < alWordsPhonemes.size() - 1; j++) {
                Map<Integer, String> phonemesWord1 = alWordsPhonemes.get(j);
                Map<Integer, String> phonemesWord2 = alWordsPhonemes.get(j + 1);
                int indexLastPhonemeWord1 = phonemesWord1.size();

                //Rule for 2 non diacritics (إلتقاء الساكنان) in 2 words
                String sTmp1 = phonemesWord1.get(indexLastPhonemeWord1);
                String sTmp2 = phonemesWord2.get(1);
                if ((sTmp2 != null && sTmp1 != null && sTmp2.equals("لْ"))
                        && sTmp1.equals("ى")) {
                    phonemesWord1.remove(indexLastPhonemeWord1--);
                }//if
                else if (sTmp2 != null && sTmp1 != null && (sTmp2.contains(STR_HARAKA_SHADDAH)
                        && (sTmp1.equals(STR_LETTER_WAW)
                        || sTmp1.equals(STR_LETTER_YA)))) {
                    phonemesWord1.remove(indexLastPhonemeWord1--);
                }//if

                //Rule إقلاب: Replace "ب" with "م" when preceded by "نْ"
                sTmp1 = phonemesWord1.get(indexLastPhonemeWord1);
                if (sTmp1 != null && sTmp2 != null && sTmp1.contains(STR_HARAKA_TANWEEN_DAMMAH)
                        && sTmp2.contains("ب")) {
                    phonemesWord1.replace(indexLastPhonemeWord1, phonemesWord1.get(indexLastPhonemeWord1).replace(STR_HARAKA_TANWEEN_DAMMAH, STR_HARAKA_DAMMAH));
                    phonemesWord1.put(++indexLastPhonemeWord1, "م");
                }//if
                else if (sTmp1 != null && sTmp2 != null && sTmp1.contains(STR_HARAKA_TANWEEN_FATHAH)
                        && sTmp2.contains("ب")) {
                    phonemesWord1.replace(indexLastPhonemeWord1, phonemesWord1.get(indexLastPhonemeWord1).replace(STR_HARAKA_TANWEEN_FATHAH, STR_HARAKA_FATHAH));
                    phonemesWord1.put(++indexLastPhonemeWord1, "م");
                }//else if
                else if (sTmp1 != null && sTmp2 != null
                        && sTmp1.contains(STR_HARAKA_TANWEEN_KASRAH)
                        && sTmp2.contains("ب")) {
                    phonemesWord1.replace(indexLastPhonemeWord1, phonemesWord1.get(indexLastPhonemeWord1).replace(STR_HARAKA_TANWEEN_KASRAH, STR_HARAKA_KASRAH));
                    phonemesWord1.put(++indexLastPhonemeWord1, "م");
                }//else if
            }//for j
        }//if

        //Since this is the last word of the Ayah. Check for an ending "نْ "
        if (isLastPhonemeInLastWordWithTanween) {
            Map<Integer, String> lastPhonemes = alWordsPhonemes.get(alWordsPhonemes.size() - 1);
            Map.Entry<Integer, String> lastPhonemeEntry = null;
            for (Map.Entry<Integer, String> p : lastPhonemes.entrySet()) {
                lastPhonemeEntry = p;
            }//for each phoneme
            if (lastPhonemeEntry != null && lastPhonemeEntry.getValue().equals(STR_PHONEME_NOUN_SUKUN)) {
                //Replace with a Madd
                //TODO: using only Madd for Fathah. Need to implement others (Dammah and Kasrah) when adding more chapters
                lastPhonemeEntry.setValue("ا");
            }//if
        }//if

        if (isEndOfTranscription) {
            //Displaying the Aya, words and phonemes if configured
            String sMsg = "\n\n--------------\n";
            if (isTranscriptionsUpdated) {
                sMsg += _alChapters.get(chapter) + " "
                        + iIdAyah + " "
                        + resultSet.getString("text") + "\n";
            }//if
            else {
                sMsg += resultSet.getInt("sura") + " "
                        + resultSet.getInt("aya") + " "
                        + resultSet.getString("text") + "\n";
            }//else
            sMsg += "--------------\n";
            try {
                _bwResultsPhonemes.write(sMsg);
            }//try//try
            catch (IOException ex) {
                Logger.getLogger(PhonemeDictionaryGenerator.class.getName()).log(Level.SEVERE, null, ex);
                writeLogFile(ex);
            }//catch
            if (_bDebugShowAya) {
                System.out.println(sMsg);
                writeLogFile(sMsg);
            }//if
            try (Connection newConnection = DriverManager.getConnection(STR_CONNECTION)) {
                for (int j = 0; j < alWords.size(); j++) {
                    sMsg = "";
                    Map<Integer, String> phonemes = alWordsPhonemes.get(j);
                    //Save the phonemes in the db
                    addWordMappingToDB(newConnection, iIdAyah, j + 1, alWords.get(j), phonemes);
                    sMsg += "\n" + alWords.get(j) + " = ";
                    for (Map.Entry<Integer, String> p : phonemes.entrySet()) {
                        sMsg += p.getValue() + " ";
                    }//for each phoneme
                    sMsg += "\n";
                    if (_bDebugShowPhonemes) {
                        System.out.println(sMsg);
                        writeLogFile(sMsg);
                    }//if
                    try {
                        _bwResultsPhonemes.write(sMsg);
                    }//try//try
                    catch (IOException ex) {
                        Logger.getLogger(PhonemeDictionaryGenerator.class.getName()).log(Level.SEVERE, null, ex);
                        writeLogFile(ex);
                    }//catch
                }//for j
            }//try connection//try connection
        }//if
    }

    public void setDebugShowAya(boolean _bDebugShowAya) {
        this._bDebugShowAya = _bDebugShowAya;
    }

    public void setDebugShowPhonemes(boolean _bDebugShowPhonemes) {
        this._bDebugShowPhonemes = _bDebugShowPhonemes;
    }

    public void setDebugShowSqlCrud(boolean _bDebugShowSqlCrud) {
        this._bDebugShowSqlCrud = _bDebugShowSqlCrud;
    }

    public void setDebugShowSqlSelect(boolean _bDebugShowSqlSelect) {
        this._bDebugShowSqlSelect = _bDebugShowSqlSelect;
    }

    public synchronized void setIdNextPhoneme(int value) {
        _iIdNextPhoneme = value;
    }

    public synchronized void setIdNextWord(int value) {
        _iIdNextWord = value;
    }

}

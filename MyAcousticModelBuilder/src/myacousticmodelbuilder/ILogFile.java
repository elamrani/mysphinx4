/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myacousticmodelbuilder;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mohamed
 */
public interface ILogFile {

    final String LOG_FILENAME = "hqamb.log";

    default void writeLogFile(Exception ex) {
        writeLogFile("[" + new java.sql.Timestamp(new java.util.Date().getTime())
                + "] EXCEPTION: " + ex.getMessage() + "\n");
    }

    default void writeLogFile(String data) {
        //Check if the file is too big to split it
        try {
            if (Files.size(Paths.get(LOG_FILENAME)) > 10048576) {//10Mb
                Files.move(Paths.get(LOG_FILENAME),
                        Paths.get(LOG_FILENAME).resolveSibling(LOG_FILENAME + "_" + LocalDateTime.now().toString()));
            }
        } catch (IOException e) {
            Logger.getLogger(ILogFile.class.getName()).log(Level.WARNING, null, e);
        }

        try {
            data += "\n";
            Files.write(Paths.get(LOG_FILENAME), data.getBytes(), StandardOpenOption.APPEND);
        } catch (NoSuchFileException e) {
            try {
                Files.write(Paths.get(LOG_FILENAME), data.getBytes());
            } catch (IOException ex) {
                Logger.getLogger(ILogFile.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (IOException ex) {
            Logger.getLogger(ILogFile.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

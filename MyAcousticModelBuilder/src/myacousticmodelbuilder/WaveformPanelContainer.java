/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myacousticmodelbuilder;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.util.ArrayList;
import javax.sound.sampled.AudioInputStream;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Mohamed Y. El Amrani
 */
public class WaveformPanelContainer extends JPanel {

    private ArrayList _singleChannelWaveformPanels = new ArrayList();
    private AudioInfo _audioInfo = null;
    private SingleWaveformPanel _singleWaveformPanel;

    public WaveformPanelContainer() {
        setLayout(new GridLayout(0, 1));
    }

    public void setAudioToDisplay(AudioInputStream audioInputStream) {
        _singleChannelWaveformPanels = new ArrayList();
        _audioInfo = new AudioInfo(audioInputStream);
        for (int t = 0; t < _audioInfo.getNumberOfChannels(); t++) {
            _singleWaveformPanel = new SingleWaveformPanel(_audioInfo, t);
            _singleWaveformPanel.setSize(this.getSize());
            _singleWaveformPanel.setIncrement(_singleWaveformPanel.getSamples().length / (this.getWidth() - 20));
            _singleChannelWaveformPanels.add(_singleWaveformPanel);
            add(createChannelDisplay(_singleWaveformPanel, t));
        }
    }

    private JComponent createChannelDisplay(
            SingleWaveformPanel waveformPanel,
            int index) {

        JPanel panel = new JPanel(new BorderLayout());
        panel.add(waveformPanel, BorderLayout.CENTER);

        JLabel label = new JLabel("Channel " + ++index);
        panel.add(label, BorderLayout.NORTH);

        return panel;
    }
}

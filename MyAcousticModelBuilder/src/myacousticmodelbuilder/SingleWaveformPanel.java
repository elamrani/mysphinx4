/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myacousticmodelbuilder;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

/**
 *
 * @author Mohamed Y. El Amrani
 */
public class SingleWaveformPanel extends JPanel {

    private Color _referenceLineColor = Color.WHITE;
    private Color _waveformColor = Color.BLACK;
    private int[] _samples;
    private int _lineHeight;
    private int _increment;
    private double _YScaleFactor;

    /**
     * @return the _referenceLineColor
     */
    public Color getReferenceLineColor() {
        return _referenceLineColor;
    }

    /**
     * @param _referenceLineColor the _referenceLineColor to set
     */
    public void setReferenceLineColor(Color _referenceLineColor) {
        this._referenceLineColor = _referenceLineColor;
        this.validate();
    }

    /**
     * @return the _waveformColor
     */
    public Color getWaveformColor() {
        return _waveformColor;
    }

    /**
     * @param _waveformColor the _waveformColor to set
     */
    public void setWaveformColor(Color _waveformColor) {
        this._waveformColor = _waveformColor;
        this.validate();
    }

    /**
     * @return the samples
     */
    public int[] getSamples() {
        return _samples;
    }

    /**
     * @param samples the samples to set
     */
    public void setSamples(int[] samples) {
        this._samples = samples;
        this.validate();
    }

    /**
     * @return the _lineHeight
     */
    public int getLineHeight() {
        return _lineHeight;
    }

    /**
     * @param _lineHeight the _lineHeight to set
     */
    public void setLineHeight(int _lineHeight) {
        this._lineHeight = _lineHeight;
        this.validate();
    }

    /**
     * @return the _increment
     */
    public int getIncrement() {
        return _increment;
    }

    /**
     * @param _increment the _increment to set
     */
    public void setIncrement(int _increment) {
        this._increment = _increment;
        this.validate();
    }

    /**
     * @return the _YScaleFactor
     */
    public double getYScaleFactor() {
        return _YScaleFactor;
    }

    /**
     * @param _YScaleFactor the _YScaleFactor to set
     */
    public void setYScaleFactor(double _YScaleFactor) {
        this._YScaleFactor = _YScaleFactor;
        this.validate();
    }

    SingleWaveformPanel(AudioInfo audioInfo, int t) {
        _lineHeight = 50;
        _increment = 10;
        _YScaleFactor = 0.02;
        _samples = audioInfo.getToReturn()[t];
        this.addComponentListener(new java.awt.event.ComponentAdapter() {
            @Override
            public void componentResized(java.awt.event.ComponentEvent evt) {
                validate();
            }
        });
    }

    @Override
    public void validate() {
        super.validate();
        repaint();
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g); //To change body of generated methods, choose Tools | Templates.

        g.setColor(_referenceLineColor);
        _lineHeight = getHeight() / 2;
        g.drawLine(0, _lineHeight, (int) getWidth(), _lineHeight);

        int oldX = 0;
        int oldY = (int) (getHeight() / 2);
        int xIndex = 0;

        int increment = _increment;
        g.setColor(_waveformColor);

        int t;

        for (t = 0; t < increment; t += increment) {
            g.drawLine(oldX, oldY, xIndex, oldY);
            xIndex++;
            oldX = xIndex;
        }

        for (; t < _samples.length; t += increment) {
            double scaledSample = _samples[t] * _YScaleFactor;
            int y = (int) ((getHeight() / 2) - (scaledSample));
            g.drawLine(oldX, oldY, xIndex, y);

            xIndex++;
            oldX = xIndex;
            oldY = y;
        }
    }

}

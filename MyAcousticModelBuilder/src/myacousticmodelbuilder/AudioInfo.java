/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myacousticmodelbuilder;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioInputStream;
import mysounds.WaveformDisplay;

/**
 *
 * @author Mohamed Y. El Amrani
 */
class AudioInfo {

    private int numChannels;
    private int[][] toReturn;

    public AudioInfo(AudioInputStream audioInputStream) {
        try {
            int frameLength = (int) audioInputStream.getFrameLength();
            int frameSize = (int) audioInputStream.getFormat().getFrameSize();
            byte[] bytes = new byte[frameLength * frameSize];

            int result = 0;
            result = audioInputStream.read(bytes);

            numChannels = audioInputStream.getFormat().getChannels();
            toReturn = new int[numChannels][frameLength];

            int sampleIndex = 0;

            for (int t = 0; t < bytes.length;) {
                for (int channel = 0; channel < numChannels; channel++) {
                    int low = (int) bytes[t];
                    t++;
                    int high = (int) bytes[t];
                    t++;
                    int sample = getSixteenBitSample(high, low);
                    toReturn[channel][sampleIndex] = sample;
                }
                sampleIndex++;
            }
        } catch (IOException ex) {
            Logger.getLogger(WaveformDisplay.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                audioInputStream.close();
            } catch (IOException ex) {
                Logger.getLogger(WaveformDisplay.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    private int getSixteenBitSample(int high, int low) {
        return (high << 8) + (low & 0x00ff);
    }

    int getNumberOfChannels() {
        return numChannels;
    }

    public int[][] getToReturn() {
        return toReturn;
    }

}

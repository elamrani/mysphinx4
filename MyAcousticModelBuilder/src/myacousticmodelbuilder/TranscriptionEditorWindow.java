/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myacousticmodelbuilder;

import java.awt.ComponentOrientation;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Mohamed Y. El Amrani
 */
public class TranscriptionEditorWindow extends javax.swing.JDialog implements ILogFile {

    private final String STR_ALL = "--- ALL ---";
    private final String STR_SIL = "<sil>";
    private String _sAcousticModelRootDir;
    private String _sAcousticModelName;
    private String _sQuranRootDir;
    private String _sTestFileidsFilename;
    private String _sTrainingTranscriptionFilename;
    private String _sFillerFilename;
    private String _sPhonelistFilename;
    private String _sTrainingFileidsFilename;
    private String _sTestTranscriptionFilename;
    private String _sQuranFolder;
    private String _sInitialTranscriptionText;
    private Clip _audioClip;
    private int _lastAudioClipFrame;
    private Statement _statement;
    private ResultSet _resultSet;
    private boolean _bVerboseMode;
    private boolean _bUseTestForTraining;
    private boolean _bEnableEvent;
    private String _sLogData;

    /**
     * Creates new form TranscriptionAlignerWindow
     *
     * @param parent
     * @param modal
     * @param p_sAcousticModelRootDir the root folder for the Acoustic Model
     * @param p_sHolyQuranRootDir the root folder for the Holy Quran recitations
     * @param p_bUseTestForTraining
     * @param p_bVerboseMode Verbose mode if true will display the SQL
     * statements
     * @param connection the connection to the database
     * @throws java.sql.SQLException
     */
    public TranscriptionEditorWindow(JFrame parent, boolean modal,
            String p_sAcousticModelRootDir,
            String p_sHolyQuranRootDir,
            boolean p_bUseTestForTraining,
            boolean p_bVerboseMode,
            Connection connection) throws SQLException {
        super(parent, modal);
        initComponents();
        _bEnableEvent = false;
        init(p_sAcousticModelRootDir, p_sHolyQuranRootDir, p_bUseTestForTraining, p_bVerboseMode, connection);
        _bEnableEvent = true;
    }

    private void displayAudioFilename(String sAudioFilename) {
        int index = sAudioFilename.indexOf("/");
        int lastIndex = -1, beforeLastIndex = -1;
        while (index >= 0) {
            beforeLastIndex = lastIndex;
            lastIndex = index;
            index = sAudioFilename.indexOf("/", index + 1);
        }//while
        lblAudioFilename.setText(sAudioFilename.substring(beforeLastIndex + 1));
    }

    /**
     * Load the transcription for the selected audio file
     *
     * @param e The ActionEvent
     */
    private void loadTranscription() {
        if (cbAudioFiles != null && cbAudioFiles.getSelectedItem() != null && !cbAudioFiles.getSelectedItem().toString().equals(STR_ALL)) {

            String sAudioFilename = getAudioFilename();
            displayAudioFilename(sAudioFilename);

            //Check if it is in the test file
            String sLine;

            try {
                boolean isPresent = false;
                lblTranscriptionFilename.setText("");
                String sPath = lblAudioFilename.getText();
                int iSlash = sPath.indexOf("/");
                String sFolder = sPath.substring(0, iSlash);
                int iSura = Integer.parseInt(sPath.substring(iSlash + 1, iSlash + 4));
                int iAya = Integer.parseInt(sPath.substring(iSlash + 4, iSlash + 7));
                String sSql = "SELECT * "
                        + "FROM `load_transcription` "
                        + "WHERE `name` = \"" + sFolder
                        + "\" AND sura = " + iSura
                        + " AND aya = " + iAya;
                if (_bVerboseMode) {
                    System.out.println(sSql);
                    writeLogFile(sSql);
                }//if
                _resultSet = _statement.executeQuery(sSql);
                sLine = "";
                while (_resultSet.next()) {
                    sLine += _resultSet.getString("token") + " ";
                }//while
                _resultSet.close();

                _sInitialTranscriptionText = sLine.trim();
                taTranscription.setText(_sInitialTranscriptionText);
                loadTranscriptionCombinations(iSura, iAya);
                //check in the training file first
                try (BufferedReader br = new BufferedReader(new FileReader(_sTrainingTranscriptionFilename))) {
                    while ((sLine = br.readLine()) != null) {
                        if (sLine.contains(cbAudioFiles.getSelectedItem().toString())) {
                            lblTranscriptionFilename.setText(_sTrainingTranscriptionFilename.substring(_sTrainingTranscriptionFilename.lastIndexOf("/") + 1));
                            isPresent = true;
                            break;
                        }//if
                    }//while
                }//try BufferedReader

                if (!isPresent) {
                    try (BufferedReader br = new BufferedReader(new FileReader(_sTestTranscriptionFilename))) {
                        while ((sLine = br.readLine()) != null) {
                            if (sLine.contains(cbAudioFiles.getSelectedItem().toString())) {
                                lblTranscriptionFilename.setText(_sTestTranscriptionFilename.substring(_sTestTranscriptionFilename.lastIndexOf("/") + 1));
                                break;
                            }//if
                        }//while
                    }//try BufferedReader
                }//if

                if (ckAutoPlay.isSelected()) {
                    stopAudioClip();
                    while (_audioClip != null && (_audioClip.isRunning() || _audioClip.isRunning() || _audioClip.isOpen())) {
                        _audioClip.close();
                    }//while
                    _audioClip = null;
                    playPauseAudioClip(sAudioFilename);
                }//if

                //Display the waveform
                waveformPanelContainer.removeAll();
                AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File(sAudioFilename));
                waveformPanelContainer.setAudioToDisplay(audioInputStream);

            }//try
            catch (IOException | UnsupportedAudioFileException | SQLException ex) {
                Logger.getLogger(TranscriptionEditorWindow.class.getName()).log(Level.SEVERE, null, ex);
            }//catch
        }//if
        else {
            _sInitialTranscriptionText = "";
            taTranscription.setText(_sInitialTranscriptionText);
        }//else
    }

    /**
     * Load different combinations based on different types of silences.
     *
     * @param sInitialTranscriptionText Initial transcription that was saved in
     * the db
     * @param iSura the Surah number
     * @param iAya the Ayah number
     */
    private void loadTranscriptionCombinations(int iSura, int iAya) {
        try {
            _bEnableEvent = false;
            cbTranscriptions.removeAllItems();

            //FIXME: Need to properly populate the combobox
            if (!_sInitialTranscriptionText.contains("(")) {

                String sAyahOriginal = "", sAyah = "";
                String sSql = "SELECT * FROM `quran_text` WHERE sura = " + iSura
                        + " AND aya = " + iAya;
                if (_bVerboseMode) {
                    System.out.println(sSql);
                    writeLogFile(sSql);
                }//if
                _resultSet = _statement.executeQuery(sSql);
                if (_resultSet.next()) {
                    sAyah = sAyahOriginal = _resultSet.getString("text");
                }//if
                _resultSet.close();

                //Remove not stopping character
                sAyah = sAyah.replace(PhonemeDictionaryGenerator.STR_NON_STOP, "");

                List<Integer> listStopsPositions = new ArrayList<>();
                for (int i = 0; i < PhonemeDictionaryGenerator.STR_STOPS.length(); i++) {
                    String sStopChar = PhonemeDictionaryGenerator.STR_STOPS.substring(i, i + 1);
                    int indexStopChar = sAyah.indexOf(sStopChar);
                    while (indexStopChar >= 0) {
                        listStopsPositions.add(indexStopChar);
                        indexStopChar = sAyah.indexOf(sStopChar, indexStopChar + 1);
                    }//while
                }//for i

                if (listStopsPositions.size() > 0) {
                    int max = (int) Math.pow(2, listStopsPositions.size());
                    for (int i = 0; i < max; i++) {
                        // Convert int to bitset
                        BitSet bs = getConvertedBitSet(i, listStopsPositions.size());
                        for (int j = 0; j < listStopsPositions.size(); j++) {
                            StringBuilder sb = new StringBuilder(sAyahOriginal);
                            if (bs.get(j)) {
                                sb.setCharAt(listStopsPositions.get(j), 'S');//Silence
                            }//if
                            else {
                                sb.setCharAt(listStopsPositions.get(j), 'D');//Delete this character
                            }//else
                            sAyah = sb.toString();
                        }//for j

                        //replace all Ss with <sil> and remove all Ds
                        sAyah = sAyah.trim();
                        sAyah = sAyah.replace(" D ", " ");
                        sAyah = sAyah.replace("S", " " + STR_SIL + " ");
                        sAyah = sAyah.replaceAll(" {2,}", " "); //Keeping only 1 space between words/fillers

                        cbTranscriptions.addItem("<s> " + sAyah + " </s>");
                    }//for i
                }//if
            }//if
            _bEnableEvent = true;
        }//try
        catch (SQLException ex) {
            Logger.getLogger(TranscriptionEditorWindow.class.getName()).log(Level.SEVERE, null, ex);
        }//catch

    }

    /**
     * Helper function for this function converts an integer to the bitset
     *
     * @see
     * https://stackoverflow.com/questions/18466258/finding-power-set-of-a-set
     * @param value
     * @param size
     * @return
     */
    private BitSet getConvertedBitSet(int value, int size) {
        BitSet bits = new BitSet(size);
        bits.set(0, size - 1, false);
        int index = 0;
        while (value != 0) {
            if (value % 2 != 0) {
                bits.set(index);
            }//if
            ++index;
            value = value >>> 1;
        }//while
        return bits;
    }

    private void updateComponentOrientation() {
        if (ckRightToLeft.isSelected()) {
            jScrollPane2.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
            taTranscription.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
            cbTranscriptions.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        }//if
        else {
            jScrollPane2.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
            taTranscription.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
            cbTranscriptions.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
        }//else
    }

    private void init(String p_sAcousticModelRootDir, String p_sHolyQuranRootDir,
            boolean p_bUseTestForTraining, boolean p_bVerboseMode,
            Connection connection) throws SQLException {

        Set<String> stReciters = new HashSet<>();
        Set<String> stChapters = new HashSet<>();
        Set<String> stAyat = new HashSet<>();
        Set<String> stAudioFiles = new HashSet<>();

        cbTranscriptions.removeAllItems();
        lblAudioFilename.setText("");
        lblTranscriptionFilename.setText("");
        _bEnableEvent = true;

        updateComponentOrientation();

        _sAcousticModelRootDir = p_sAcousticModelRootDir;
        _sAcousticModelName = _sAcousticModelRootDir.substring(_sAcousticModelRootDir.lastIndexOf("/") + 1);
        _sQuranRootDir = p_sHolyQuranRootDir;
        _statement = connection.createStatement();
        _bVerboseMode = p_bVerboseMode;
        _bUseTestForTraining = p_bUseTestForTraining;

        //Preparing the needed filenames
        _sTestFileidsFilename
                = _sAcousticModelRootDir + "/etc/" + _sAcousticModelName + "_test.fileids";

        _sTestTranscriptionFilename
                = _sAcousticModelRootDir + "/etc/" + _sAcousticModelName + "_test.transcription";

        _sTrainingFileidsFilename
                = _sAcousticModelRootDir + "/etc/" + _sAcousticModelName + "_train.fileids";

        _sTrainingTranscriptionFilename
                = _sAcousticModelRootDir + "/etc/" + _sAcousticModelName + "_train.transcription";

        _sFillerFilename
                = _sAcousticModelRootDir + "/etc/" + _sAcousticModelName + ".filler";

        _sPhonelistFilename
                = _sAcousticModelRootDir + "/etc/" + _sAcousticModelName + ".phone";

        _sQuranFolder = _sAcousticModelRootDir + "/wav/";

        //Populate the arrays from the HDD once to reduce the transactions with the disk
        for (File folder : new File(_sQuranFolder).listFiles()) {
            if (folder.isDirectory()) {
                stReciters.add(folder.getName());
                for (File file : folder.listFiles()) {
                    stChapters.add(file.getName().substring(0, 3));
                    stAyat.add(file.getName().substring(3, 6));
                    stAudioFiles.add(file.getName().substring(0, file.getName().lastIndexOf(".wav")));
                }//foreach file
            }//if
        }//foreach folder

        _bEnableEvent = false;
        cbRecitors.setModel(new SortedComboBoxModel<>(new ArrayList<>(stReciters)));
        cbRecitors.insertItemAt(STR_ALL, 0);
        cbRecitors.setSelectedIndex(0);
        cbChapters.setModel(new SortedComboBoxModel<>(new ArrayList<>(stChapters)));
        cbChapters.insertItemAt(STR_ALL, 0);
        cbChapters.setSelectedIndex(0);
        cbAyat.setModel(new SortedComboBoxModel<>(new ArrayList<>(stAyat)));
        cbAyat.insertItemAt(STR_ALL, 0);
        cbAyat.setSelectedIndex(0);
        cbAudioFiles.setModel(new SortedComboBoxModel<>(new ArrayList<>(stAudioFiles)));
        cbAudioFiles.insertItemAt(STR_ALL, 0);
        cbAudioFiles.setSelectedIndex(0);
        _bEnableEvent = true;

        try {
            //Load fillers in the listbox
            String sSql = "SELECT `tag`, `value` FROM `fillers`";
            if (_bVerboseMode) {
                System.out.println(sSql);
                writeLogFile(sSql);
            }//if
            _resultSet = _statement.executeQuery(sSql);
            DefaultListModel listModel = new DefaultListModel();
            while (_resultSet.next()) {
                listModel.addElement(_resultSet.getString("tag") + "   " + _resultSet.getString("value"));
            }//while
            _resultSet.close();
            lbFillers.setModel(listModel);

            //Center window
            pack();
            setLocationRelativeTo(null);

        }//try
        catch (SQLException ex) {
            Logger.getLogger(TranscriptionEditorWindow.class.getName()).log(Level.SEVERE, null, ex);
        }//catch//catch
    }

    /**
     * Loading the lists based on the combo boxes
     */
    private void loadLists() {
        String sReciter = null, sChapter = null, sAyah = null;
        if (cbRecitors != null && cbRecitors.getSelectedIndex() != -1) {
            sReciter = cbRecitors.getItemAt(cbRecitors.getSelectedIndex()).toString();
            if (sReciter.equals(STR_ALL)) {
                sReciter = null;
            }//if
        }//if
        if (cbChapters != null && cbChapters.getSelectedIndex() != -1) {
            sChapter = cbChapters.getItemAt(cbChapters.getSelectedIndex()).toString();
            if (sChapter.equals(STR_ALL)) {
                sChapter = null;
            }//if
        }//if
        if (cbAyat != null && cbAyat.getSelectedIndex() != -1) {
            sAyah = cbAyat.getItemAt(cbAyat.getSelectedIndex()).toString();
            if (sAyah.equals(STR_ALL)) {
                sAyah = null;
            }//if
        }//if

        loadLists(sReciter, sChapter, sAyah);
    }

    /**
     * Load list of audio files in the combo boxes
     */
    private void loadLists(String sReciter, String sChapter, String sAyah) {
        //Clean the lists
        if (sReciter != null || sChapter != null || sAyah != null) {
            cbChapters.removeAllItems();
            cbAyat.removeAllItems();
        }//if
        cbAudioFiles.removeAllItems();

        Set<String> stChapters = new HashSet<>();
        Set<String> stAyat = new HashSet<>();
        Set<String> stAudioFiles = new HashSet<>();

        if (sReciter == null && sChapter == null && sAyah == null) {
            //No restrictions! Load all
            for (File folder : new File(_sQuranFolder).listFiles()) {
                if (folder.isDirectory()) {
                    for (File file : folder.listFiles()) {
                        stChapters.add(file.getName().substring(0, 3));
                        stAyat.add(file.getName().substring(3, 6));
                        stAudioFiles.add(file.getName().substring(0, file.getName().indexOf(".wav")));
                    }//foreach file
                }//if
            }//foreach folder
        }//if
        else if (sReciter == null && sChapter == null && sAyah != null) {
            for (File folder : new File(_sQuranFolder).listFiles()) {
                if (folder.isDirectory()) {
                    for (File file : folder.listFiles()) {
                        if (file.getName().substring(3, 6).equals(sAyah)) {
                            stChapters.add(file.getName().substring(0, 3));
                            stAyat.add(file.getName().substring(3, 6));
                            stAudioFiles.add(file.getName().substring(0, file.getName().indexOf(".wav")));
                        }//if
                    }//foreach file
                }//if
            }//foreach folder
        }//else if
        else if (sReciter == null && sChapter != null && sAyah == null) {
            for (File folder : new File(_sQuranFolder).listFiles()) {
                if (folder.isDirectory()) {
                    for (File file : folder.listFiles()) {
                        if (file.getName().substring(0, 3).equals(sChapter)) {
                            stChapters.add(file.getName().substring(0, 3));
                            stAyat.add(file.getName().substring(3, 6));
                            stAudioFiles.add(file.getName().substring(0, file.getName().indexOf(".wav")));
                        }//if
                    }//foreach file
                }//if
            }//foreach folder
        }//else if
        else if (sReciter == null && sChapter != null && sAyah != null) {
            for (File folder : new File(_sQuranFolder).listFiles()) {
                if (folder.isDirectory()) {
                    for (File file : folder.listFiles()) {
                        if (file.getName().substring(0, 6).equals(sChapter + sAyah)) {
                            stChapters.add(file.getName().substring(0, 3));
                            stAyat.add(file.getName().substring(3, 6));
                            stAudioFiles.add(file.getName().substring(0, file.getName().indexOf(".wav")));
                        }//if
                    }//foreach file
                }//if
            }//foreach folder
        }//else if
        else if (sReciter != null && sChapter == null && sAyah == null) {
            for (File folder : new File(_sQuranFolder).listFiles()) {
                if (folder.isDirectory() && folder.getName().equals(sReciter)) {
                    for (File file : folder.listFiles()) {
                        stChapters.add(file.getName().substring(0, 3));
                        stAyat.add(file.getName().substring(3, 6));
                        stAudioFiles.add(file.getName().substring(0, file.getName().indexOf(".wav")));
                    }//foreach file
                }//if
            }//foreach folder
        }//else if
        else if (sReciter != null && sChapter != null && sAyah == null) {
            for (File folder : new File(_sQuranFolder).listFiles()) {
                if (folder.isDirectory() && folder.getName().equals(sReciter)) {
                    for (File file : folder.listFiles()) {
                        if (file.getName().substring(0, 3).equals(sChapter)) {
                            stChapters.add(file.getName().substring(0, 3));
                            stAyat.add(file.getName().substring(3, 6));
                            stAudioFiles.add(file.getName().substring(0, file.getName().indexOf(".wav")));
                        }//if
                    }//foreach file
                }//if
            }//foreach folder
        }//else if
        else if (sReciter != null && sChapter == null && sAyah != null) {
            for (File folder : new File(_sQuranFolder).listFiles()) {
                if (folder.isDirectory() && folder.getName().equals(sReciter)) {
                    for (File file : folder.listFiles()) {
                        if (file.getName().substring(3, 6).equals(sAyah)) {
                            stChapters.add(file.getName().substring(0, 3));
                            stAyat.add(file.getName().substring(3, 6));
                            stAudioFiles.add(file.getName().substring(0, file.getName().indexOf(".wav")));
                        }//if
                    }//foreach file
                }//if
            }//foreach folder
        }//else if
        else if (sReciter != null && sChapter != null && sAyah != null) {
            for (File folder : new File(_sQuranFolder).listFiles()) {
                if (folder.isDirectory() && folder.getName().equals(sReciter)) {
                    for (File file : folder.listFiles()) {
                        if (file.getName().substring(0, 6).equals(sChapter + sAyah)) {
                            stChapters.add(file.getName().substring(0, 3));
                            stAyat.add(file.getName().substring(3, 6));
                            stAudioFiles.add(file.getName().substring(0, file.getName().indexOf(".wav")));
                        }//if
                    }//foreach file
                }//if
            }//foreach folder
        }//else if

        //Update the combo boxes
        SortedComboBoxModel<String> model = new SortedComboBoxModel<>(new ArrayList<>(stChapters));
        cbChapters.setModel(model);
        cbChapters.insertItemAt(STR_ALL, 0);
        if (sChapter != null) {
            cbChapters.setSelectedIndex(model.getIndexOf(sChapter));
        }//if
        else {
            cbChapters.setSelectedIndex(0);
        }//else
        model = new SortedComboBoxModel<>(new ArrayList<>(stAyat));
        cbAyat.setModel(model);
        cbAyat.insertItemAt(STR_ALL, 0);
        if (sAyah != null) {
            cbAyat.setSelectedIndex(model.getIndexOf(sAyah));
        }//if
        else {
            cbAyat.setSelectedIndex(0);
        }//else
        cbAudioFiles.setModel(new SortedComboBoxModel<>(new ArrayList<>(stAudioFiles)));
        cbAudioFiles.insertItemAt(STR_ALL, 0);
        cbAudioFiles.setSelectedIndex(0);
    }

    /**
     * *
     * Check if an audio filename is in the training or the testing file
     *
     * @param sFile The training or the testing filename
     * @param sAudioFilename The filename of the audio to be found
     * @return Empty string if not found or else returns the line containing the
     * filename from the file
     * @throws FileNotFoundException
     * @throws IOException
     */
    private String doesFileContainAudio(String sFile, String sAudioFilename) throws FileNotFoundException, IOException {
        String sLine = "";
        boolean isFound = false;
        try (BufferedReader br = new BufferedReader(new FileReader(sFile))) {
            while ((sLine = br.readLine()) != null) {
                if (sLine.contains(sAudioFilename)) {
                    isFound = true;
                    break;
                }//if
            }//while
        }//try BufferedReader
        if (!isFound) {
            sLine = "";
        }//if

        return sLine;
    }

    private String getAudioFilename() {
        String sReturnedValue = null;
        String sLine = "";
        if (cbAudioFiles != null && cbAudioFiles.getSelectedItem() != null
                && !cbAudioFiles.getSelectedItem().toString().equals("")) {
            String sSelectedAudioFile = cbAudioFiles.getSelectedItem().toString();
            try {
                //Search for the selected file
                sLine = doesFileContainAudio(_sTrainingFileidsFilename, sSelectedAudioFile);
                if (sLine.equals("")) {
                    sLine = doesFileContainAudio(_sTestFileidsFilename, sSelectedAudioFile);
                }//if
                if (!sLine.equals("")) {
                    sReturnedValue = _sQuranFolder + sLine + ".wav";
                }//if
            }//try
            catch (IOException ex) {
                Logger.getLogger(TranscriptionEditorWindow.class.getName()).log(Level.SEVERE, null, ex);
            }//catch
        }
        return sReturnedValue;
    }

    /**
     * Play the audio file
     *
     * @param appName The name of the application to open the audio file with
     */
    private void editSelectedAudioFile(String appName) {
        String sFilename = getAudioFilename();
        if (sFilename != null) {
            runCommand(appName + " " + sFilename);
        }//if
    }

    private void runCommand(String cmd) {
        try {
            Runtime r = Runtime.getRuntime();
            Process p = r.exec(cmd, null, new File(_sAcousticModelRootDir));
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
            BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
            // read the output from the command
            String s;
            _sLogData = "\n-------\nRunning: " + cmd;
            System.out.println(_sLogData);
            writeLogFile(_sLogData);
            while ((s = stdInput.readLine()) != null) {
                System.out.println(s);
                writeLogFile(s);
            }//while

            // read any errors from the attempted command
            _sLogData = "\n> Errors of the command (if any):";
            System.out.println(_sLogData);
            writeLogFile(_sLogData);
            while ((s = stdError.readLine()) != null) {
                System.out.println(s);
                writeLogFile(s);
            }//while
        }//try
        catch (IOException ex) {
            Logger.getLogger(MainWindow.class
                    .getName()).log(Level.WARNING, null, ex);
            writeLogFile(ex);
        }//catch
    }

    /**
     * *
     * Updates the file by replacing an old line (transcription) by another
     * newer transcription.
     *
     * @throws FileNotFoundException, IOException
     *
     * @param filename the file to edit
     * @param oldLine the old transcription line (including the file id)
     * @param newLine the new transcription line (including the file id)
     */
    private void updateFile(String filename, String oldLine,
            String newLine) throws FileNotFoundException, IOException {
        File file = new File(filename);
        File fileTmp = new File(filename + ".tmp");
        BufferedWriter bwWriter;
        try (BufferedReader brReader = new BufferedReader(new FileReader(file))) {
            bwWriter = new BufferedWriter(new FileWriter(fileTmp));
            String line;
            while ((line = brReader.readLine()) != null) {
                if (line.equals(oldLine)) {
                    line = newLine;
                }//if
                bwWriter.write(line + "\n");
            }//while
        }//try BufferedReader
        bwWriter.close();
        //make the tmp file permanent
        if (!file.delete()) {
            System.out.println("***Cannot delete file: " + file.getCanonicalPath());
        }//if
        else if (!fileTmp.renameTo(new File(filename))) {
            System.out.println("***Cannot rename file: " + fileTmp.getCanonicalPath());
        }//if
    }

    private void saveTranscription() {
        try {
            Connection connection = _statement.getConnection();
            String sFilename;
            Path path;
            String content;
            String sIdTranscription = "";
            sFilename = lblAudioFilename.getText();
            int iPosSlash = sFilename.indexOf("/");
            String sRecitor = sFilename.substring(0, iPosSlash);
            String sFileID = sFilename.substring(iPosSlash + 1, sFilename.indexOf(".wav"));
            String sSura = sFilename.substring(iPosSlash + 1, iPosSlash + 4);
            String sAya = sFilename.substring(iPosSlash + 4, iPosSlash + 7);

            //Save the updated transcription to the database
            //Before doing anything, check if it was updated
            String sSql = "SELECT * FROM `load_transcription` WHERE `name` = \""
                    + sRecitor
                    + "\" AND sura = " + sSura
                    + " AND aya = " + sAya;
            if (_bVerboseMode) {
                System.out.println(sSql);
                writeLogFile(sSql);
            }//if
            _resultSet = _statement.executeQuery(sSql);
            content = "";
            boolean firstIteration = true;
            while (_resultSet.next()) {
                if (firstIteration) {
                    firstIteration = false;
                    sIdTranscription = _resultSet.getString("index");
                }//if
                content += _resultSet.getString("token") + " ";
            }//while
            _resultSet.close();
            content = content.trim();
            try {
                if (!content.equals(taTranscription.getText())) {
                    //start a transaction
                    connection.setAutoCommit(false);
                    //Updated the database
                    sSql = "DELETE FROM `transcription_has_words_and_fillers` "
                            + "WHERE `id_transcriptions` IN "
                            + "(SELECT `transcriptions`.`index` "
                            + "FROM `transcriptions`, `quran_text`, `recitors`"
                            + "WHERE `id_quran_text` = `quran_text`.`index` "
                            + "AND `id_recitors` = `recitors`.`index` "
                            + "AND `recitors`.`name` = \"" + sRecitor
                            + "\" AND sura = " + sSura
                            + " AND aya = " + sAya
                            + ")";
                    if (_bVerboseMode) {
                        System.out.println(sSql);
                        writeLogFile(sSql);
                    }//if
                    _statement.executeUpdate(sSql);

                    //preparing the query to insert the new transcription
                    int iPositionInTranscription = 1;
                    String sTranscriptionSql = "INSERT INTO `transcription_has_words_and_fillers`"
                            + "(`id_transcriptions`, `position`, `id_words`, `id_fillers`) VALUES ";
                    String[] tokens = taTranscription.getText().split(" ");
                    firstIteration = true;
                    for (String token : tokens) {
                        int iIdToken = -1;
                        if (!firstIteration) {
                            sTranscriptionSql += ", ";
                        }//if
                        firstIteration = false;
                        //Is it a word?
                        sSql = "SELECT `index` FROM `words` WHERE `word` = \"" + token + "\"";
                        if (_bVerboseMode) {
                            System.out.println(sSql);
                            writeLogFile(sSql);
                        }//if
                        _resultSet = _statement.executeQuery(sSql);
                        if (_resultSet.next()) {
                            iIdToken = _resultSet.getInt("index");
                            sTranscriptionSql += String.format("(%s, %d, %d, null)",
                                    sIdTranscription, iPositionInTranscription++, iIdToken);
                        }//if
                        else {
                            //not a word already in the db! maybe a filler that is already in the db
                            _resultSet.close();
                            sSql = "SELECT `index` FROM `fillers` WHERE `tag` = \"" + token + "\"";
                            if (_bVerboseMode) {
                                System.out.println(sSql);
                                writeLogFile(sSql);
                            }//if
                            _resultSet = _statement.executeQuery(sSql);
                            if (_resultSet.next()) {
                                iIdToken = _resultSet.getInt("index");
                                sTranscriptionSql += String.format("(%s, %d, null, %d)",
                                        sIdTranscription, iPositionInTranscription++, iIdToken);
                            }//if
                            else {
                                //Is it a new filler or a new word?
                                _resultSet.close();
                                if (token.startsWith("<") || token.startsWith("+")) {
                                    //TODO: Add the new filler to the db and the fillers file before adding it to the transcription
                                    throw new SQLException("\"" + token + "\": token not found in the database");
                                }//if
                                else {
                                    //This is a new word that needs to be added to the db
                                    sSql = "INSERT INTO `words`(`word`) VALUES (\""
                                            + token + "\")";
                                    if (_bVerboseMode) {
                                        System.out.println(sSql);
                                        writeLogFile(sSql);
                                    }//if
                                    _statement.executeUpdate(sSql, Statement.RETURN_GENERATED_KEYS);
                                    _resultSet = _statement.getGeneratedKeys();
                                    _resultSet.next();
                                    iIdToken = _resultSet.getInt(1);
                                    sTranscriptionSql += String.format(
                                            "(%s, %d, %d, null)",
                                            sIdTranscription, iPositionInTranscription++,
                                            iIdToken);
                                    _resultSet.close();
                                }//else
                            }//else
                        }//else
                        if (!_resultSet.isClosed()) {
                            _resultSet.close();
                        }//if
                    }//for each

                    sSql = sTranscriptionSql;
                    if (_bVerboseMode) {
                        System.out.println(sSql);
                        writeLogFile(sSql);
                    }//if
                    _statement.executeUpdate(sSql);

                    //Commit the transaction
                    connection.commit();

                    //Updated transcription files. Let's save it
                    if (_bUseTestForTraining) {
                        // Training transcription
                        updateFile(_sTrainingTranscriptionFilename,
                                _sInitialTranscriptionText + " (" + sFileID + ")",
                                taTranscription.getText() + " (" + sFileID + ")");
                        // Testing transcription
                        updateFile(_sTestTranscriptionFilename,
                                _sInitialTranscriptionText + " (" + sFileID + ")",
                                taTranscription.getText() + " (" + sFileID + ")");
                    }//if
                    else {
                        //Determine which file to update
                        sFilename = lblTranscriptionFilename.getText().contains("_train.transcription") ? _sTrainingTranscriptionFilename : _sTestTranscriptionFilename;
                        //Save the updated transcription to the file
                        updateFile(sFilename,
                                _sInitialTranscriptionText + " (" + sFileID + ")",
                                taTranscription.getText() + " (" + sFileID + ")");
                    }//else
                }//if

                //Using a new filler?
                if (taTranscription.getText().contains("+")) {
                    //Count how many filler are there?
                    int index = taTranscription.getText().indexOf("+");
                    ArrayList<Integer> alIndices = new ArrayList<>();
                    while (index >= 0) {
                        alIndices.add(index);
                        index = taTranscription.getText().indexOf("+", index + 1);
                    }//while
                    String sFiller;
                    for (int i = 0; i < alIndices.size() / 2; i++) {
                        //get the filler
                        sFiller = taTranscription.getText().substring(alIndices.get(i), alIndices.get(i + 1) + 1);

                        //Add to the listbox
                        DefaultListModel listModel = (DefaultListModel) lbFillers.getModel();
                        //is it already in it?
                        if (!listModel.contains(sFiller)) {
                            listModel.addElement(sFiller + "   +" + sFiller + "+");
                            lbFillers.setModel(listModel);

                            //Add to the file Filler
                            File file = new File(_sFillerFilename);
                            try (BufferedWriter bwWriter = new BufferedWriter(new FileWriter(file, true))) {
                                bwWriter.write(sFiller + "   +" + sFiller + "+");
                            }//try BufferedWriter

                            //Add to the phone list
                            file = new File(_sPhonelistFilename);
                            try (BufferedWriter bwWriter = new BufferedWriter(new FileWriter(file, true))) {
                                bwWriter.write(sFiller + "   +" + sFiller + "+");
                            }//try BufferedWriter
                        }//if
                    }//for i
                }//if
            }//try
            catch (SQLException | IOException e) {
                connection.rollback();
                if (_bVerboseMode) {
                    _sLogData = "Transaction Rollback";
                    System.out.println(_sLogData);
                    writeLogFile(_sLogData);
                }//if
                writeLogFile(e);
                throw e;
            }//catch
        }//try
        catch (IOException | SQLException ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(),
                    "Exception!", JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(TranscriptionEditorWindow.class
                    .getName()).log(Level.SEVERE, null, ex);
            writeLogFile(ex);
        }//catch

    }

    /**
     * Launch an application to edit the audio file in the Quran root folder
     *
     * @param appName The name of the application
     */
    private void editFileInQuranRootDir(String appName) {
        String sFilename = _sQuranRootDir + "/";
        sFilename += lblAudioFilename.getText().substring(0, lblAudioFilename.getText().lastIndexOf("_id"));
        sFilename += ".mp3";
        if (sFilename != null) {
            runCommand(appName + " " + sFilename);
        }//if
    }

    private void loadAudioClip(File audioFile) throws LineUnavailableException, IOException, UnsupportedAudioFileException {
        AudioInputStream audioStream = AudioSystem.getAudioInputStream(audioFile);

        AudioFormat format = audioStream.getFormat();
        DataLine.Info info = new DataLine.Info(Clip.class, format);
        if (_audioClip
                != null) {
            _audioClip.flush();
        }//if
        _audioClip = (Clip) AudioSystem.getLine(info);

        _audioClip.open(audioStream);
    }

    private void playPauseAudioClip(String sAudioFilename) {
        if (_audioClip == null) {
            try {
                loadAudioClip(new File(sAudioFilename));
                _audioClip.start();
                btnPlayPause.setText("Pause");

            }//try
            catch (LineUnavailableException | IOException | UnsupportedAudioFileException ex) {
                Logger.getLogger(TranscriptionEditorWindow.class
                        .getName()).log(Level.SEVERE, null, ex);
                writeLogFile(ex);
                JOptionPane.showMessageDialog(this,
                        "Failed to load audio clip", "Error", JOptionPane.ERROR_MESSAGE);
            }//catch//catch
        }//if
        else if (_audioClip.isRunning()) {
            _lastAudioClipFrame = _audioClip.getFramePosition();
            _audioClip.stop();
            btnPlayPause.setText("Play");
        }//if
        else {
            if (_lastAudioClipFrame < _audioClip.getFrameLength()) {
                _audioClip.setFramePosition(_lastAudioClipFrame);
            }//if
            else {
                _audioClip.setFramePosition(0);
            }//else
            _audioClip.start();
            btnPlayPause.setText("Pause");
        }//else//else
    }

    private void playPauseAudioClip() {
        playPauseAudioClip(getAudioFilename());
    }

    private void stopAudioClip() {
        if (_audioClip != null) {
            _lastAudioClipFrame = 0;
            _audioClip.stop();
            btnPlayPause.setText("Play");
        }//if
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        cbAudioFiles = new javax.swing.JComboBox();
        btnEditAudioFileMhWaveEdit = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        taTranscription = new javax.swing.JTextArea();
        btnSaveTranscription = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        lbFillers = new javax.swing.JList();
        btnClose = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        lblTranscriptionFilename = new javax.swing.JLabel();
        ckRightToLeft = new javax.swing.JCheckBox();
        btnNextFile = new javax.swing.JButton();
        btnPreviousFile = new javax.swing.JButton();
        ckAutoPlay = new javax.swing.JCheckBox();
        btnPlayPause = new javax.swing.JButton();
        lblAudioFilename = new javax.swing.JLabel();
        btnEditFileInRootDirMhWaveEdit = new javax.swing.JButton();
        btnStop = new javax.swing.JButton();
        waveformPanelContainer = new myacousticmodelbuilder.WaveformPanelContainer();
        jLabel5 = new javax.swing.JLabel();
        cbAyat = new javax.swing.JComboBox();
        jLabel6 = new javax.swing.JLabel();
        cbRecitors = new javax.swing.JComboBox();
        cbChapters = new javax.swing.JComboBox();
        cbTranscriptions = new javax.swing.JComboBox();
        btnEditAudioFileAudacity = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        btnEditFileInRootDirAudacity = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Transciption Editor");

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setLabelFor(cbAudioFiles);
        jLabel1.setText("Audio File");

        cbAudioFiles.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cbAudioFiles.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbAudioFilesActionPerformed(evt);
            }
        });

        btnEditAudioFileMhWaveEdit.setText("mhWaveEdit");
        btnEditAudioFileMhWaveEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditAudioFileMhWaveEditActionPerformed(evt);
            }
        });

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Transcription text");

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setLabelFor(lbFillers);
        jLabel3.setText("Fillers");

        taTranscription.setColumns(20);
        taTranscription.setFont(taTranscription.getFont().deriveFont(taTranscription.getFont().getSize()+9f));
        taTranscription.setLineWrap(true);
        taTranscription.setRows(5);
        jScrollPane2.setViewportView(taTranscription);

        btnSaveTranscription.setText("Save");
        btnSaveTranscription.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveTranscriptionActionPerformed(evt);
            }
        });

        lbFillers.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        jScrollPane1.setViewportView(lbFillers);

        btnClose.setText("Close");
        btnClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCloseActionPerformed(evt);
            }
        });

        jLabel4.setText("in file:");

        lblTranscriptionFilename.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        lblTranscriptionFilename.setText("_filename_");
        lblTranscriptionFilename.setVerticalAlignment(javax.swing.SwingConstants.TOP);

        ckRightToLeft.setSelected(true);
        ckRightToLeft.setText("Right to Left");
        ckRightToLeft.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ckRightToLeftMouseClicked(evt);
            }
        });

        btnNextFile.setText("Next");
        btnNextFile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNextFileActionPerformed(evt);
            }
        });

        btnPreviousFile.setText("Prev.");
        btnPreviousFile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPreviousFileActionPerformed(evt);
            }
        });

        ckAutoPlay.setSelected(true);
        ckAutoPlay.setText("Auto play on each selection");

        btnPlayPause.setText("Play");
        btnPlayPause.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPlayPauseActionPerformed(evt);
            }
        });

        lblAudioFilename.setFont(new java.awt.Font("Ubuntu", 1, 15)); // NOI18N
        lblAudioFilename.setText("_audio filename_");

        btnEditFileInRootDirMhWaveEdit.setText("mhWaveEdit");
        btnEditFileInRootDirMhWaveEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditFileInRootDirMhWaveEditActionPerformed(evt);
            }
        });

        btnStop.setText("Stop");
        btnStop.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnStopActionPerformed(evt);
            }
        });

        waveformPanelContainer.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setLabelFor(cbAudioFiles);
        jLabel5.setText("Holy Quran");

        cbAyat.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cbAyat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbAyatActionPerformed(evt);
            }
        });

        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setLabelFor(cbAudioFiles);
        jLabel6.setText("Recitors");

        cbRecitors.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cbRecitors.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbRecitorsActionPerformed(evt);
            }
        });

        cbChapters.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cbChapters.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbChaptersActionPerformed(evt);
            }
        });

        cbTranscriptions.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cbTranscriptions.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbTranscriptionsActionPerformed(evt);
            }
        });

        btnEditAudioFileAudacity.setText("Audacity");
        btnEditAudioFileAudacity.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditAudioFileAudacityActionPerformed(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Arimo", 1, 12)); // NOI18N
        jLabel7.setText("Trainning Audio File");

        jLabel8.setFont(new java.awt.Font("Arimo", 1, 12)); // NOI18N
        jLabel8.setText("Quran root folder File");

        btnEditFileInRootDirAudacity.setText("Audacity");
        btnEditFileInRootDirAudacity.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditFileInRootDirAudacityActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(cbChapters, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addComponent(cbAyat, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbAudioFiles, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbRecitors, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(4, 4, 4)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2)
                    .addComponent(cbTranscriptions, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(13, 13, 13))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(ckAutoPlay, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lblTranscriptionFilename, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(17, 17, 17))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnEditAudioFileMhWaveEdit)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnEditAudioFileAudacity)
                .addGap(77, 77, 77)
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnEditFileInRootDirMhWaveEdit)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnEditFileInRootDirAudacity, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnClose, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(13, 13, 13))
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(waveformPanelContainer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblAudioFilename, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnPreviousFile)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnPlayPause, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(1, 1, 1)
                        .addComponent(btnStop)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnNextFile)
                        .addGap(429, 429, 429)
                        .addComponent(ckRightToLeft)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnSaveTranscription, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 224, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 550, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3))))
                .addGap(2, 2, 2)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cbRecitors, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cbTranscriptions, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(4, 4, 4)
                        .addComponent(jLabel5)
                        .addGap(4, 4, 4)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cbChapters, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cbAyat, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(14, 14, 14)
                        .addComponent(jLabel1)
                        .addGap(4, 4, 4)
                        .addComponent(cbAudioFiles, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(3, 3, 3)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnSaveTranscription, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(ckRightToLeft))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnPreviousFile, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnPlayPause)
                            .addComponent(btnStop)
                            .addComponent(btnNextFile, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(1, 1, 1)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTranscriptionFilename, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(ckAutoPlay))
                .addGap(9, 9, 9)
                .addComponent(lblAudioFilename, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(waveformPanelContainer, javax.swing.GroupLayout.DEFAULT_SIZE, 320, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnEditFileInRootDirMhWaveEdit, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnEditFileInRootDirAudacity, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnClose, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnEditAudioFileAudacity)
                    .addComponent(jLabel7)
                    .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnEditAudioFileMhWaveEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCloseActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnCloseActionPerformed

    private void btnEditAudioFileMhWaveEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditAudioFileMhWaveEditActionPerformed
        editSelectedAudioFile("mhwaveedit");
    }//GEN-LAST:event_btnEditAudioFileMhWaveEditActionPerformed

    private void btnSaveTranscriptionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveTranscriptionActionPerformed
        saveTranscription();
    }//GEN-LAST:event_btnSaveTranscriptionActionPerformed

    private void ckRightToLeftMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ckRightToLeftMouseClicked
        updateComponentOrientation();
    }//GEN-LAST:event_ckRightToLeftMouseClicked

    private void cbAudioFilesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbAudioFilesActionPerformed
        if (_bEnableEvent) {
            _bEnableEvent = false;
            loadTranscription();
            _bEnableEvent = true;
        }//if
    }//GEN-LAST:event_cbAudioFilesActionPerformed

    private void btnNextFileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNextFileActionPerformed
        if (_bEnableEvent) {
            _bEnableEvent = false;
            if (cbAudioFiles != null) {
                int index = cbAudioFiles.getSelectedIndex();
                if (index < cbAudioFiles.getModel().getSize() - 1) {
                    cbAudioFiles.setSelectedIndex(++index);
                }//if
                else {
                    cbAudioFiles.setSelectedIndex(1);
                }//else
                loadTranscription();
            }//if
            _bEnableEvent = true;
        }//if
    }//GEN-LAST:event_btnNextFileActionPerformed

    private void btnPreviousFileActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPreviousFileActionPerformed
        if (_bEnableEvent) {
            _bEnableEvent = false;
            if (cbAudioFiles != null) {
                int index = cbAudioFiles.getSelectedIndex();
                if (index > 1) {
                    cbAudioFiles.setSelectedIndex(--index);
                }//if
                else {
                    cbAudioFiles.setSelectedIndex(cbAudioFiles.getModel().getSize() - 1);
                }//else
                loadTranscription();
            }//if
            _bEnableEvent = true;
        }//if
    }//GEN-LAST:event_btnPreviousFileActionPerformed

    private void btnPlayPauseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPlayPauseActionPerformed
        playPauseAudioClip();
    }//GEN-LAST:event_btnPlayPauseActionPerformed

    private void btnEditFileInRootDirMhWaveEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditFileInRootDirMhWaveEditActionPerformed
        editFileInQuranRootDir("mhwaveedit");
    }//GEN-LAST:event_btnEditFileInRootDirMhWaveEditActionPerformed

    private void btnStopActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnStopActionPerformed
        stopAudioClip();
    }//GEN-LAST:event_btnStopActionPerformed

    private void cbRecitorsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbRecitorsActionPerformed
        if (_bEnableEvent) {
            _bEnableEvent = false;
            loadLists();
            _bEnableEvent = true;
        }//if
    }//GEN-LAST:event_cbRecitorsActionPerformed

    private void cbChaptersActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbChaptersActionPerformed
        if (_bEnableEvent) {
            _bEnableEvent = false;
            loadLists();
            _bEnableEvent = true;
        }//if
    }//GEN-LAST:event_cbChaptersActionPerformed

    private void cbAyatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbAyatActionPerformed
        if (_bEnableEvent) {
            _bEnableEvent = false;
            loadLists();
            _bEnableEvent = true;
        }//if
    }//GEN-LAST:event_cbAyatActionPerformed

    private void cbTranscriptionsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbTranscriptionsActionPerformed
        if (_bEnableEvent && cbTranscriptions.getSelectedIndex() != -1) {
            _bEnableEvent = false;
            String sTranscriptionText = taTranscription.getText();
            taTranscription.setText(cbTranscriptions.getSelectedItem().toString());
            cbTranscriptions.removeItemAt(cbTranscriptions.getSelectedIndex());
            DefaultComboBoxModel model = (DefaultComboBoxModel) cbTranscriptions.getModel();
            if (model.getIndexOf(sTranscriptionText) == -1) {
                cbTranscriptions.addItem(sTranscriptionText);
            }//if
            _bEnableEvent = true;
        }//if
    }//GEN-LAST:event_cbTranscriptionsActionPerformed

    private void btnEditAudioFileAudacityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditAudioFileAudacityActionPerformed
        editSelectedAudioFile("audacity");
    }//GEN-LAST:event_btnEditAudioFileAudacityActionPerformed

    private void btnEditFileInRootDirAudacityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditFileInRootDirAudacityActionPerformed
        editFileInQuranRootDir("audacity");
    }//GEN-LAST:event_btnEditFileInRootDirAudacityActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnClose;
    private javax.swing.JButton btnEditAudioFileAudacity;
    private javax.swing.JButton btnEditAudioFileMhWaveEdit;
    private javax.swing.JButton btnEditFileInRootDirAudacity;
    private javax.swing.JButton btnEditFileInRootDirMhWaveEdit;
    private javax.swing.JButton btnNextFile;
    private javax.swing.JButton btnPlayPause;
    private javax.swing.JButton btnPreviousFile;
    private javax.swing.JButton btnSaveTranscription;
    private javax.swing.JButton btnStop;
    private javax.swing.JComboBox cbAudioFiles;
    private javax.swing.JComboBox cbAyat;
    private javax.swing.JComboBox cbChapters;
    private javax.swing.JComboBox cbRecitors;
    private javax.swing.JComboBox cbTranscriptions;
    private javax.swing.JCheckBox ckAutoPlay;
    private javax.swing.JCheckBox ckRightToLeft;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JList lbFillers;
    private javax.swing.JLabel lblAudioFilename;
    private javax.swing.JLabel lblTranscriptionFilename;
    private javax.swing.JTextArea taTranscription;
    private myacousticmodelbuilder.WaveformPanelContainer waveformPanelContainer;
    // End of variables declaration//GEN-END:variables

}

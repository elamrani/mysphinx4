DROP VIEW IF EXISTS `sub_load_transcription`;
CREATE VIEW `sub_load_transcription` AS (
SELECT `transcriptions`.`index`, `position`, `tag` as `token`, `fillers`.`index` as `id`, "f" as `type`, 
		`quran_text`.`index` as `id_quran_text`, `quran_text`.`sura`, `quran_text`.`aya`, `recitors`.`index` as `id_recitors`, `recitors`.`name`
FROM `recitors`, `transcriptions`, `transcription_has_words_and_fillers`, `fillers`, `quran_text`
WHERE `recitors`.`index` = `transcriptions`.`id_recitors` 
	AND `transcriptions`.`index` = `id_transcriptions` 
    AND `id_fillers` = `fillers`.`index`
    AND `transcriptions`.`id_quran_text` = `quran_text`.`index`    
) UNION (
SELECT `transcriptions`.`index`, `position`, `word` as `token`, `words`.`index` as `id`, "w" as `type`, 
		`quran_text`.`index` as `id_quran_text`, `quran_text`.`sura`, `quran_text`.`aya`, `recitors`.`index` as `id_recitors`, `recitors`.`name`
FROM `recitors`, `transcriptions`, `transcription_has_words_and_fillers`, `words`, `quran_text`
WHERE `recitors`.`index` = `transcriptions`.`id_recitors` 
	AND `transcriptions`.`index` = `id_transcriptions` 
    AND `id_words` = `words`.`index`
    AND `transcriptions`.`id_quran_text` = `quran_text`.`index`
);

DROP VIEW IF EXISTS `load_transcription`;
CREATE VIEW `load_transcription` AS
SELECT *
FROM `sub_load_transcription`
ORDER BY `id_recitors`, `id_quran_text`, `sura`, `aya`, `position`;
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema quran
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema quran
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `quran` DEFAULT CHARACTER SET utf8 ;
USE `quran` ;

-- Update the `quran_text_clean_no_pauses` to make the sura and aya indexes
ALTER TABLE `quran`.`quran_text_clean_no_pauses` 
ADD INDEX `sura_idx` (`sura` ASC),
ADD INDEX `aya_idx` (`aya` ASC);

-- -----------------------------------------------------
-- Table `quran`.`words`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `quran`.`words` ;

CREATE TABLE IF NOT EXISTS `quran`.`words` (
  `index` INT(11) NOT NULL AUTO_INCREMENT,
  `word` VARCHAR(45) NOT NULL,
  `id_sura` INT(3) NOT NULL,
  `id_aya` INT(3) NOT NULL,
  PRIMARY KEY (`index`),
  INDEX `words_fk_quran_text_clean_no_pauses_idx` (`id_sura` ASC),
  INDEX `words_fk_quran_text_clean_no_pauses1_idx` (`id_aya` ASC),
  CONSTRAINT `words_fk_quran_text_clean_no_pauses`
    FOREIGN KEY (`id_sura`)
    REFERENCES `quran`.`quran_text_clean_no_pauses` (`sura`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `words_fk_quran_text_clean_no_pauses1`
    FOREIGN KEY (`id_aya`)
    REFERENCES `quran`.`quran_text_clean_no_pauses` (`aya`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quran`.`phonemes`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `quran`.`phonemes` ;

CREATE TABLE IF NOT EXISTS `quran`.`phonemes` (
  `index` INT(11) NOT NULL AUTO_INCREMENT,
  `phoneme` VARCHAR(3) NOT NULL,
  PRIMARY KEY (`index`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quran`.`words_has_phonemes`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `quran`.`words_has_phonemes` ;

CREATE TABLE IF NOT EXISTS `quran`.`words_has_phonemes` (
  `id_words` INT(11) NOT NULL,
  `index` INT(11) NOT NULL,
  `id_phonemes` INT(11) NOT NULL,
  PRIMARY KEY (`id_words`, `index`, `id_phonemes`),
  INDEX `words_has_phonemes_fk_phonemes1_idx` (`id_phonemes` ASC),
  INDEX `words_has_phonemes_fk_words1_idx` (`id_words` ASC),
  CONSTRAINT `words_has_phonemes_fk_words1`
    FOREIGN KEY (`id_words`)
    REFERENCES `quran`.`words` (`index`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `words_has_phonemes_fk_phonemes1`
    FOREIGN KEY (`id_phonemes`)
    REFERENCES `quran`.`phonemes` (`index`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema quran
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema quran
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `quran` DEFAULT CHARACTER SET utf8 ;
USE `quran` ;

-- -----------------------------------------------------
-- Table `quran`.`quran_text`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `quran`.`quran_text` ;

CREATE TABLE IF NOT EXISTS `quran`.`quran_text` (
  `index` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `sura` INT NOT NULL COMMENT '',
  `aya` INT NOT NULL COMMENT '',
  `text` TEXT NOT NULL COMMENT '',
  PRIMARY KEY (`index`)  COMMENT '')
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quran`.`words`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `quran`.`words` ;

CREATE TABLE IF NOT EXISTS `quran`.`words` (
  `index` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `word` VARCHAR(45) NOT NULL COMMENT '',
  PRIMARY KEY (`index`)  COMMENT '',
  UNIQUE INDEX `word_UNIQUE` (`word` ASC)  COMMENT '')
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quran`.`phonemes`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `quran`.`phonemes` ;

CREATE TABLE IF NOT EXISTS `quran`.`phonemes` (
  `index` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `phoneme` VARCHAR(3) NOT NULL COMMENT '',
  PRIMARY KEY (`index`)  COMMENT '',
  UNIQUE INDEX `phoneme_UNIQUE` (`phoneme` ASC)  COMMENT '')
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quran`.`word_has_phonemes`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `quran`.`word_has_phonemes` ;

CREATE TABLE IF NOT EXISTS `quran`.`word_has_phonemes` (
  `index` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `id_words` INT NOT NULL COMMENT '',
  `phoneme_position` INT NOT NULL COMMENT '',
  `id_phonemes` INT NOT NULL COMMENT '',
  PRIMARY KEY (`index`)  COMMENT '',
  INDEX `words_has_phonemes_fk_phonemes1_idx` (`id_phonemes` ASC)  COMMENT '',
  INDEX `words_has_phonemes_fk_words1_idx` (`id_words` ASC)  COMMENT '',
  CONSTRAINT `words_has_phonemes_fk_words1`
    FOREIGN KEY (`id_words`)
    REFERENCES `quran`.`words` (`index`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `words_has_phonemes_fk_phonemes1`
    FOREIGN KEY (`id_phonemes`)
    REFERENCES `quran`.`phonemes` (`index`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quran`.`quran_text_has_words`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `quran`.`quran_text_has_words` ;

CREATE TABLE IF NOT EXISTS `quran`.`quran_text_has_words` (
  `id_quran_text` INT NOT NULL COMMENT '',
  `word_position` INT NOT NULL COMMENT '',
  `id_words` INT NOT NULL COMMENT '',
  PRIMARY KEY (`id_quran_text`, `word_position`, `id_words`)  COMMENT '',
  INDEX `quran_text_has_words_fk_words1_idx` (`id_words` ASC)  COMMENT '',
  INDEX `quran_text_has_words_fk_quran_text1_idx` (`id_quran_text` ASC)  COMMENT '',
  CONSTRAINT `quran_text_has_words_fk_quran_text1`
    FOREIGN KEY (`id_quran_text`)
    REFERENCES `quran`.`quran_text` (`index`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `quran_text_has_words_fk_words1`
    FOREIGN KEY (`id_words`)
    REFERENCES `quran`.`words` (`index`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `quran`.`config`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `quran`.`config` ;

CREATE TABLE IF NOT EXISTS `quran`.`config` (
  `id` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `index_next_word` INT NOT NULL COMMENT '',
  `index_next_phoneme` INT NOT NULL COMMENT '',
  PRIMARY KEY (`id`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `quran`.`recitors`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `quran`.`recitors` ;

CREATE TABLE IF NOT EXISTS `quran`.`recitors` (
  `index` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `name` VARCHAR(100) NOT NULL COMMENT '',
  PRIMARY KEY (`index`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `quran`.`transcriptions`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `quran`.`transcriptions` ;

CREATE TABLE IF NOT EXISTS `quran`.`transcriptions` (
  `index` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `id_quran_text` INT NOT NULL COMMENT '',
  `id_recitors` INT NOT NULL COMMENT '',
  `fileid` VARCHAR(45) NOT NULL COMMENT 'fileid is the audio file associated to the transcription',
  PRIMARY KEY (`index`)  COMMENT '',
  INDEX `transcriptions_fk_recitors1_idx` (`id_recitors` ASC)  COMMENT '',
  INDEX `transcriptions_fk_quran_text1_idx` (`id_quran_text` ASC)  COMMENT '',
  UNIQUE INDEX `fileid_UNIQUE` (`fileid` ASC)  COMMENT '',
  CONSTRAINT `transcriptions_fk_recitors1`
    FOREIGN KEY (`id_recitors`)
    REFERENCES `quran`.`recitors` (`index`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `transcriptions_fk_quran_text1`
    FOREIGN KEY (`id_quran_text`)
    REFERENCES `quran`.`quran_text` (`index`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `quran`.`fillers`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `quran`.`fillers` ;

CREATE TABLE IF NOT EXISTS `quran`.`fillers` (
  `index` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `tag` VARCHAR(45) NOT NULL COMMENT '',
  `value` VARCHAR(45) NOT NULL DEFAULT 'SIL' COMMENT '',
  PRIMARY KEY (`index`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `quran`.`transcription_has_words_and_fillers`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `quran`.`transcription_has_words_and_fillers` ;

CREATE TABLE IF NOT EXISTS `quran`.`transcription_has_words_and_fillers` (
  `id_transcriptions` INT NOT NULL COMMENT '',
  `position` INT NOT NULL COMMENT '',
  `id_words` INT NULL DEFAULT NULL COMMENT '',
  `id_fillers` INT NULL DEFAULT NULL COMMENT '',
  PRIMARY KEY (`id_transcriptions`, `position`)  COMMENT '',
  INDEX `transcriptions_has_words_fk_words1_idx` (`id_words` ASC)  COMMENT '',
  INDEX `transcriptions_has_words_fk_transcriptions1_idx` (`id_transcriptions` ASC)  COMMENT '',
  INDEX `transcriptions_has_words_fk_fillers1_idx` (`id_fillers` ASC)  COMMENT '',
  CONSTRAINT `transcriptions_has_words_fk_transcriptions1`
    FOREIGN KEY (`id_transcriptions`)
    REFERENCES `quran`.`transcriptions` (`index`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `transcriptions_has_words_fk_words1`
    FOREIGN KEY (`id_words`)
    REFERENCES `quran`.`words` (`index`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `transcriptions_has_words_fk_fillers1`
    FOREIGN KEY (`id_fillers`)
    REFERENCES `quran`.`fillers` (`index`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = 'Only one of the fields id_words and id_fillers is allowed to be null';

USE `quran`;

DELIMITER $$

USE `quran`$$
DROP TRIGGER IF EXISTS `quran`.`twf_BINS` $$
USE `quran`$$
CREATE TRIGGER `twf_BINS` BEFORE INSERT ON `transcription_has_words_and_fillers` FOR EACH ROW
BEGIN
	IF (NEW.id_words IS NULL AND NEW.id_fillers IS NULL) THEN
		SIGNAL SQLSTATE '45000'
		SET MESSAGE_TEXT = '\'id_words\' and \'id_fillers\' cannot both be null';
	END IF;
END$$


USE `quran`$$
DROP TRIGGER IF EXISTS `quran`.`twf_BUPD` $$
USE `quran`$$
CREATE TRIGGER `twf_BUPD` BEFORE UPDATE ON `transcription_has_words_and_fillers` FOR EACH ROW
BEGIN
	IF (NEW.id_words IS NULL AND NEW.id_fillers IS NULL) THEN
		SIGNAL SQLSTATE '45000'
		SET MESSAGE_TEXT = '\'id_words\' and \'id_fillers\' cannot both be null';
	END IF;
END$$


DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `quran`.`config`
-- -----------------------------------------------------
START TRANSACTION;
USE `quran`;
INSERT INTO `quran`.`config` (`id`, `index_next_word`, `index_next_phoneme`) VALUES (1, 1, 1);

COMMIT;


-- -----------------------------------------------------
-- Data for table `quran`.`fillers`
-- -----------------------------------------------------
START TRANSACTION;
USE `quran`;
INSERT INTO `quran`.`fillers` (`index`, `tag`, `value`) VALUES (DEFAULT, '<s>', 'SIL');
INSERT INTO `quran`.`fillers` (`index`, `tag`, `value`) VALUES (DEFAULT, '</s>', 'SIL');
INSERT INTO `quran`.`fillers` (`index`, `tag`, `value`) VALUES (DEFAULT, '<sil>', 'SIL');

COMMIT;


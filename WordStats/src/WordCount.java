import java.io.BufferedWriter;
import java.io.File;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringTokenizer;

/**
 * @author Mohamed Y. El Amrani
 *
 */
public class WordCount {

	/**
	 * @param args
	 *            the filename
	 */
	public static void main(String[] args) {

		if (args.length != 1) {
			System.out.println("Usage: WordCount <filename>");
			System.out.println(" <filename> is the path to the file containing the text to calculate the word count");
			System.exit(-1);
		}// if

		countWords(new File(args[0]));

	}// main()

	private static HashMap<String, Integer> countWords(File file) {
		HashMap<String, Integer> wordCountsList = new HashMap<String, Integer>();

		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			String line;
			System.out.println("Counting the words...");
			int lineCount = 0;
			while ((line = br.readLine()) != null) {
				// Extract the words from the line
				StringTokenizer st = new StringTokenizer(line);

				// Put them in the list if not found or increment the count if
				// found.
				while (st.hasMoreTokens()) {
					String word = st.nextToken();
					if (wordCountsList.containsKey(word)) {
						wordCountsList.put(word, wordCountsList.get(word) + 1);
					}// if
					else {
						// check if the word which starts with 'و' or 'ف' and is
						// already in the list
						if (wordCountsList.containsKey("و" + word) || wordCountsList.containsKey("ف" + word)) {
							// remove those letters from the word and
							// increment the count
							int wordCount = wordCountsList.get(word.substring(1)) + 1;
							wordCountsList.remove(word);
							wordCountsList.put(word.substring(1), wordCount);
						}// if
						else if ('و' == word.charAt(0) || 'ف' == word.charAt(0)) {
							if (wordCountsList.containsKey(word.substring(1))) {
								wordCountsList.put(word.substring(1), wordCountsList.get(word.substring(1)) + 1);
							}// if
						}// else
						else {
							// add a new entry
							wordCountsList.put(word, new Integer(1));
						}// else
					}// else
				}// while

				lineCount++;
				//if (lineCount % 10 == 0) {
				//	System.out.println("Processed " + lineCount + " lines");
				//}//if
			}// while
			br.close();
			System.out.println("Processed " + lineCount + " lines");

			// Write the info in a file
			File fout = new File("out.txt");
			FileOutputStream fos = new FileOutputStream(fout);

			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));

			// create your iterator for your map
			Iterator<Entry<String, Integer>> it = wordCountsList.entrySet().iterator();

			// then use the iterator to loop through the map, stopping when we
			// reach the
			// last record in the map or when we have printed enough records
			int wordCounter = 0;
			while (it.hasNext()) {
				wordCounter++;
				// the key/value pair is stored here in pairs
				Map.Entry<String, Integer> pairs = it.next();
				// System.out.println(pairs.getKey() + ": " + pairs.getValue());

				// since you only want the value, we only care about
				// pairs.getValue(), which is written to out
				bw.write(pairs.getKey() + " " + pairs.getValue() + "\n");
			}// while
			bw.write("\nNumber of words: " + wordCounter + "\n");
			System.out.println("\nNumber of words: " + wordCounter + "\n");
			// lastly, close the file and end
			bw.close();

		}// try
		catch (Exception e) {
			System.err.println("An exception occured: ");
			System.err.println(e.getMessage());
			System.err.println(e.getStackTrace());
		}// catch

		return wordCountsList;
	}

}

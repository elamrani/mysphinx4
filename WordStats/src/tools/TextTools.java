package tools;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;

/**
 * @author Mohamed Y. El Amrani
 *
 */
public class TextTools {

	/**
	 * @param args program arguments
	 */
	public static void main(String[] args) {
		
		if (args.length != 2) {
			System.out.println("Usage: TextTools <prepareReferenceText | removeDuplicatedLines> <filename>");
			System.out.println(" <prepareReferenceText | removeDuplicatedLines> either one of the tools");
			System.out.println(" <filename> is the path to the file containing the text to calculate the word count");
			System.exit(-1);
		}// if

		if (args[0].equals("prepareReferenceText")) {
			prepareReferenceText(new File(args[1]));
		}//if
		else if (args[0].equals("removeDuplicatedLines")) {
			removeDuplicatedLines(new File(args[1]));
		}//else if
		else {
			System.out.println("Wrong arguments: Use either prepareReferenceText or removeDuplicatedLines");
			System.exit(-2);			
		}//else
		
	}
	
	
	private static void removeDuplicatedLines(File file) {
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			ArrayList<String> phones = new ArrayList<String>();
			phones.add("SIL");//make sure that SIL is part of the phone list.
			String line;
			System.out.print("Parsing the file...");
			while ((line = br.readLine()) != null) {
				System.out.print(".");
				//remove extra spaces
				line = line.trim();
				if (!phones.contains(line)) {
					phones.add(line);
				}//if
			}// while
			br.close();

			//Sort the list
			Collections.sort(phones);
			
			// Write the info in the a file
			File fout = new File(file.getPath());
			FileOutputStream fos = new FileOutputStream(fout);
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
			
			for (String p : phones) {
				bw.write(p + "\n");
			}//for each string
			bw.close();
			System.out.println("\ndone. Saved file in: " + fout.getPath());
		}// try
		catch (Exception e) {
			System.err.println("An exception occured: ");
			System.err.println(e.getMessage());
			System.err.println(e.getStackTrace());
		}// catch
	}


	/**
	 * Prepare a reference text that will be used to generate the language model. 
	 * The language model toolkit expects its input to be in the form of normalized text files, 
	 * with utterances delimited by <s> and </s> tags. 
	 * A number of input filters are available for specific corpora such as Switchboard, 
	 * ISL and NIST meetings, and HUB5 transcripts. 
	 * The result should be the set of sentences that are bounded by the start and end 
	 * sentence markers: <s> and </s>
	 * @param file 
	 */
	public static void prepareReferenceText(File file) {
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			// Write the info in a file
			//File fout = new File(file.getPath() + "-no-symbols.txt");
			File fout = new File(file.getPath() + ".out.txt");
			FileOutputStream fos = new FileOutputStream(fout);
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
			
			String line;
			System.out.print("Parsing the file...");
			while ((line = br.readLine()) != null) {
				System.out.print(".");
				line = line.replaceAll("[ۙۛۜ۩ۘۖۗۚ]", "");//remove extra characters for the reading stops.
				bw.write("<s> " + line + " </s>\n");
				//bw.write(line + "\n");
			}// while
			br.close();
			bw.close();
			System.out.println("\ndone. Saved file in: " + fout.getPath());
		}// try
		catch (Exception e) {
			System.err.println("An exception occured: ");
			System.err.println(e.getMessage());
			System.err.println(e.getStackTrace());
		}// catch
		
	}

}
